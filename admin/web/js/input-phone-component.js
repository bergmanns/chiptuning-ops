(function () {
  var defaults = {
    match: /[0-9]/,
    replace: '#',
    listKey: 'mask',
    inputmask: {
      definitions: {
        '#': {
          validator: '[0-9]',
          cardinality: 1
        }
      },
      showMaskOnHover: false,
      autoUnmask: true,
      clearMaskOnLostFocus: false,
    }
  };

  var masks = [];
  var received = false;

  // TODO Extract mask options into input-component.
  Vue.component('input-phone-component', {
    props: {
      type: null,
      name: null,
      valid: null,
      required: null,
      placeholder: null,
      value: {default: '+7'},
      compact: null
    },

    computed: {
      runtime: {
        get: function () {
          return this.value;
        },

        set: function (value) {
          this.$emit('input', value);
        }
      }
    },

    created: function () {
      this.receiveMaskTemplates()
        .then(this.createMask.bind(this));
    },

    methods: {
      receiveMaskTemplates: function () {
        if (received) {
          return Promise.resolve();
        }

        // TODO Use variable instead of data fetching.
        return axios.get('/vendor/jquery-inputmask/phone-codes.json').then(function (response) {
          // change global scope variable to remember data response
          masks = $.masksSort(response.data, ['#'], /[0-9]|#/, 'mask');
          received = true;
        });
      },

      createMask: function () {
        var options = this.resolveMaskOptions();
        $(this.$el).inputmasks('remove');
        $(this.$el).inputmasks(options);
      },

      validateValue: function (value) {
        return !!value;
      },

      resolveMaskOptions: function () {
        return _.merge({}, defaults, {list: masks}, {
          inputmask: {
            onKeyDown: this.onFieldKeyDown,
            oncomplete: this.onFieldMaskComplete
          }
        });
      },

      resolveIsMaskComplete: function () {
        return $(this.$el).inputmask('isComplete');
      },

      onFieldKeyDown: function () {
        this.valid = this.resolveIsMaskComplete();
      },

      onFieldMaskComplete: function () {
        this.valid = this.resolveIsMaskComplete() && this.validateValue(this.$el.value);
      }
    },

    template: `
      <input-component 
        v-model="runtime" 
        v-bind:type="type" 
        v-bind:name="name"
        v-bind:valid="valid"
        v-bind:required="required"
        v-bind:placeholder="placeholder" 
        v-bind:compact="compact"
      />
    `
  });
})();
