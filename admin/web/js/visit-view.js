var E_SERVER_ERROR = 'Неизвестная ошибка загрузки данных';

// fields definition
var tableColumns = [
//    {
//        name: '__actions',
//        dataClass: 'text-center'
//    },
    {
        name: '__component:customActions',
        title: '',
        dataClass: 'text-center vuetable-actions'

    },
    {
        name: 'id',
        title: '',
        dataClass: 'text-center',
        callback: 'showDetailRow'
    },
    {
        name: 'visit_at',
        title: 'День',
        callback: 'formatDate|DD.MM',
        // sortField: 'created_at',
    },
    {
        name: 'visit_at',
        title: 'Время',
        callback: 'formatDate|HH:mm',
        // sortField: 'created_at',
    },
    {
        name: 'location.name',
        title: 'Филиал',
        // sortField: 'email',
    },
    {
        name: '__component:car-title',
        title: 'Машина'
    },
    {
        name: 'category.name',
        title: 'Цель',
        titleClass: 'text-center',
        dataClass: 'text-center',
        // sortField: 'category.id'
    },
    {
        name: 'product.name',
        title: 'Продукт',
        // sortField: 'product.name',
    },
    {
        name: 'product.total_price',
        title: 'Цена',
        // sortField: 'product.price',
    },
    {
        name: '__component:client-title',
        title: 'Клиент'
    },
    {
        dataClass: 'width-fixed',
        name: 'comment',
        title: 'Комментарий',
    },
    {
        name: 'status',
        title: 'Статус',
        titleClass: 'text-center',
        dataClass: 'text-center',
        callback: 'status'
    }
];

Vue.config.debug = true;

Vue.component('customActions', {
    template: [
`    
    <button title="Изменить" data-toggle="tooltip" data-placement="top" v-bind:class="{ disabled: !allowEdit }"  class="btn btn-default"  @click="allowEdit && callAction('edit-item', rowData, rowIndex)">
        <i class="glyphicon glyphicon-pencil"></i> 
    </button>
    <button title="Удалить" data-toggle="tooltip" data-placement="right" v-bind:class="{ disabled: !allowDelete }" class="btn btn-default" @click="allowDelete && callAction('delete-item', rowData, rowIndex)">
        <i class="glyphicon glyphicon-trash"></i> 
    </button>
    <button title="Копировать" data-toggle="tooltip" data-placement="right" v-bind:class="{ disabled: !allowCopy }"  class="btn btn-default" @click="allowCopy && callAction('copy-item', rowData, rowIndex)">
        <i class="glyphicon glyphicon-copy"></i> 
    </button>
    <button title="Добавить" data-toggle="tooltip" data-placement="right" v-bind:class="{ disabled: !allowAddItem }" class="btn btn-default" @click="allowAddItem && callAction('add-item', rowData, rowIndex)">
        <i class="glyphicon glyphicon-plus-sign"></i> 
    </button>
    <button title="Перевести в Успех" data-toggle="tooltip" data-placement="right" v-bind:class="{ disabled: !allowComplete }" class="btn btn-default" @click="allowComplete && callAction('complete-item', rowData, rowIndex)">
        <i class="glyphicon glyphicon-ok"></i> 
    </button>
    <button title="Сохранить заказ-наряд" data-toggle="tooltip" data-placement="right" v-bind:class="{ disabled: !allowGenerateOrder  }" class="btn btn-default" @click="allowGenerateOrder && callAction('generate-order', rowData, rowIndex)">
        <i class="glyphicon glyphicon-list-alt"></i> 
    </button>
`
    ].join(''),

    data: function () {
        return {
            allowEdit: !this.rowData.paid_at,
            allowDelete: !this.rowData.paid_at && this.rowData.status !== 'create',
            allowCopy: true,
            allowAddItem: true,
            allowComplete: !this.rowData.completed_at,
            allowGenerateOrder: this.rowData.completed_at || this.rowData.paid_at,
        }
    },
    methods: {
        callAction: function callAction(action, data) {
            this.$dispatch('vuetable:action', action, data);
        },
    },
    props: {
        rowData: {
            type: Object,
            required: true
        },
        rowIndex: {
            type: Number
        }
    },
});

Vue.component('car-title', {
    template: [
`<div>
        <span>
            <span>
                {{rowData.car.make_name}}
            </span>
            <span>
                {{rowData.car.model_name}}
            </span>
             <span>
                {{rowData.car.engine_volume}}
            </span>
             <span>
                {{rowData.car.engine_name}}
            </span>
        </span>
        <br>
        <span>
            <span>
                {{rowData.car.issue_year}}
            </span>
             <span>
                {{rowData.car.vin_number}}
            </span>
        </span>
</div>
`
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
        itemAction: function(action, data) {
            swal({
                title: 'Item Action',
                text: 'custom-action: ' + action + ' ' + data.name,
                icon: "error",
            });
        },
    }
});

Vue.component('payment-title', {
    template: [
        `<div>
        <span>
            {{rowData.car.make_name}}
        </span>
</div>
`
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        },
        rowIndex: {
            type: Number
        }
    },
    data: function () {
        return {
            items: [
            ]
        }
    },
    created: function () {
        // `this` указывает на экземпляр vm
    },
    methods: {
    },
});

Vue.component('client-title', {
    template: [
        `<div>
        <span>
            <span>
                {{rowData.client.phone}}
            </span>
        </span>
        <br>
        <span>
            <span>
                {{rowData.client.full_name}}
            </span>
        </span>
        </div>
`
    ].join(''),
    props: {
        rowData: {
            type: Object,
            required: true
        }
    },
    methods: {
    }
});

Vue.component('payment-table', {
    template:
        `
        <div class="detail-row ui form">
           <table v-if="items.length > 0" class="table table-dark table-hover table-sm">
              <thead>
                <tr>
                  <th scope="col">Платеж</th>
<!--                  <th scope="col">Аккаунт</th>-->
                  <th scope="col">Категория</th>
                  <th scope="col">Тип</th>
                  <th scope="col">Статус</th>
                  <th scope="col">Сумма к оплате</th>
                  <th scope="col">Оплачено</th>
                  <th scope="col">Дата</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="item in items">
                  <th scope="row">{{item.name}}</th>
<!--                  <th scope="row">{{item.account.name}}</th>-->
                  <th scope="row">{{item.category.name}}</th>
                  <td>{{item.type.name}}</td>
                  <td>{{item.status}}</td>
                  <td :class="{red: item.amount > item.paid_amount}" >{{item.amount}}</td>
                  <td :class="{green: item.amount <= item.paid_amount}" >{{item.paid_amount}}</td>
                  <td>{{item.created_at}}</td>
                  <td><button :class="{hidden: (item.repayment == false)}" @click="selectDebt(item)" type="button" class="btn btn-sm btn-default" data-toggle="modal" data-target="#paymentModal_{{rowData.id}}">Погасить</button></td>
                </tr>
              </tbody>
            </table>
            <div v-else>
                <span>Платежи отсутствуют</span>
            </div>
            <div class="modal fade" id="paymentModal_{{rowData.id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Платеж:</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="">
                                <div class="form-group">
                                    <label for="payment_account" class="control-label">Комментарий:</label>
                                    <input id="payment_name" v-model="modalItem.name" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="payment_account" class="control-label">Аккаунт:</label>
                                    <select type="text" v-model="modalItem.account.id" class="form-control" id="payment_account">
                                       <template v-for="account in accounts">
                                            <option value="{{account.id}}">{{account.name}}</option>
                                        </template>
                                    </select>                                
                                </div>
                                <div class="form-group">
                                    <label for="payment_category" class="control-label">Категория</label>
                                    <select type="text" v-model="modalItem.category.id" class="form-control" id="payment_category">
                                        <template v-for="category in categories">
                                            <option value="{{category.id}}">{{category.name}}</option>
                                        </template>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="payment-amount" class="control-label">Сумма</label>
                                    <input id="payment-amount" v-model="modalItem.amount" type="input" class="form-control">
                                </div>
                            </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <button type="button" class="btn btn-primary save-payment" @click="savePayment">Внести платеж</button>
                  </div>
                </div>
              </div>
            </div>
        </div>`,
    props: {
        rowData: {
            type: Object,
            required: true
        },
    },
    data: function () {
        return {
            items: [
            ],
            selectedDebtId: null,
            categories: [],
            accounts: [],
            modalItem: {
                id: null,
                name: null,
                category: {
                  id: null,
                  name: null,
                },
                account: {
                    id: null,
                    name: null,
                },
                type: 'visit',
                status: null,
                amount: null,
                created_at: null,
            }
        }
    },
    created: function () {
        this.initCategories();
        this.initAccounts();

        if (this.rowData.isNewRow === false) {
            this.fetchPayments();
        }
    },
    methods: {
        selectDebt: function (debt) {
            this.selectedDebtId = debt.id;
        },
        onClick: function(event) {
            console.log('payment-table: on-click')
        },
        initCategories: function() {
            let self = this;
            let url = '/categories/api/index';

            self.$http.get(url).then(function (response) {
                self.categories = response.data;

                if (self.categories.length > 0) {
                    self.modalItem.category.id = response.data[0].id;
                    self.modalItem.category.name = response.data[0].name;
                }
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        initAccounts: function() {
            let self = this;
            let url = '/accounts/api/visible/';

            self.$http.get(url).then(function (response) {
                self.accounts = response.data;

                if (self.categories.length > 0) {
                    self.modalItem.account.id = response.data[0].id;
                    self.modalItem.account.name = response.data[0].name;
                }
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        fetchPayments: function () {
            let self = this;
            let url = '/visit/api/payments?id=' + this.rowData.visit_id;

            self.$http.get(url).then(function (response) {
                self.items = response.data;
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        savePayment: function () {
            let self = this;

            if (self.rowData.status !== 'done' && self.rowData.status !== 'paid') {
                swal({
                    title: "Ошибка",
                    text: "Визит еще не завершен",
                    icon: "error",
                });

                self.clearModal();
                $('#paymentModal_' + self.rowData.id).modal('toggle');

                return;
            }

            let url = '/visit/api/payment-save';
            let param = $('meta[name=csrf-param]').attr("content");
            let token = $('meta[name=csrf-token]').attr("content");
            let body = {
                visit_id: self.rowData.id,
                debt_id: this.selectedDebtId,
                location_id: self.rowData.location.id,
                account_id: self.modalItem.account.id,
                category_id: self.modalItem.category.id,
                amount: self.modalItem.amount,
                name: self.modalItem.name,
            };

            body[param] = token;

            this.$http.post(url, body, {
                emulateJSON: true
            }).then(function (response) {
                if (response.data.amount === response.data.paid_amount) {
                    self.rowData.status = 'paid';
                }

                self.fetchPayments();
                self.clearModal();
                $('#paymentModal_' + self.rowData.id).modal('toggle');
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });

        },
        clearModal: function () {
            this.modalItem = {
                name: null,
                category: {
                    id: null,
                    name:null,
                },
                account: {
                    id: 2,
                    name: null,
                },
                type: 'visit',
                status: null,
                amount: null,
                created_at: null,
            };
        }
    },
});

let vue = new Vue({
    el: '#app',
    data: {
        selectedRow: {
            id: null,
            externalId: null,
            isAmountConfirmed: false,
            isLandingPaid: false,
            isNewRow: true,
            real_amount: null,
            visit_id: null,
            paid_at: null,
            completed_at: null,
            client: {
                phone: null,
                full_name: null,
            },
            location: {
                name: null,
            },
            car: {
                make_name: null,
                model_name: null,
                engine_id: null,
                gearbox_id: null,
                full_name: null,
                vin_number: null,
                engine_model: null,
                gos_number: null,
                current_mileage: null,
            },
            product: {
                id: null,
                name: null,
            },
            category: {
                id: null,
                name: null
            },
            parts: [
            ],
            comment: null
        },
        newPart: {
            name: null,
            count: null,
            amount: null,
        },
        defaultRow: null,
        filter: {
            make_ids: null,
            model_ids: null,
            car_ids: null,
            phone: null,
            name: null,
            location: null,
            interval: 'week',
            current: null,
        },
        fields: tableColumns,
        sortOrder: [{
            field: 'visit_at',
            direction: 'asc'
        }],
        cars: [],
        multiSort: true,
        perPage: 25,
        paginationComponent: 'vuetable-pagination',
        itemActions: [
            { name: 'edit-item', label: '', icon: 'glyphicon glyphicon-pencil', class: 'btn btn-default', extra: {title: 'Изменить', 'data-toggle':"tooltip", 'data-placement': "top"} },
            { name: 'delete-item', label: '', icon: 'glyphicon glyphicon-trash', class: 'btn btn-default', extra: {title: 'Удалить', 'data-toggle':"tooltip", 'data-placement': "right" } },
            { name: 'copy-item', label: '', icon: 'glyphicon glyphicon-copy', class: 'btn btn-default', extra: {title: 'Копировать', 'data-toggle':"tooltip", 'data-placement': "right" } },
            { name: 'add-item', label: '', icon: 'glyphicon glyphicon-plus-sign', class: 'btn btn-default', extra: {title: 'Добавить', 'data-toggle':"tooltip", 'data-placement': "right" } },
            { name: 'complete-item', label: '', icon: 'glyphicon glyphicon-ok', class: 'btn btn-default', extra: {title: 'Перевести в Успех', 'data-toggle':"tooltip", 'data-placement': "right" } },
            { name: 'generate-order', label: '', icon: 'glyphicon glyphicon-list-alt', class: 'btn btn-default', extra: {title: 'Сохранить заказ-наряд', 'data-toggle':"tooltip", 'data-placement': "right" } }
        ],
        moreParams: [],
        gosCountries: [],
        locations: [],
        currentCountry: "Страна",
    },
    created: function () {
        this.defaultRow = this.selectedRow;
        this.initGosNumberInput();
        this.initLocations();
        this.initVinMask();
    },
    computed: {
        selectedVisitDateAt: {
            get: function () {
                if (this.selectedRow !== null) {
                    return this.formatDate(this.selectedRow.visit_at, 'DD.MM');
                }

                return  '';
            },
        },
        selectedVisitTimeAt: {
            get: function () {
                if (this.selectedRow !== null) {
                    return this.formatDate(this.selectedRow.visit_at, 'HH:mm');
                }

                return  '';
            },
        }
    },
    watch: {
        'selectedRow.isLandingPaid': function (val, oldVal) {
            if (val === true) {
                $('#external-order-id').removeAttr('disabled');
            } else if (val === false) {
                $('#external-order-id').attr('disabled', true);
            }
        },
        'perPage': function(val, oldVal) {
            this.$broadcast('vuetable:refresh')
        },
        'paginationComponent': function(val, oldVal) {
            this.$broadcast('vuetable:load-success', this.$refs.vuetable.tablePagination)
            this.paginationConfig(this.paginationComponent)
        }
    },
    methods: {
        initGosNumberInput: function() {
            let self = this;
            let url = "/locations/api/countries";

            self.$http.get(url).then(function (response) {
                self.gosCountries = response.data;
            }, function (response) {
                swal({
                    title: "Ошибка загрузки маски гос.номеров!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        initLocations: function() {
            let self = this;
            let url = "/locations/api/index";

            self.$http.get(url).then(function (response) {
                self.locations = response.data;
            }, function (response) {
                swal({
                    title: "Ошибка загрузки филиалов",
                    text: response.data.message,
                    icon: "error",
                });
            });
        },
        initVinMask: function () {
            let maskOpts = {
                definitions: {
                    "a": {
                        validator: "[0-9AaBbCcDdEeFfGgHhJjKkLlMmNnPpRrSsTtUuVvWwXxYyZz]",
                        casing: "upper"
                    },
                },
                showMaskOnHover: true,
                autoUnmask: false,
                clearMaskOnLostFocus: true,
                oncomplete: function() {
                    let value = $('#visits-vin_number').val();

                    vue.$emit('car-block:vin-selected', value);
                },
            };

            let selector = $("#visits-vin_number");
            selector.inputmask('a{17}', maskOpts);

            $(document).on('blur', '#visits-vin_number', function(){
                if (selector.inputmask("isComplete")){
                    vue.$emit('car-block:vin-selected', selector.val());
                }
            });
        },
        selectCountryMask: function (country) {
            if (country === null || country.gos_number_mask === null) {
                return;
            }

            let selector = $(".gos-input-number");

            this.currentCountry = country.short;
            let maskOpts = {
                definitions: {
                    "#": {
                        validator: "[0-9]",
                        cardinality: 1
                    },
                    "a": {
                        validator: "[a-zA-ZА-Яа-я]",
                        casing: "upper"
                    },
                    "R": {
                        validator: "[АВЕКМНОРСТУХавекмнорстухABEKMHOPCTYXabekmhopctyx]",
                        casing: "upper"
                    },
                    "B": {
                        validator: "[АаВвЕеІiКкМмНнОоРрСсТтХхAaBbTtIiKkMmHhOoPpCcTtXx]",
                        casing: "upper"
                    }
                },
                showMaskOnHover: false,
                autoUnmask: true,
                clearMaskOnLostFocus: false,
                oncomplete: function() {
                    let gos = $('.gos-input-number').val();

                    vue.$emit('car-block:field-selected', 'gos_number', gos);
                },
            };

            selector.inputmask('remove');
            selector.inputmask(country.gos_number_mask, maskOpts, false);
            selector.val(this.selectedRow.car.gos_number);

            $(document).on('blur', '.gos-input-number', function(){
                if (selector.inputmask("isComplete")){
                    vue.$emit('car-block:field-selected', 'gos_number', selector.val());
                }
            });
        },
        getFilterValue: function() {
            let value;

            if (this.filter.current === null) {
                value = moment().format('YYYY MM DD');
           } else {
                value = moment.unix(this.filter.current).format('YYYY MM DD');
            }

            return value;
        },
        setDateTime: function(date) {
            try {
                $('#visits-visit_at').data("DateTimePicker").destroy();
            } catch (e) {
            }

            let mDate = moment(date);

            let option = {
                defaultDate: date,
                format: 'YYYY-MM-DD HH:mm',
                locale: 'ru',
                inline: true,
                sideBySide: true,
                stepping: 15,
                useCurrent: false,
                showTodayButton: true,
                // enabledHours: [10, 11, 12, 13, 14, 15, 16, 17, 18],
                tooltips: {
                    today: 'Сегодня',
                    clear: 'Сбросить',
                    close: 'Закрыть',
                    selectMonth: 'Месяц',
                    prevMonth: 'Следующий Месяц',
                    nextMonth: 'Предыдущий месяц',
                    selectYear: 'Год',
                    prevYear: 'Предыдущий Год',
                    nextYear: 'Следующий Год',
                }
            };
            $('#visits-visit_at').datetimepicker(option);
        },
        getClient: function() {
            let self = this;
            let url = '/client/get-by-phone?phone=' + this.selectedRow.client.phone

            self.$http.get(url).then(function (response) {
                self.selectedRow.client.id = response.data;
                self.initializeClient(self.selectedRow.client.id);
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        status: (value) => {
            switch (value) {
                case 'new':
                    return '<span class="label label-default">Записан</span>';
                case 'active':
                    return '<span class="label label-default">В работе</span>';
                case 'done':
                    return '<span class="label label-warning">Выполнен</span>';
                case 'cancelled':
                    return '<span class="label label-default">Отменен</span>';
                case 'paid':
                    return '<span class="label label-success">Оплачен</span>';
                case 'paid_not_done':
                    return '<span class="label label-danger">Оплачен, не завершен</span>';
                default:
                    return '<span class="label label-default">Слот доступен</span>';
            }
        },
        payment: (value, id) => {
                return value;
        },
        allCap: function(value) {
            if (value === null) {
                return value;
            }

            return value.toUpperCase()
        },
        formatDate: function(value, fmt) {
            if (value == null) {
                return '';
            }
            fmt = (typeof fmt == 'undefined') ? 'YYYY-MM-DD HH:mm:ss' : fmt;
            return moment(value, 'YYYY-MM-DD HH:mm:ss').format(fmt)
        },
        showDetailRow: function(value) {
            var icon = this.$refs.vuetable.isVisibleDetailRow(value) ? 'glyphicon glyphicon-minus-sign' : 'glyphicon glyphicon-rub'

            return [
                '<button type="button" class="btn btn-sm btn-default"><i class="' + icon + '"></i></button>',
            ].join('')
        },
        setFilter: function() {
            this.moreParams = [];

            for (let variable in this.filter) {
                if (variable === null || this.filter[variable] === null) {
                    continue;
                }
                this.moreParams.push([
                    variable+ '=' + this.filter[variable],
                ]);
            }
        },
        setTableInterval: function(value) {
            if (this.filter.interval === value) {
                return;
            }

            this.filter.interval = value;
            this.filter.current = null;
            this.setFilter();
            this.refreshTable();
        },
        shiftFilter: function(direction) {
            let init;

            if (this.filter.current === null) {
                init = moment();
            } else {
                init = moment.unix(this.filter.current);
            }

            switch (this.filter.interval) {
                case 'month':
                    if (direction === 'next') {
                        init = init.add(1, 'M');
                    } else {
                        init = init.subtract(1, 'M');
                    }
                    break;
                case 'week':
                    if (direction === 'next') {
                        init = init.add(1, 'w');
                    } else {
                        init = init.subtract(1, 'w');
                    }
                    break;
                case 'day':
                    if (direction === 'next') {
                        init = init.add(1, 'd');
                    } else {
                        init = init.subtract(1, 'd');
                    }
                    break;
            }

            this.filter.current = init.unix();
            this.setFilter();
            this.refreshTable();
        },
        refreshTable: function() {
            this.$nextTick(function() {
                this.$broadcast('vuetable:refresh')
            })
        },
        resetFilter: function() {
            this.filter = {
                make_ids: null,
                model_ids: null,
                car_ids: null,
                phone: null,
                name: null,
                location: null,
                interval: 'week',
                current: null
            };

            $('.phone-filter').val(null);

            this.setFilter();
            this.refreshTable();
        },
        applyFilter: function() {
            let phoneFilter = $('.phone-filter').val();

            if (phoneFilter !== '') {
                this.filter.phone = phoneFilter;
                this.setFilter();
            }

            this.refreshTable();
        },
        preg_quote: function( str ) {
            return (str+'').replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1");
        },
        highlight: function(needle, haystack) {
            return haystack.replace(
                new RegExp('(' + this.preg_quote(needle) + ')', 'ig'),
                '<span class="highlight">$1</span>'
            )
        },
        rowClassCB: function(data, index) {
            return data.isLastTaskInDay === true ? 'last-row-in-day' : ''
        },
        addNewRow: function(rowData = null) {
            let rowsCount = this.$refs.vuetable.tableData.length;
            let lastRow = null;
            let lastVisitDate = moment().hours(6).startOf('hour');
            let lastId = 0;

            if (this.filter.current !== null) {
                lastVisitDate = moment.unix(this.filter.current).hours(6).startOf('hour');
            }

            if (rowsCount > 0) {
                lastRow = this.$refs.vuetable.tableData[rowsCount - 1];
                lastVisitDate = moment(lastRow.visit_at);
                lastId = lastRow.id;

                if (lastId.toString().charAt(0) === 'v') {
                    lastId = parseInt(lastId.slice(1, lastId.length));
                }
            }

            if (rowData === null) {
                let newRow = { ...this.defaultRow };

                newRow.visit_at = lastVisitDate.add(2, 'hours');
                newRow.id = lastId + 1;

                this.$refs.vuetable.tableData.push({ ...newRow });
            } else {
                let copyRow = { ...rowData };
                let rowIndex = this.$refs.vuetable.tableData.indexOf(rowData);
                copyRow.id = lastId + 1;
                copyRow.isNewRow = true;
                copyRow.status = 'create';

                if (rowData.isLastTaskInDay) {
                    copyRow.isLastTaskInDay = true;
                    rowData.isLastTaskInDay = false;
                }

                this.$refs.vuetable.tableData = [
                    ...this.$refs.vuetable.tableData.slice(0, rowIndex + 1),
                    ...[copyRow],
                    ...this.$refs.vuetable.tableData.slice(rowIndex + 1)
                ]
            }
        },
        addItem: function () {
            this.renderEditView(this.defaultRow);
        },

        // Parts
        deletePart: function (index) {
            this.selectedRow.parts.splice(index, 1);
        },
        addPart: function (newItem) {
            newItem.deleted = false;

            if (newItem.name === '') {
                return;
            }

            if (newItem.count === '') {
                return;
            }

            if (isNaN(parseInt(newItem.count)) === true) {
                return;
            }

            if (isNaN(parseFloat(newItem.amount)) === true) {
                return;
            }

            this.selectedRow.parts.push({ ...newItem });
            newItem.name = '';
            newItem.count = '';
            newItem.amount = '';
        },
        renderEditView: function(rowData) {
            $('#visits-products').val(null).trigger('change');
            this.selectedRow = { ...rowData };

            if (this.selectedRow.location.id === "") {
                this.selectedRow.location.id = this.locations[0].id;
                this.selectCountryMask(this.locations[0].country);
            }
            this.getClient();
            this.setDateTime(this.selectedRow.visit_at);

            if (this.selectedRow.isNewRow === false) {
                this.selectCountryMask(rowData.location.country);
            }
            $('#myModal').modal('toggle');
        },
        paginationConfig: function(componentName) {
            if (componentName == 'vuetable-pagination') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'pagination',
                    icons: { first: '', prev: '', next: '', last: ''},
                    activeClass: 'active',
                    linkClass: 'btn btn-default',
                    pageClass: 'btn btn-default'
                })
            }
            if (componentName == 'vuetable-pagination-dropdown') {
                this.$broadcast('vuetable-pagination:set-options', {
                    wrapperClass: 'form-inline',
                    icons: { prev: 'glyphicon glyphicon-chevron-left', next: 'glyphicon glyphicon-chevron-right' },
                    dropdownClass: 'form-control'
                })
            }
        },
        initializeClient(clientId) {
            self = this;

            if (clientId === null) {
                $('#visits-client_name').removeClass('hidden').val('').removeAttr('disabled');
                self.selectedRow.car.id = '-1';
                self.cars = [];
                this.renderClientCarsBlock([]);
            } else {
                $.ajax({
                    url: '/client/info/',
                    data: {
                        'id': clientId,
                    },
                    type: 'GET',
                    success: function (data) {
                        self.client = data.client;
                        self.cars = data.cars;
                        renderClientFields(self.client);
                        self.renderClientCarsBlock(self.cars);

                        if (self.selectedRow.car.id !== null) {
                            self.selectCar(self.selectedRow.car.id);
                        }
                    },
                    error: function (error, eer) {
                        console.log(error);
                    }
                });
            }
        },
        selectCar(id) {
            id = parseInt(id);
            this.initProductsForCar();

            if (id === -1) {
                this.selectedRow.car = { ...this.defaultRow.car };
                resetCarsBlock();
            } else {
                let car = this.getCarById(id, this.cars);

                if (typeof car !== 'undefined') {
                  this.selectedRow.car = { ...car };
                  this.renderSelectedCar(this.selectedRow.car);
                }
            }
        },
        renderSelectedCar(car) {
            let carBlock = $('.client-car-block');
            finalizeS2Field(carBlock.find('#visits-client_car_make'), {id: car.make_id, text: car.make_name});
            finalizeS2Field(carBlock.find('#visits-client_car_model'), {id: car.model_id, text: car.model_name});
            finalizeS2Field(carBlock.find('#visits-client_car_chassis'), {id: car.chassis_id, text: car.chassis_name});
            finalizeS2Field(carBlock.find('#visits-client_car_engine'), {id: car.engine_id, text: car.engine_name});
            finalizeS2Field(carBlock.find('#visits-client_car_engine_model'), {id: car.engine_model, text: car.engine_model}, false);

            if (car.gearbox_id) {
                finalizeS2Field(carBlock.find('#visits-client_car_gearbox'), {id: car.gearbox_id, text: car.gearbox_name});
            } else {
                this.initGearboxSelect();
            }

            carBlock.find('#visits-client_car_sepcs')
                .removeClass('hidden')
                // .attr('disabled', true)
                .val(car.name)
            ;

            if (car.vin_number !== null) {
                carBlock
                    .find('#visits-vin_number')
                    .removeClass('hidden')
                    // .attr('disabled', true)
                    .val(car.vin_number)
                ;
            }
            if (car.gos_number !== null) {
                carBlock
                    .find('#visits-gos_number')
                    .removeClass('hidden')
                    // .attr('disabled', true)
                    .val(car.gos_number)
                ;
            }

            this.initProductsForCar();

            if (this.selectedRow.product.id !== null) {
                finalizeS2Field($('#visits-products'), {
                    id: this.selectedRow.product.id,
                    text: this.selectedRow.product.name + ' (' + this.selectedRow.product.price + ' ₽)'
                }, false, true);
            }
        },
        initProductsForCar() {
            let productSelector = $('#visits-products');
            let self = this;

            if (productSelector.hasClass("select2-hidden-accessible")) {
                productSelector.val(null).trigger('change');
                productSelector.select2('destroy');
                productSelector.off('select2:select');
            }

            productSelector.select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/product/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            model_id: self.selectedRow.car.model_id,
                            make_id: self.selectedRow.car.make_id,
                            chassis_id: self.selectedRow.car.chassis_id,
                            engine_id: self.selectedRow.car.engine_id,
                            gearbox_id: self.selectedRow.car.gearbox_id,
                            term: params.term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите Продукт для выбранной машины',
                minimumInputLength: 0,
                minimumResultsForSearch: 10,
            }).on('select2:select', function (e) {
                self.selectedRow.product.id = e.params.data.id;
                self.selectedRow.product.name = e.params.data.text;
                self.selectedRow.real_amount = null;
            });
        },
        saveVisit: function (rowData) {
            this.selectedRow.visit_at =  $('#visits-visit_at').val();
            this.selectedRow.product.id =  $('#visits-products').val();
            this.selectedRow.car.id =  $('#visits-car_id').val();
            this.selectedRow.location.id =  $('#visits-location').val();
            this.selectedRow.car.engine_model =  $('#visits-client_car_engine_model').val();
            let self = this;
            let url = '/visit/create';
            let param = $('meta[name=csrf-param]').attr("content");
            let token = $('meta[name=csrf-token]').attr("content");
            let body = {
                Visits: {
                    id: self.selectedRow.visit_id,
                    external_id: self.selectedRow.externalId,
                    real_amount: self.selectedRow.real_amount,
                    is_amount_confirmed: self.selectedRow.isAmountConfirmed,
                    is_landing_paid: self.selectedRow.isLandingPaid,
                    client_name: self.selectedRow.client.full_name,
                    client_phone: self.selectedRow.client.phone,
                    client_id: self.selectedRow.client.id,
                    location_id: self.selectedRow.location.id,
                    car_id: self.selectedRow.car.id,
                    gearbox_id: self.selectedRow.car.gearbox_id,
                    client_car_model : self.selectedRow.car.model_id,
                    model_id : self.selectedRow.car.model_id,
                    make_id : self.selectedRow.car.make_id,
                    engine_id : self.selectedRow.car.engine_id,
                    vin_number : self.selectedRow.car.vin_number,
                    engine_model : self.selectedRow.car.engine_model,
                    gos_number : self.selectedRow.car.gos_number,
                    current_mileage : self.selectedRow.car.current_mileage,
                },
                VisitsProducts: {
                    visit_at: this.formatDate(self.selectedRow.visit_at, 'Y-MM-DD HH:mm:ss'),
                    product_id: self.selectedRow.product.id,
                    category_id: self.selectedRow.category.id,
                    comment: self.selectedRow.comment,
                },
                Parts: self.selectedRow.parts
            };

            if (self.selectedRow.isNewRow === false) {
                url = '/visit/update';
            }

            body[param] = token;

            this.$http.post(url, body, {
                emulateJSON: true
            }).then(function (response) {
                self.refreshTable();
                resetCarsBlock();
                $('#myModal').modal('toggle');
            }, function (response) {
                let errors = '';
                let title = '';

                if (response.status === 400) {
                    title = "Необходимо правильно внести данные:";
                    for (let key in response.body){
                        if(response.body.hasOwnProperty(key)){
                            errors += `${response.body[key]}` + '\n';
                        }
                    }
                } else {
                    errors = response.data.message;
                    title = 'Ошибка';
                }

                swal({
                    title: title,
                    text: errors,
                    icon: "error",
                });
            });
        },
        deleteVisit: function (id) {
            let self = this;
            let url = '/visit/delete';
            let param = $('meta[name=csrf-param]').attr("content");
            let token = $('meta[name=csrf-token]').attr("content");
            let body = {
                id:id,
            };

            body[param] = token;

            this.$http.post(url, body, {
                emulateJSON: true
            }).then(function (response) {
                self.refreshTable();
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        getCarById(id, cars) {
            return cars.find((element) => {
                return parseInt(element.id) === parseInt(id);
            });
        },
        renderClientCarsBlock: function(cars) {
            let carBlock = $('.client-car-block');

            if ((cars.length === 1)) {
                this.selectedRow.car.id = cars[0].id;
                this.initializeAddCar();
                this.renderSelectedCar(cars[0]);
            } else {
                this.initializeAddCar();
            }
        },
        initializeAddCar: function() {
            let carBlock = $('.client-car-block');
            let carMake = $("#visits-client_car_make");
            let carModel = $("#visits-client_car_model");
            let carChassis = $("#visits-client_car_chassis");
            let carEngine = $("#visits-client_car_engine");
            let carEngineModel = $("#visits-client_car_engine_model");
            let carGearbox = $("#visits-client_car_gearbox");
            let self = this;

            if (carMake.hasClass("select2-hidden-accessible")) {
                carMake.select2('destroy');
                carMake.off('select2:select');
            }

            if (carModel.hasClass("select2-hidden-accessible")) {
                carModel.select2('destroy');
                carModel.off('select2:select');
            }

            if (carChassis.hasClass("select2-hidden-accessible")) {
                carChassis.select2('destroy');
                carChassis.off('select2:select');
            }

            if (carEngine.hasClass("select2-hidden-accessible")) {
                carEngine.select2('destroy');
                carEngine.off('select2:select');
            }

            if (carEngineModel.hasClass("select2-hidden-accessible")) {
                carEngineModel.select2('destroy');
                carEngineModel.off('select2:select');
            }

            if (carGearbox.hasClass("select2-hidden-accessible")) {
                carGearbox.select2('destroy');
                carGearbox.off('select2:select');
            }

            this.initGearboxSelect();

            carModel.select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/car-model-variant/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            model: params.term,
                            make_id: self.selectedRow.car.make_id,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите модель',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                carEngine.val(null).trigger('change');
                carEngineModel.val(null).trigger('change');
                carChassis.val(null).trigger('change');
                carGearbox.val(null).trigger('change');

                vue.$emit('car-block:field-selected', 'model_id', e.params.data.id);
            });

            carChassis.val(null).select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/chassis/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            model: params.term,
                            model_id: self.selectedRow.car.model_id,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите кузов',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                carEngine.val(null).trigger('change');
                carEngineModel.val(null).trigger('change');
                carGearbox.val(null).trigger('change');

                vue.$emit('car-block:field-selected', 'chassis_id', e.params.data.id);
            });

            carEngine.val(null).select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/engine/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            model: params.term,
                            chassis_id: self.selectedRow.car.chassis_id,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите двигатель',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                vue.$emit('car-block:field-selected', 'engine_id', e.params.data.id);
            });


            carEngineModel.val(null).select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/car/get-engine-models",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            model: params.term,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите модель двигателя',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                vue.$emit('car-block:field-selected', 'engine_model', e.params.data.id);
            });

            carMake.select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/car-make-variant/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            make: params.term, // search term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите марку',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                carModel.val(null).trigger('change');
                carGearbox.val(null).trigger('change');
                carChassis.val(null).trigger('change');
                carEngine.val(null).trigger('change');

                vue.$emit('car-block:field-selected', 'make_id', e.params.data.id);
            });

            carBlock.find('#visits-client_car_make').removeClass('hidden').removeAttr('disabled');
            carBlock.find('#visits-client_car_model').removeClass('hidden').val(null).removeAttr('disabled');
            carBlock.find('#visits-client_car_chassis').removeClass('hidden').val(null).removeAttr('disabled');
            carBlock.find('#visits-client_car_engine').removeClass('hidden').val(null).removeAttr('disabled');
            carBlock.find('#visits-client_car_engine_model').removeClass('hidden').val(null).removeAttr('disabled');
            carBlock.find('#visits-vin_number').removeClass('hidden').val('').removeAttr('disabled');
            carBlock.find('#visits-gos_number').removeClass('hidden').val('').removeAttr('disabled');
            carBlock.find('#visits-current_mileage').removeClass('hidden').val('').removeAttr('disabled');
        },
        initGearboxSelect: function() {
            let carGearbox = $("#visits-client_car_gearbox");
            let self = this;

            if (carGearbox.hasClass("select2-hidden-accessible")) {
                carGearbox.select2('destroy');
                carGearbox.off('select2:select');
            }

            carGearbox.prop("disabled", false);

            carGearbox.select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/gearboxes/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        return {
                            make_id: self.selectedRow.car.make_id,
                            model_id: self.selectedRow.car.model_id,
                            chassis_id: self.selectedRow.car.chassis_id,
                            name: params.term, // search term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите коробку',
                minimumInputLength: 0,
            }).on('select2:select', function (e) {
                gearboxSelected = e.params.data.id;

                vue.$emit('car-block:field-selected', 'gearbox_id', gearboxSelected, false);
            });
        }
    },
    events: {
        'client-block:phone-input' : function (phone) {
            this.selectedRow.client.phone = phone;
            this.getClient();
        },
        'client-block:phone-change' : function () {
            this.selectedRow.car = { ...this.defaultRow.car };
            this.cars = [];
            resetCarsBlock();
        },
        'car-block:field-selected' : function (name, value, changeProduct = true) {
            let carFieldValue = parseInt(value);

            if (!isNaN(carFieldValue)) {
                this.selectedRow.car[name] = carFieldValue;

                if (changeProduct === true) {
                    this.initProductsForCar();
                }
            }
        },
        'car-block:vin-selected' : function (value) {
            this.selectedRow.car.vin_number = value;
        },
        'car-block:model-selected' : function (data) {
            let carModelId = parseInt(data);

            if (!isNaN(carModelId)) {
                this.selectedRow.car.model_id = carModelId;
                this.initProductsForCar();
            }
        },
        'vuetable:row-changed': function(data) {
            // console.log('row-changed:', data.name)
        },
        'vuetable:row-clicked': function(data, event) {
            // console.log('row-clicked:', data.id)
        },
        'vuetable:cell-clicked': function(data, field, event) {
            // console.log('cell-clicked:', field.name)

            if (field.name === 'id') {
                this.$broadcast('vuetable:toggle-detail', data.id)

                return;
            }
        },
        'vuetable:action': function(action, data) {
            if(action === 'generate-order') {
                if (data.isNewRow === true || !['paid', 'done'].includes(data.status)) {
                    swal({
                        title: 'Визит',
                        text: 'Еще не оплачен',
                        icon: "error",
                    });
                    return;
                }
                window.location = '/visit/generate-order?id=' + data.id;
            } else  if (action === 'view-item') {
                window.location = '/visit/view?id=' + data.visit_id;
            } else if (action === 'complete-item') {
                if (data.isNewRow === true || data.completed_at !== null) {
                    swal({
                        title: 'Визит',
                        text: 'Уже завершен',
                        icon: "success",
                    });

                    return;
                }

                self = this;
                $.ajax({
                    url: '/visit/complete',
                    data: {
                        'id': data.id,
                    },
                    type: 'POST',
                    success: function (response) {
                        data.status = response.status;

                        swal({
                            title: 'Визит',
                            text: 'Успешно завершен',
                            icon: "success",
                        });
                        self.refreshTable();
                    },
                    error: function (error, eer) {
                        let response = error.responseJSON;

                        swal({
                            title: 'Визит',
                            text: response.error || response.message,
                            icon: "error",
                        });
                    }
                });
            } else if (action === 'edit-item') {
                this.renderEditView(data);
            } else if (action === 'delete-item') {
                swal({
                    title: "Уверены ли вы?",
                    text: "Что хотите удалить этот визит",
                    icon: "warning",
                    buttons: ["Нет", "Да, удалить"],
                    dangerMode: true,
                }).then((result) => { // <--
                    if (result) { // <-- if confirmed
                        this.deleteVisit(data.id);
                    }
                });
            } else if (action === 'copy-item') {
                this.addNewRow(data);
            } else if (action === 'add-item') {
                this.addNewRow();
            }  else {
                swal({
                    title: action,
                    text: data.name,
                    icon: "success",
                });
            }
        },
        'vuetable:load-success': function(response) {
        },
        'vuetable:load-error': function(response) {
            if (response.status == 400) {
                swal({
                    title: 'Что-то пошло не так!',
                    text: response.data,
                    icon: "error",
                });
            } else {
                swal({
                    title: 'Что-то пошло не так!',
                    text: E_SERVER_ERROR,
                    icon: "error",
                });
            }
        },
    }
});

$( document ).ready(function() {
    let clientIdSelector = $('#visits-client_id');
    var self = this;
    this.client = {};
    this.cars = [];
    this.selectedCar = {};

    initPhoneMask();

    clientIdSelector.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/client/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    id: params.term,
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите Пользователя',
        minimumInputLength: 0,
    });
    clientIdSelector.on('select2:select', function(event){
        let clientId = event.params.data.id;

        this.initializeClient(clientId);
    });

});

function initPhoneMask(){
    var url = "/js/phone-codes.json";
    var maskOpts = {
        inputmask: {
            definitions: {
                "#": {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            showMaskOnHover: false,
            autoUnmask: true,
            clearMaskOnLostFocus: false,
            oncomplete: function() {
                let phone = $('.client-input-phone').val();

                vue.$emit('client-block:phone-input', phone);
            },
            onincomplete: function() {
                vue.$emit('client-block:phone-change');
            },
            oncleared: function() {
                vue.$emit('client-block:phone-change');
            }
        },
        match: /[0-9]/,
        replace: "#",
        listKey: "mask"
    };

    $.ajax({
        url: url,
        dataType: "json",
        success: function(response) {
            let listCountries = $.masksSort(response, ["#"], /[0-9]|#/, "mask");
            let selector =  $('.client-input-phone');

            selector.inputmasks(
                $.extend(true, {}, maskOpts, {
                    list: listCountries
                })
            )

            $(document).on('blur', '.client-input-phone', function(){
                if (selector.inputmask("isComplete")){
                    vue.$emit('client-block:phone-input', selector.val());
                }
            });
        },
        error: function(err){
            console.log('error', err);
        }
    });
}

function renderClientFields(client) {
    let clientBlock = $('.client-info-block');
    clientBlock.find('#visits-client_phone').removeClass('hidden').val(client.phone);
    clientBlock
        .find('#visits-client_name')
        .removeClass('hidden')
        .val(client.name)
        // .attr('disabled', true)
    ;
}

function finalizeS2Field(selector, data, disable = true, selected = true, ) {
    selector.removeClass('hidden');

    if (selector.find("option[value='" + data.id + "']").length) {
        if (selected === true) {
            selector.find("option[value='" + data.id + "']").attr('selected', selected);
            selector.find("option[value='" + data.id + "']").val(data.id);
        }

        selector.val(data.id).trigger('change');
    } else {
        let newOption = new Option(data.text, data.id, selected, selected);

        selector.append(newOption).trigger('change');
    }

    if (disable) {
        selector.prop("disabled", disable);
    }
}

function resetCarsBlock() {
    $('#visits-client_car_make').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-client_car_model').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-client_car_chassis').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-client_car_engine').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-client_car_engine_model').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-client_car_gearbox').empty().val(null).trigger('change').attr('disabled', false);
    $('#visits-vin_number').removeClass('hidden').val('');
    $('#visits-gos_number').removeClass('hidden').val('');
    $('#visits-current_mileage').removeClass('hidden').val('').attr('disabled', false);
}
