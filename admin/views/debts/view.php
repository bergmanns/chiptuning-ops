<?php

use admin\models\Debts;
use admin\models\User;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Debts */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['location/index']];
$this->params['breadcrumbs'][] = ['label' => 'Долги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$role = Yii::$app->user->getIdentity()->role;
YiiAsset::register($this);

?>
<div class="debts-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::a('Погасить', ['repayment', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php if ($role === User::ROLE_ADMIN) {
            echo Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]);
        } ?>
    </p>

    <p>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type_id',
                'value' => function ($model) {
                    /* @var $model Debts */
                    if ($model->type !== null) {
                        return $model->type->name;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'category_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Debts */
                    if ($model->category !== null) {
                        $html = Html::a($model->category->name, ['categories/view', 'id' => $model->category_id]);
                        return $html;
                    }

                    return null;
                },
            ],

            [
                'attribute' => 'location_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Debts */
                    if ($model->location !== null) {
                        $html = Html::a($model->location->name, ['location/view', 'id' => $model->location_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'paid_by',
            'name',
            'amount',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
