<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\CarChassisSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-models-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'make') ?>

    <?= $form->field($model, 'model') ?>

    <?= $form->field($model, 'body') ?>

    <?= $form->field($model, 'stock_power') ?>

    <?php // echo $form->field($model, 'injection') ?>

    <?php // echo $form->field($model, 'engine_name') ?>

    <?php // echo $form->field($model, 'gearbox_model') ?>

    <?php // echo $form->field($model, 'gearbox_vers') ?>

    <?php // echo $form->field($model, 'engine_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
