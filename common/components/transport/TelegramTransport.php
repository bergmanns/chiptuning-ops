<?php

declare(strict_types = 1);

namespace common\components\transport;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;

class TelegramTransport extends Component
{
    public const HTML_PARSE_MODE = 'HTML';
    public const MARKDOWN_PARSE_MODE = 'Markdown';

    /**
     * @var string
     */
    public $botToken;

    /**
     * @var null|array|string
     */
    public $proxy;
    /**
     * @var Client
     */
    private $httpClient;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        $this->validateConfig();
        $this->initializeClient();
    }

    /**
     * @param string $chatId
     * @param string $message
     * @param array $options
     * @return Response
     * @throws ServerErrorHttpException
     */
    public function sendMessage(string $chatId, string $message, array $options = []): ResponseInterface
    {
        $formData = [
            'chat_id' => $chatId,
            'text' => $message,
        ];

        if (empty($options) === false) {
            $formData = ArrayHelper::merge($formData, $options);
        }

        try {
            $response = $this->sendRequest(
                'POST',
                'sendMessage',
                [
                    'form_params' => $formData,
                ]
            );

            return $response;
        } catch (GuzzleException $e) {
            Yii::error($e);

            throw new ServerErrorHttpException($e->getMessage());
        }
    }

    /**
     * @throws InvalidConfigException
     */
    private function validateConfig()
    {
        if ($this->botToken === null) {
            throw new InvalidConfigException('The "botToken" option must be specified.');
        }
    }


    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return Response
     * @throws GuzzleException
     */
    private function sendRequest(string $method, string $uri, array $options = []): Response
    {
        return $this->httpClient->request(
            $method,
            $uri,
            $options
        );
    }

    private function initializeClient(): void
    {
        $parameters = [
            'base_uri' => sprintf('https://api.telegram.org/bot%s/', $this->botToken),
            'allow_redirects' => false,
            'exceptions' => false,
        ];

       if ($this->proxy !== null) {
            $parameters['proxy'] = $this->proxy;
        }

        $this->httpClient = new Client($parameters);
    }
}
