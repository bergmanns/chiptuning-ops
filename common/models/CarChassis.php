<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "car_models".
 *
 * @property int $id
 * @property int $make_id
 * @property int $model_id
 * @property string|null $name
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Cars[] $cars
 * @property Products[] $products
 * @property CarMakeVariants $make
 * @property CarModelVariants $model
 * @property CarEngines[] $engines

 */
class CarChassis extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_chassis';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'softdeleteable' => [
                'class' => 'common\behaviors\SoftDeleteableBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_id', 'model_id'], 'integer'],
            [['make_id', 'model_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'make_id' => Yii::t('app', 'Марка'),
            'model_id' => Yii::t('app', 'Модель'),
            'name' => Yii::t('app', 'Кузов'),
        ];
    }

    public function getSpecific(): string
    {
        return $this->name;
    }


    public function getName(): string
    {
        return $this->model->name . ' ' .  $this->getSpecific();
    }
    /**
     * @return ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Cars::class, ['model_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::class, ['car_model_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMakeVariants::class, ['id' => 'make_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModelVariants::class, ['id' => 'model_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEngines()
    {
        return $this->hasMany(CarEngines::class, ['chassis_id' => 'id']);
    }
}
