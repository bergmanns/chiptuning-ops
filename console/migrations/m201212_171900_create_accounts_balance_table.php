<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%accounts_balance}}`.
 */
class m201212_171900_create_accounts_balance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%accounts_balance_history}}', [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer(),
            'change_by' => $this->integer(),
            'old_balance' => $this->float(),
            'new_balance' => $this->float(),
            'created_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'accounts_balance_history_payment_id_key',
            'accounts_balance_history',
            'payment_id',
            'payments',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'accounts_balance_history_change_by_key',
            'accounts_balance_history',
            'change_by',
            'staff',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%accounts_balance_history}}');
    }
}
