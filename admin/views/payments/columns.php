<?php

use admin\widgets\DropDownList;
use common\models\{Accounts, Categories, CategoryTypes, Locations, Payments};
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $searchModel admin\models\DebtsSearch */
/* @var $role string */

return [
    'id',
    'name',
    [
        'attribute' => 'account_id',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'account_id',
            'data' => ArrayHelper::map(Accounts::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Payments */
            return $model->account->name ?? null;
        },
    ],
    [
        'attribute' => 'location_id',
        'label' => 'Филиал',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'location_id',
            'data' => ArrayHelper::map(Locations::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Payments */
            return $model->location->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'type_id',
        'label' => 'Тип',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'type_id',
            'data' => ArrayHelper::map(CategoryTypes::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Payments */
            return $model->type->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'category_id',
        'label' => 'Категория',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'category_id',
            'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Payments */
            return $model->category->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'status',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'status',
            'data' => [
                Payments::STATUS_NOT_PAID => 'Не оплачен',
                Payments::STATUS_CANCEL => 'Отменен',
                Payments::STATUS_LOCAL_PAY => 'Оплачен наличными',
                Payments::STATUS_PAID => 'Оплачен',
            ],
        ]),
        'value' => function ($model) {
            /* @var $model Payments */

            switch ($model->status) {
                case Payments::STATUS_NOT_PAID:
                    return "Не оплачен";
                case Payments::STATUS_LOCAL_PAY:
                    return "Оплачен наличными";
                case Payments::STATUS_PAID:
                    return "Оплачен";
                case Payments::STATUS_CANCEL:
                    return "Отменен";
            }

            return null;
        },
    ],
    [
        'attribute' => 'amount',
        'format' => 'currency',
        'headerOptions' => [
            'style' => 'width: 5%',
        ],
    ],
    [
        'attribute' => 'created_at',
        'format' => 'datetime',
        'filter' => DateRangePicker::widget([
            'model' => $searchModel,
            'attribute' => 'created_at',
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'Y-m-d H:i',
                ],
            ],
        ]),
    ],
    //'updated_at',

    [
        'class' => 'admin\components\grid\ActionWithAccessColumn',
        'role' => $role,
        'template' => '{delete}'
    ],
];