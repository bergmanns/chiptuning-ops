<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarChassis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-models-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'make_id')->dropDownList([
            $model->make_id ?? null => $model->make->name ?? null
     ], [
            'class' => 'car-make-selector form-control',
    ])?>

<!--    todo Пофиксить предзаполнение при редактировании Кузова-->
    <?= $form->field($model, 'model_id')->dropDownList([
        $model->model_id ?? null => $model->model->name ?? null
    ], [
        'class' => 'car-model-variants-selector form-control',
    ])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
