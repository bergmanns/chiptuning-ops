<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_phones}}`.
 */
class m200130_163936_create_client_phones_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_phones}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'client_phones__key',
            'client_phones',
            'client_id',
            'clients',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_phones}}');
    }
}
