<?php

declare(strict_types = 1);

namespace admin\actions\debts;

use admin\models\Debts;
use Carbon\CarbonImmutable;
use common\models\Payments;
use common\models\Visits;
use Yii;
use yii\base\Action;
use yii\db\Expression;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class RepaymentAction extends Action
{
    /**
     * Updates an existing Debts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function run($id = null)
    {

        if (empty($id) === false) {
            try {
                $model = $this->controller->findModel((int) $id);
            } catch (NotFoundHttpException $e) {
            }
        } else {
            $model = new Debts();
        }

        $model->load(Yii::$app->request->post());

        if (Yii::$app->request->isPost === true) {
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
            $payments = Yii::$app->request->post('payments', []);
            $fullPayment = array_sum(ArrayHelper::getColumn($payments, 'value'));

            if ($fullPayment <= 0) {
                $model->addError('amount', 'Сумма погашения слишком мала');
                $transaction->rollBack();

                return $this->controller->render('repayment', [
                    'model' => $model,
                ]);
            }

            $openDebts = Debts::find()->where([
                'location_id' => $model->location_id,
                'type_id' => $model->type_id,
                'category_id' => $model->category_id,
                'deleted_at' => null,
            ])
                ->andWhere([
                    '<>',
                    'amount',
                    new Expression('paid_amount'),
                ])
                ->all()
            ;

            if (empty($openDebts) === true) {
                $model->addError('amount', 'Нет долгов к погашению');
                $transaction->rollBack();

                return $this->controller->render('repayment', [
                    'model' => $model,
                ]);
            }

            foreach ($payments as $paymentVars) {
                $paymentRemaining = ArrayHelper::getValue($paymentVars, 'value', 0);
                $paymentRemaining = (float) str_replace(' ', '', $paymentRemaining);

                /** @var Debts $debt */
                foreach ($openDebts as $debt) {
                    if ($debt->amount === $debt->paid_amount) {
                        continue; //todo Сделать уменьшение массива, а то бегает для каждого аккаунта по всем долгам.
                    }

                    if ($paymentRemaining < 0) {
                        continue 2; // todo если на прошлой итерации ушли в минус
                    }

                    Yii::debug($paymentRemaining);
                    Yii::debug($debt->amount);
                    Yii::debug($debt->paid_amount);

                    $paymentRemaining = $paymentRemaining - ($debt->amount - $debt->paid_amount);

                    Yii::debug($debt->toArray());
                    Yii::debug($paymentRemaining);

                    if ($paymentRemaining >= 0.0) {
                        $debt->paid_amount = $debt->amount;

                        $visit = $debt->visit;

                        if ($visit !== null) {
                            $visit->status = Visits::STATUS_PAID;
                            $visit->fillStatusDates();
                            $debt->visitProduct->status = Visits::STATUS_PAID;
                            $debt->visitProduct->save();

                            $visit->save();
                        }
                    } elseif ($paymentRemaining < 0.0) {
                        $debt->paid_amount = $debt->amount + $paymentRemaining;
                    }

                    $debt->paid_by = Yii::$app->user->getId();
                    $debt->paid_at = CarbonImmutable::now();

                    Yii::debug($debt->toArray());

                    if ($debt->save() === false) {
                        Yii::debug($debt->getErrors());
                        $model->addError('amount', 'Ошибка сохранения оплаты');
                        $transaction->rollBack();

                        return $this->controller->render('repayment', [
                            'model' => $model,
                        ]);
                    }

                    $payment = $payment ?? new Payments();
                    $payment->type_id = $debt->type_id;
                    $payment->category_id = $debt->category_id;
                    $payment->location_id = $debt->location_id;
                    $payment->status = Payments::STATUS_PAID;
                    $payment->account_id = ArrayHelper::getValue($paymentVars, 'account_id');
                    $payment->amount = ArrayHelper::getValue($paymentVars, 'value');

                    if ($payment->save() === false) {
                        $model->addError('amount', 'Ошибка сохранения оплаты');

                        Yii::debug($payment->getErrors());
                        $transaction->rollBack();

                        return $this->controller->render('repayment', [
                            'model' => $model,
                        ]);
                    }

                    $debt->link('payments', $payment);

                    if ($paymentRemaining <= 0.0) {
                        $payment = new Payments();
                    }
                }
            }

            $transaction->commit();

            return $this->controller->redirect(['index']);
        }

        return $this->controller->render('repayment', [
            'model' => $model,
        ]);
    }
}
