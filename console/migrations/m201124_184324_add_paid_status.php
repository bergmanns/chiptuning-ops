<?php

use yii\db\Migration;

/**
 * Class m201124_184324_add_paid_status
 */
class m201124_184324_add_paid_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('visits', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201124_184324_add_paid_status cannot be reverted.\n";

        return false;
    }
}
