<?php

use yii\db\Migration;

/**
 * Class m201119_000119_add_table_debts_payments_relations.
 */
class m201119_000119_add_table_debts_payments_relations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%debts_payments_relation}}', [
            'id' => $this->primaryKey(),
            'debt_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'created_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'debts_payments_relation_debt_id_key',
            'debts_payments_relation',
            'debt_id',
            'debts',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'debts_payments_relation_payment_id_key',
            'debts_payments_relation',
            'payment_id',
            'payments',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%debts_payments_relation}}');
    }
}
