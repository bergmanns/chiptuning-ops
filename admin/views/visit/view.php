<?php

use admin\models\Visits;
use common\models\User;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Visits */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Визиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="visits-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
<!--        --><?//= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?// if (Yii::$app->user->can(User::ROLE_ADMIN)) {
//            echo Html::a('Delete', ['delete', 'id' => $model->id], [
//                'class' => 'btn btn-danger',
//                'data' => [
//                    'confirm' => 'Are you sure you want to delete this item?',
//                    'method' => 'post',
//                ],
//            ]);
//        }
//        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'car_id',
                'value' => function ($model) {
                    /* @var $model Visits*/
                    return sprintf('%s %s %s', $model->car->make->name, $model->car->modelVariant->name, $model->car->model->engine_name);
                },
            ],
            [
                'attribute' => 'client_id',
                'value' => function ($model) {
                    /* @var $model Visits*/
                    return sprintf('%s (%s)', $model->client->full_name, $model->client->phone);
                },
            ],

            [
                'attribute' => 'contact_id',
                'value' => function ($model) {
                    /* @var $model Visits*/
                    return sprintf('%s', $model->contact->username);
                },
            ],
            [
                'attribute' => 'location_id',
                'value' => function ($model) {
                    /* @var $model Visits*/
                    return sprintf('%s', $model->location->name);
                },
            ],
            'current_mileage',
            'status',
            'source',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
