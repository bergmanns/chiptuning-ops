<?php


namespace admin\models;

Use common\models\User as BaseUser;
use yii\helpers\ArrayHelper;

class User extends BaseUser
{
    public $new_password;
    public $new_password_confirm;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['new_password', 'new_password_confirm'], 'string'],
            ['new_password_confirm', 'compare', 'compareAttribute' => 'new_password', 'message' => 'Пароли не совпадают'],
        ]);
    }

    public function init()
    {
        $this->on(self::EVENT_AFTER_VALIDATE, [$this, 'updatePassword']);
        $this->on(self::EVENT_BEFORE_VALIDATE, [$this, 'clearPhone']);

        parent::init();
    }

    public function updatePassword()
    {
        if (empty($this->new_password) === false) {
            $this->setPassword($this->new_password);
        }
    }

    public function clearPhone()
    {
        $phone = preg_replace('/\D/', '', $this->phone);
        $this->phone = sprintf('+%s', $phone);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'new_password' => 'Новый пароль',
            'new_password_confirm' => 'Подтверждение нового пароля',
        ]);
    }
}