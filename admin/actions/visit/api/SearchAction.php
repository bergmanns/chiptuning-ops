<?php

declare(strict_types = 1);

namespace admin\actions\visit\api;

use admin\models\User;
use common\models\Visits;
use Carbon\CarbonImmutable;
use common\models\VisitsProducts;
use Throwable;
use Yii;
use yii\base\Action;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class SearchAction extends Action
{
    const INTERVAL_DAY = 'day';
    const INTERVAL_WEEK = 'week';
    const INTERVAL_MONTH = 'month';
    const INTERVAL_DEFAULT = self::INTERVAL_WEEK;
    const WORK_HOUR_END = 18;
    const WORK_HOUR_START = 10;


    /**
     * @var ActiveQuery
     */
    private $query;

    /**
     * @var array
     */
    private $get;
    private $interval;

    /**
     * @var CarbonImmutable
     */
    private $intervalInit;

    /**
     * @var CarbonImmutable
     */
    private $end;

    /**
     * @var CarbonImmutable
     */
    private $start;

    /**
     * @var User
     */
    private $user;


    /**
     * Creates a new Visits model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function run()
    {
        $this->initializeRequest();
        $this->initializeAccess();
        $this->initializeQuery();
        $this->initializeVisit();
        $this->initializeFilter();

        $result = [];
        $byDate = [];
        Yii::debug($this->query->createCommand()->getRawSql());

        /** @var VisitsProducts $visit */
        foreach ($this->query->each() as $visit) {
            if ($visit->client->deleted_at !== null) {
                continue;
            }

            try {
                $visitAt = CarbonImmutable::parse($visit->visit_at);
                $parts = $this->initializeParts($visit);
                $partsTotalPrice = $visit->getAllPartsPrice();
                $visit = [
                    'id' => $visit->id,
                    'externalId' => $visit->visit->external_id,
                    'isAmountConfirmed' => $visit->visit->is_amount_confirmed,
                    'isLandingPaid' => $visit->visit->is_landing_paid,
                    'real_amount' => $visit->visit->real_amount,
                    'payments' => [],
                    'isNewRow' => false,
                    'paid_at' => $visit->visit->paid_at,
                    'completed_at' => $visit->visit->completed_at,
                    'visit_id' => $visit->visit_id,
                    'visit_at' => $visit->visit_at,
                    'visit_month' => $visitAt->month,
                    'visit_day' => $visitAt->day,
                    'visit_year' => $visitAt->year,
                    'visit_hour' => $visitAt->hour,
                    'comment' => $visit->comment,
                    'status' => $this->getStatus($visit->visit),
                    'category' => [
                        'name' => $visit->category->name ?? null,
                        'id' => $visit->category->id ?? null,
                    ],
                    'location' => [
                        'id' => $visit->location->id,
                        'name' => $visit->location->name,
                        'country' => $visit->location->country->toArray(['short', 'gos_number_mask']),
                    ],
                    'car' => $this->getCar($visit),
                    'product' => [
                        'id' => $visit->product->id ?? null,
                        'name' => $visit->product->name ?? null,
                        'price' => $visit->product->price ?? null,
                        'total_price' => ($visit->visit->real_amount ?? $visit->product->price ?? null) + $partsTotalPrice,
                    ],
                    'client' => [
                        'phone' => $visit->client->defaultPhone->phone,
                        'id' => $visit->client->id,
                        'full_name' => $visit->client->full_name,
                    ],
                    'parts' => $parts,
                ];

                $byDate[$visitAt->year][$visitAt->month][$visitAt->day][$visitAt->hour][] = $visit;
            } catch (Throwable $e) {
                Yii::error($e);
            }
        }

        $count = count($result);
        $result = $this->createVisitTable($byDate);

        Yii::$app->response->format = Response::FORMAT_JSON;


        return [
            'total' => $count,
            'per_page' => $count,
            'current_page' => 1,
            'last_page' => 1,
//            'next_page_url' => '',
            'from' => 1,
            'to' => $count,
            'data' => $result,
        ];
    }

    private function initializeVisit(): void
    {
        $this->query->joinWith([
            'visit',
        ])->andWhere([
            'visits.deleted_at' => null,
        ]);

        if ($this->interval === self::INTERVAL_DAY) {
            $this->start = $this->intervalInit->startOfDay();
            $this->end = $this->intervalInit->endOfDay();
            $this->query->andWhere([
                'between',
                'visit_at',
                $this->start->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
                $this->end->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
            ]);
        }

        if ($this->interval === self::INTERVAL_WEEK) {
            $this->start = $this->intervalInit->startOfWeek();
            $this->end = $this->intervalInit->endOfWeek();
            $this->query->andWhere([
                'between',
                'visit_at',
                $this->start->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
                $this->end->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
            ]);
        }

        if ($this->interval === self::INTERVAL_MONTH) {
            $this->start = $this->intervalInit->startOfMonth();
            $this->end = $this->intervalInit->endOfMonth();
            $this->query->andWhere([
                'between',
                'visit_at',
                $this->start->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
                $this->end->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
            ]);
        }
    }

    private function initializeRequest(): void
    {
        $this->get = Yii::$app->request->get();
        $this->interval = ArrayHelper::getValue($this->get, 'interval', self::INTERVAL_DEFAULT);
        $current = ArrayHelper::getValue($this->get, 'current');

        if (empty($current) === false) {
            $this->intervalInit = CarbonImmutable::createFromTimestamp($current)->locale('ru_RU');
        } else {
            $this->intervalInit = CarbonImmutable::now()->locale('ru_RU');
        }
    }

    private function initializeQuery()
    {
        $this->query = VisitsProducts::find()->orderBy('visit_at');

        if ($this->user->role === User::ROLE_MANAGER) {
            $this->query->andWhere([
                'visits.location_id' => $this->user->location_id,
            ]);
        }
    }

    private function createVisitTable(array $result)
    {
        /** @var CarbonImmutable $currentDate */
        $currentDate = $this->start;
        $table = [];

        while ($currentDate->lessThanOrEqualTo($this->end)) {
            do {
                $visitForThisHour = ArrayHelper::getValue($result, [
                    $currentDate->year,
                    $currentDate->month,
                    $currentDate->day,
                    $currentDate->hour,
                ]);

                if (empty($visitForThisHour) === true) {
                    if ($this->interval !== self::INTERVAL_MONTH && ($currentDate->isPast() === false || $currentDate->isToday() === true) && $currentDate->hour >= self::WORK_HOUR_START && $currentDate->hour <= self::WORK_HOUR_END) {
                        $virtualId = ($virtualId ?? 0) + 1;
                        $table[] = $this->getVirtualVisit($virtualId, $currentDate);
                    }
                } else {
                    foreach ($visitForThisHour as $visit) {
                        $table[] = $visit;
                    }
                }
                $prevDate = $currentDate;
                $currentDate = $currentDate->addHour();

                if ($currentDate->day !== $prevDate->day) {
                    $prevTask = array_pop($table);

                    if (empty($prevTask) === false) {
                        $prevTask['isLastTaskInDay'] = true;
                        $table[] = $prevTask;
                    }
                }
            } while ($currentDate->hour !== 00);
        }

        return $table;
    }

    private function getVirtualVisit(int $id, CarbonImmutable $visitAt)
    {
        return [
            'id' => 'v' . (string) $id,
            'externalId' => null,
            'isAmountConfirmed' => false,
            'isLandingPaid' => false,
            'isNewRow' => true,
            'real_amount' => null,
            'paid_at' => null,
            'completed_at' => null,
            'visit_at' => $visitAt->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT),
            'visit_month' => $visitAt->month,
            'visit_day' => $visitAt->day,
            'visit_year' => $visitAt->year,
            'visit_hour' => $visitAt->hour,
            'comment' => '',
            'status' => 'create',
            'parts' => [
            ],
            'category' => [
                'name' => '',
                'id' => null,
            ],
            'reason' => [
                'name' => '',
                'id' => '',
            ],
            'location' => [
                'id' => '',
                'name' => '',
            ],
            'car' => $this->getEmptyCar(),
            'product' => [
                'id' => '',
                'name' => '',
                'price' => '',
                'total_price' => '',
            ],
            'client' => [
                'phone' => '',
                'id' => '',
                'full_name' => '',
            ]
       ];
    }

    private function initializeParts(VisitsProducts $visit)
    {
        return $visit->getParts()->select(['id', 'name', 'serial', 'count', 'amount'])->asArray()->all();
    }

    private function initializeAccess()
    {
        $this->user = Yii::$app->user->getIdentity();
    }

    private function initializeFilter()
    {
        $phoneFilter = ArrayHelper::getValue($this->get, 'phone');
        $phone = preg_replace('/\D/', '', $phoneFilter);

        if (empty($phone) === false) {
            $phoneFilter = sprintf('+%s', $phone);

            $this->query->joinWith([
                'client',
                'client.clientPhones'
            ])->andWhere([
                'like',
                'phone',
                $phoneFilter,
            ]);
        }
    }

    private function getCar(VisitsProducts $visit)
    {
        if ($visit->car === null) {
            return $this->getEmptyCar();
        }

        return [
            'id' => $visit->car->id,
            'full_name' => $visit->car->getSpecific(),
            'make_name' => $visit->car->make->name,
            'make_id' => $visit->car->make->id,
            'model_name' => $visit->car->model->name,
            'model_id' => $visit->car->model->id,
            'model_variant_id' => $visit->car->model->id,
            'engine_id' => $visit->car->engine_id,
            'gearbox_id' => $visit->car->gearbox_id,
//                        'engine_volume' => sprintf('%1.1f', $visit->car->model->engine_volume),
            'engine_name' => $visit->car->engine->fullName,
            'issue_year' => $visit->car->issue_year,
            'vin_number' => $visit->car->vin_number,
            'gos_number' => $visit->car->gos_number,
            'current_mileage' => $visit->visit->current_mileage,
        ];
    }

    private function getEmptyCar()
    {
        return [
            'id' => '-1',
            'full_name' => '',
            'make_name' => '',
            'make_id' => '',
            'model_name' => '',
            'model_id' => '',
            'model_variant_id' => '',
            'engine_name' => '',
            'engine_id' => '',
            'gearbox_id' => '',
            'issue_year' => '',
            'vin_number' => '',
            'gos_number' => '',
            'current_mileage' => '',
        ];
    }

    private function getStatus(Visits $visit): string
    {
        if ($visit->paid_at !== null && $visit->completed_at === null) {
            return 'paid_not_done';
        }

        return $visit->status;
    }
}
