<?php

use yii\db\Migration;

/**
 * Class m200927_103019_add_car_engines_table
 */
class m200927_103019_add_car_engines_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
         $this->createTable('car_engines', [
             'id' => $this->primaryKey(),
             'chassis_id' => $this->integer(),
             'hp' => $this->integer(),
             'nm' => $this->integer(),
             'name' => $this->string(),
             'created_at' => $this->dateTime(),
             'updated_at' => $this->dateTime(),
             'deleted_at' => $this->dateTime(),
         ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'car_engines_chassis_id_key',
            'car_engines',
            'chassis_id',
            'car_chassis',
            'id'
        );

        $this->dropForeignKey('cars_model_key', 'cars');
        $this->dropIndex('cars_model_key', 'cars');
        $this->renameColumn('cars', 'model_id', 'engine_id');
        $this->addForeignKey(
            'cars_engine_id_key',
            'cars',
            'engine_id',
            'car_engines',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropTable('car_engines');
    }
}
