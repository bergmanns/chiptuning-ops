<?php

use yii\db\Migration;

/**
 * Class m210730_192839_add_engine_model_column_to_cars_table
 */
class m210730_192839_add_engine_model_column_to_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cars', 'engine_model', $this->string(20)->after('vin_number')->null());
        $this->createIndex(
            'idx-cars-engine_model',
            'cars',
            'engine_model'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-cars-engine_model',
            'cars'
        );
        $this->dropColumn('cars', 'engine_model');
    }
}
