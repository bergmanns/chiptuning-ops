<?php

namespace admin\models;

use common\models\Cars;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * CarSearch represents the model behind the search form of `common\models\Cars`.
 */
class CarSearch extends Cars
{
    public $model_variant_id;
    public $make_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'chip_id', 'make_id', 'model_variant_id', 'chassis_id', 'engine_id'], 'integer'],
            [['issue_year', 'vin_number', 'engine_model', 'gos_number', 'eng_chip_status', 'gear_chip_status', 'files_link', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cars::find()->andWhere([
            'cars.deleted_at' => null,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->modifyQueryByRole($query);

        // grid filtering conditions
        $query->andFilterWhere([
            'cars.id' => $this->id,
            'cars.client_id' => $this->client_id,
            'cars.engine_id' => $this->engine_id,
            'cars.chip_id' => $this->chip_id,
            'cars.issue_year' => $this->issue_year,
            'cars.created_at' => $this->created_at,
            'cars.updated_at' => $this->updated_at,
            'cars.deleted_at' => $this->deleted_at,
        ]);

        if (empty($this->make_id) === false) {
            $query->joinWith(['chassis'])->andWhere([
                'car_chassis.make_id' => $this->make_id,
            ]);
        }

        if (empty($this->model_variant_id) === false) {
            $query->joinWith(['chassis'])->andWhere([
                'car_chassis.model_id' => $this->model_variant_id,
            ]);
        }

        if (empty($this->chassis_id) === false) {
            $query->joinWith(['chassis'])->andWhere([
                'car_chassis.id' => $this->chassis_id,
            ]);
        }

        $query->andFilterWhere(['like', 'vin_number', $this->vin_number])
            ->andFilterWhere(['like', 'engine_model', $this->engine_model])
            ->andFilterWhere(['like', 'gos_number', $this->gos_number])
            ->andFilterWhere(['like', 'eng_chip_status', $this->eng_chip_status])
            ->andFilterWhere(['like', 'gear_chip_status', $this->gear_chip_status])
            ->andFilterWhere(['like', 'files_link', $this->files_link]);
        Yii::debug($query->createCommand()->getRawSql());
        return $dataProvider;
    }

    private function modifyQueryByRole(ActiveQuery $query): ActiveQuery
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();

        if ($user->role === User::ROLE_ADMIN) {
            return $query;
        }

        $query->joinWith(['client.locations'])->andWhere([
            'locations.id' => $user->location_id,
        ]);

        return $query;
    }
}
