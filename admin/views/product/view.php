<?php

use common\models\Products;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = sprintf('%s', $model->name);
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'make_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Products */
                    if ($model->make !== null) {
                        $html = Html::a($model->make->name, ['car-make-variant/view', 'id' => $model->make_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'model_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Products */
                    if ($model->model !== null) {
                        $html = Html::a($model->model->name, ['car-model-variant/view', 'id' => $model->model_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Products */
                    if ($model->chassis !== null) {
                        $html = Html::a($model->chassis->name, ['chassis/view', 'id' => $model->chassis_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'engine_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Products */
                    if ($model->engine !== null) {
                        $html = Html::a($model->engine->fullName, ['engine/view', 'id' => $model->engine_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'gearbox_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Products */
                    if ($model->gearbox !== null) {
                        $html = Html::a($model->gearbox->name, ['gearboxes/view', 'id' => $model->gearbox_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'name',
            [
                'attribute' => 'link',
                'format' => 'html',
                'label' => 'Ссылка',
                'value' => function ($model) {
                    /* @var $model Products*/
                    return sprintf('<a href="%s" target="_blank">%s</a>', $model->link, $model->link);
                },
            ],
            'price',
            'description:ntext',
            'status',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
