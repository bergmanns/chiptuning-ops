<?php

namespace admin\models;

use common\models\Visits as BaseVisit;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Clients represents the model of `common\models\Visists`.
 */
class Visits extends BaseVisit
{
    public $client_phone;
    public $client_name;
    public $client_car_make;
    public $client_car_model;
    public $vin_number;
    public $gos_number;
    public $engine_model;
    public $payment_status;

    public const SCENARIO_UPDATE_BY_MANAGER = 'update_by_manager';
    public const SCENARIO_UPDATE_BY_ADMIN = 'update_by_admin';
    public const SCENARIO_CREATE_BY_ADMIN = 'create_by_admin';
    public const SCENARIO_CREATE_BY_MANAGER = 'create_by_manager';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_FIND, [$this, 'initializePhone']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['client_phone', 'client_name', 'client_car_make', 'client_car_model', 'vin_number', 'gos_number'], 'string'],
            [['payment_status'], 'boolean'],

            [['current_mileage'], 'integer', 'max' => 2000000],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'client_phone' => Yii::t('app', 'Телефон'),
            'client_name' => Yii::t('app', 'Имя'),
            'client_car_make' => Yii::t('app', 'Марка'),
            'client_car_model' => Yii::t('app', 'Модель'),
            'vin_number' => Yii::t('app', 'VIN'),
            'gos_number' => Yii::t('app', 'Гос. номер'),
            'payment_status' => Yii::t('app', 'Оплачено на сайте'),
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    public function isComplete(): bool
    {
        if (in_array($this->status, [Visits::STATUS_PAID, Visits::STATUS_DONE], true) === true) {
            return true;
        }

        return false;
    }

    protected function initializePhone(): void
    {
        $this->client_phone = $this->client->phone ?? null;
        $this->client_name = $this->client->full_name ?? null;
        $this->client_car_make = $this->car->make->name ?? null;
        $this->client_car_model = $this->car->modelVariant->name ?? null;
        $this->vin_number = $this->car->vin_number ?? null;
        $this->gos_number = $this->car->gos_number ?? null;
    }
}
