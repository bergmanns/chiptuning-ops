<?php

namespace admin\controllers;

use admin\models\Clients;
use admin\models\ClientSearch;
use common\helpers\PhoneHelper;
use common\models\Cars;
use common\models\ClientPhones;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ClientController implements the CRUD actions for Clients model.
 */
class ClientController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Clients models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * List info about Client.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionInfo($id)
    {
        $carsFormatted = [];
        $model = $this->findModel($id);
        $clientCars = $model
            ->getCars()->andWhere(['cars.deleted_at' => null])
//            ->joinWith(['make', 'model', 'chassis', 'engine'])
            //            ->select([
            //                'cars.id',
            ////                'car_model_variants.model_id',
            //                'car_model_variants.name as model_name',
            //                'car_make_variants.name as make_name',
            //                'car_engines.name as engine_name',
            //                'car_chassis.name as model_body',
            //                'vin_number',
            //                'gos_number'
            //            ])
        ;
        /** @var Cars $clientCar */
        foreach ($clientCars->each() as $clientCar) {
            $carsFormatted[] = [
                'id' => (int) $clientCar->id,
                'full_name' => $clientCar->getFullName(),
                'name' => $clientCar->getSpecific(),
                'make_name' => $clientCar->make->name,
                'make_id' => $clientCar->make->id,
                'model_name' => $clientCar->model->name,
                'model_id' => $clientCar->model->id,
                'model_variant_id' => $clientCar->model->id,
//                'engine_volume' => sprintf('%1.1f', $clientCar->model->engine_volume),
                'engine_name' => $clientCar->engine->fullName,
                'engine_id' => $clientCar->engine_id,
                'engine_model' => $clientCar->engine_model,
                'chassis_id' => $clientCar->chassis->id,
                'chassis_name' => $clientCar->chassis->name,
                'gearbox_id' => $clientCar->gearbox_id,
                'gearbox_name' => $clientCar->gearbox->name ?? '',
                'issue_year' => $clientCar->issue_year,
                'vin_number' => $clientCar->vin_number ?? '',
                'gos_number' => $clientCar->gos_number ?? '',
                'current_mileage' => $clientCar->getLastMileAge() ?? '',
            ];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'client' => [
                'id' => $model->id,
                'name' => $model->full_name,
                'phone' => $model->phone,
            ],
            'cars' => $carsFormatted,
        ];
    }

    /**
     * List info about Client.
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionGet($term = null)
    {
        $models = Clients::find()
            ->andFilterWhere([
                'like',
                'full_name',
                $term,
            ])
            ->select(['full_name as text', 'id'])
            ->asArray()
            ->all()
        ;

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $models;
    }

    /**
     * Get Client ID by phone.
     * @param $phone
     * @return mixed
     */
    public function actionGetByPhone($phone)
    {
        $phone = preg_replace('/[^0-9]/', '', $phone);
        $phone = sprintf('+%s', $phone);
        $clientPhone = ClientPhones::find()
            ->andFilterWhere([
                'phone' => $phone,
            ])
            ->one()
        ;

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $clientPhone->client_id ?? null;
    }

    /**
     * Displays a single Clients model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => $model->getCars(),
        ]);

        return $this->render('view', [
            'model' => $model,
            'carsProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Clients model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Clients();
        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $isLoaded = $model->load(Yii::$app->request->post());
            $isValid = $model->validate();
            $isSaved = $model->save();

            Yii::warning([
                'load' => $isLoaded,
                'isValid' => $isValid,
                'isSaved' => $isSaved,
            ]);

            if ($isLoaded === false || $isValid === false || $isSaved === false) {
                $transaction->rollBack();

                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            $clientPhone = new ClientPhones();
            $clientPhone->phone = $model->phone;
            $clientPhone->client_id = $model->id;
            $clientPhone->save();

            $model->default_phone_id = $clientPhone->id;

            if ($model->save()) {
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } catch (Throwable $e) {
            Yii::error($e);

            $transaction->rollBack();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Clients model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        /** @var Clients $model */
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

        if ($request->isPost === true) {
            try {
                $post = $request->post();
                $clearPhone = PhoneHelper::getE164FormatterNumber(ArrayHelper::getValue($post, 'Clients.phone'));
                $clientPhone = ClientPhones::find()->where(['phone' => $clearPhone])->one();

                if ($clientPhone === null) {
                    $clientPhone = new ClientPhones();
                    $clientPhone->client_id = $model->id;
                }

                if ($clientPhone->client_id !== $model->id) {
                    $model->addError('phone', 'Пользователь с таким номером уже зарегистрирован');

                    throw new BadRequestHttpException('Validation error');
                }

                $clientPhone->phone = $clearPhone;
                $clientPhone->save();

                $model->default_phone_id = $clientPhone->id;
                $model->region_id = ArrayHelper::getValue($post, 'Clients.region_id');
                $model->full_name = ArrayHelper::getValue($post, 'Clients.full_name');
                $model->email = ArrayHelper::getValue($post, 'Clients.email');
                $model->drive2 = ArrayHelper::getValue($post, 'Clients.drive2');
                Yii::debug($model->toArray());
                $isValid = $model->validate();
                $isSaved = $model->save(false);

                Yii::warning([
                    'isValid' => $isValid,
                    'isSaved' => $isSaved,
                ]);

                if ($isValid === false || $isSaved === false) {
                    $transaction->rollBack();

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }

                if ($model->save()) {
                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } catch (Throwable $e) {
                Yii::error($e);

                $transaction->rollBack();
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clients model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clients model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clients the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clients::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
