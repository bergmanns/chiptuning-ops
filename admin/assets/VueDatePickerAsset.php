<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Vue date piker asset bundle.
 */
class VueDatePickerAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset//eonasdan-bootstrap-datetimepicker';

    public $css = [
        'build/css/bootstrap-datetimepicker.css',
    ];
    public $js = [
        'build/js/bootstrap-datetimepicker.min.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
    ];
}
