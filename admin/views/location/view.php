<?php

use admin\models\User;
use common\models\Locations;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Locations */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$role = Yii::$app->user->getIdentity()->role;

YiiAsset::register($this);

?>
<div class="locations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($role === User::ROLE_ADMIN) {
            echo Html::a('Изменить', ['location/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo Html::a('Удалить', ['location/delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]);
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'country_id',
                'label' => 'Страна',
                'value' => function ($model) {
                    /* @var $model Locations */
                    return $model->country->name ?? null;
                },
            ],
            [
                'attribute' => 'region_id',
                'value' => function ($model) {
                    /* @var $model Locations */
                    return $model->region->name ?? null;
                },
            ],
            'name',
            'share',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
