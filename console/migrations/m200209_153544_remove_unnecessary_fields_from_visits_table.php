<?php

use yii\db\Migration;

/**
 * Class m200209_153544_remove_unnecessary_fields_from_visits_table
 */
class m200209_153544_remove_unnecessary_fields_from_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('visit_reason_key', 'visits');
        $this->dropColumn('visits', 'category_id');
        $this->dropColumn('visits', 'mileage');
        $this->dropColumn('visits', 'visit_time');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('visits', 'category_id', $this->integer());
        $this->addColumn('visits', 'mileage', $this->integer());
        $this->addColumn('visits', 'visit_time', $this->dateTime());

        $this->addForeignKey(
            'visit_reason_key',
            'visits',
            'category_id',
            'visit_reasons',
            'id'
        );
    }
}
