<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%payments}}`.
 */
class m201119_001013_add_columns_to_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments', 'type_id', $this->integer()->after('visit_id'));
        $this->addColumn('payments', 'category_id', $this->integer()->after('type_id'));
        $this->addColumn('payments', 'location_id', $this->integer()->after('category_id'));

        $this->addForeignKey(
            'payments_type_id_key',
            'payments',
            'type_id',
            'category_types',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'payments_category_id_key',
            'payments',
            'category_id',
            'categories',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'payments_location_id_key',
            'payments',
            'location_id',
            'locations',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payments', 'type_id');
        $this->dropColumn('payments', 'category_id');
        $this->dropColumn('payments', 'location_id');
    }
}
