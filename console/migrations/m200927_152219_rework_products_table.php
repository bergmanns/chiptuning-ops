<?php

use yii\db\Migration;

/**
 * Class m200927_152219_rework_products_table
 */
class m200927_152219_rework_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('products_car_model_key', 'products');
        $this->renameColumn('products', 'car_model_id', 'chassis_id');
        $this->addColumn('products', 'make_id', $this->integer()->after('id'));
        $this->addColumn('products', 'model_id', $this->integer()->after('make_id'));
        $this->addColumn('products', 'engine_id', $this->integer()->after('chassis_id'));
        $this->addColumn('products', 'external_id', $this->integer()->after('id'));
        $this->addColumn('products', 'category_id', $this->integer()->after('engine_id'));

        $this->addForeignKey(
            'products_chassis_id_key',
            'products',
            'chassis_id',
            'car_chassis',
            'id'
        );

        $this->addForeignKey(
            'products_make_id_key',
            'products',
            'make_id',
            'car_make_variants',
            'id'
        );

        $this->addForeignKey(
            'products_model_id_key',
            'products',
            'model_id',
            'car_model_variants',
            'id'
        );

        $this->addForeignKey(
            'products_engine_id_key',
            'products',
            'engine_id',
            'car_engines',
            'id'
        );

        $this->addForeignKey(
            'products_category_id_key',
            'products',
            'category_id',
            'categories',
            'id'
        );

        $this->createIndex('products_external_id_idx', 'products', 'external_id', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200927_152219_rework_products_table cannot be reverted.\n";

        return false;
    }
}
