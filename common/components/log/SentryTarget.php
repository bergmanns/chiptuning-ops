<?php

declare(strict_types = 1);

namespace common\components\log;

use Sentry\Client;
use Sentry\ClientBuilder;
use Sentry\Severity;
use Sentry\State\Scope;
use Sentry\UserDataBag;
use Throwable;
use Yii;
use yii\helpers\VarDumper;
use yii\log\Logger;
use yii\log\Target;

class SentryTarget extends Target
{
    private const LOG_LEVELS = [
        Logger::LEVEL_ERROR => 'error',
        Logger::LEVEL_WARNING => 'warning',
        Logger::LEVEL_INFO => 'info',
        Logger::LEVEL_TRACE => 'debug',
        Logger::LEVEL_PROFILE_BEGIN => 'debug',
        Logger::LEVEL_PROFILE_END => 'debug',
    ];

    private const LOG_LEVEL_DEFAULT = 'debug';

    private const DEFAULT_LOG_TITLE = 'Untitled error';

    /**
     * @var string|null sentry client key
     */
    public $dsn;

    /**
     * @var Client|null
     */
    private $client;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->initializeClient();

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function export()
    {
        Yii::debug('Send Message ' . __CLASS__, __CLASS__);

        if ($this->client === null) {
            return;
        }

        // https://github.com/yiisoft/yii2/issues/16251
        $messages = static::filterMessages(
            $this->messages,
            $this->levels,
            $this->categories,
            $this->except
        );

        foreach ($messages as $message) {
            try {
                $this->processMessage($message);
            } catch (Throwable $error) {
                // Something went wrong while logging.
            }
        }
    }

    private function initializeClient(): void
    {
        if ($this->dsn === null) {
            Yii::debug('Client dsn not set ' . __CLASS__, __CLASS__);

            return;
        }

        $options = ['dsn' => $this->dsn, 'environment' => YII_ENV];
        $this->client = ClientBuilder::create($options)->getClient();
    }

    /**
     * Returns a text representation of a specified log level.
     *
     * @param int $levelCode log level code
     *
     * @return string text representation of a log level
     */
    private function getLevelName(int $levelCode): string
    {
        $levelName = self::LOG_LEVELS[$levelCode] ?? self::LOG_LEVEL_DEFAULT;

        return $levelName;
    }

    /**
     * Returns current user ID.
     *
     * @return string|int|null current user ID
     */
    private function getUserId()
    {
        $userId = null;

        try {
            $userId = Yii::$app->user->id;
        } catch (Throwable $error) {
            // Could not retrieve user ID.
        }

        return $userId;
    }

    /**
     * Processes a single log message.
     *
     * @param array $message log message
     */
    private function processMessage(array $messageData): void
    {
        $message = $messageData[0];
        $level = new Severity($this->getLevelName($messageData[1]));
        $category = $messageData[2];
        $traces = $messageData[4] ?? null;

        $scope = new Scope();
        $scope
            ->setLevel($level)
            ->setExtra('context', $this->getContextMessage())
            ->setExtra('traces', $traces)
            ->setTag('category', $category)
            ->setUser(new UserDataBag($this->getUserId()))
        ;

        if ($message instanceof Throwable) {
            $scope->setFingerprint([get_class($message), $message->getFile(), $message->getLine()]);
            $this->client->captureException($message, $scope);

            return;
        }

        if (is_array($message) === true) {
            $messageTitle = self::DEFAULT_LOG_TITLE;

            if (isset($message['message'])) {
                $messageTitle = $message['message'];

                unset($message['message']);
            }

            foreach ($message as $key => $value) {
                $scope->setExtra("$key", $value);
            }

            $scope->setFingerprint([json_encode(array_keys($message))]);
            $this->client->captureMessage($messageTitle, $level, $scope);

            return;
        }

        if (is_string($message)) {
            $scope->setFingerprint([$message]);
            $this->client->captureMessage($message, $level, $scope);

            return;
        }

        $message = VarDumper::export($message);

        $this->client->captureMessage($message, $level, $scope);
    }
}
