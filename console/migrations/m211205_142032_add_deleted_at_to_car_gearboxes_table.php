<?php

use yii\db\Migration;

/**
 * Class m211205_142032_add_deleted_at_to_car_gearboxes_table
 */
class m211205_142032_add_deleted_at_to_car_gearboxes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_gearboxes', 'deleted_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_gearboxes', 'deleted_at');
    }
}
