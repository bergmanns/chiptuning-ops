<?php

use Carbon\CarbonImmutable;
use common\models\Countries;
use yii\db\Migration;

/**
 * Class m210207_110007_alter_country_table_add_gosnumber_mask
 */
class m210207_110007_alter_country_table_add_gosnumber_mask extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('countries', 'gos_number_mask', $this->string()->after('name'));
        $this->addColumn('countries', 'short', $this->string()->after('gos_number_mask'));

        $country = Countries::findOne(['name' => ['Россия', 'РФ']]);

        if ($country !== null) {
            $country->name = 'Россия';
            $country->gos_number_mask = 'R ###RR #{2,3}';
            $country->short = 'RU';

            $country->save();
        } else {
            $this->insert('countries', [
                'name' => 'Россия',
                'gos_number_mask' => 'R ###RR #{2,3}',
                'short' => 'RU',
                'created_at' => CarbonImmutable::now(),
                'updated_at' => CarbonImmutable::now(),
            ]);
        }

        $country = Countries::findOne(['name' => 'Украина']);
        if ($country !== null) {
            $country->name = 'Украина';
            $country->gos_number_mask = 'aa #### aa';
            $country->short = 'UA';

            $country->save();
        } else {
            $this->insert('countries', [
                'name' => 'Украина',
                'gos_number_mask' => 'aa #### aa',
                'short' => 'UA',
                'created_at' => CarbonImmutable::now(),
                'updated_at' => CarbonImmutable::now(),
            ]);
        }

        $country = Countries::findOne(['name' => 'Беларусь']);
        if ($country !== null) {
            $country->name = 'Беларусь';
            $country->gos_number_mask = '#### BB-#';
            $country->short = 'BY';

            $country->save();
        } else {
            $this->insert('countries', [
                'name' => 'Беларусь',
                'gos_number_mask' => '#### BB-#',
                'short' => 'BY',
                'created_at' => CarbonImmutable::now(),
                'updated_at' => CarbonImmutable::now(),
            ]);
        }

        $this->insert('countries', [
            'name' => 'ДНР',
            'gos_number_mask' => 'a ###aa DPR',
            'short' => 'DPR',
            'created_at' => CarbonImmutable::now(),
            'updated_at' => CarbonImmutable::now(),
        ]);
        $this->insert('countries', [
            'name' => 'ЛНР',
            'gos_number_mask' => 'a ###aa LPR',
            'short' => 'LPR',
            'created_at' => CarbonImmutable::now(),
            'updated_at' => CarbonImmutable::now(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('countries', 'gos_number_mask');
        $this->dropColumn('countries', 'short');
    }
}
