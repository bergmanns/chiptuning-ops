<?php

use yii\db\Migration;

/**
 * Class m210802_162535_add_index_visit_at_column_to_visits_products_table
 */
class m210802_162535_add_index_visit_at_column_to_visits_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx_visits_products_visit_at',
            'visits_products',
            'visit_at'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx_visits_products_visit_at',
            'visits_products'
        );
    }
}
