<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category_types".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $active
 *
 * @property CashFlow[] $cashFlows
 * @property Categories[] $categories
 */
class CategoryTypes extends \yii\db\ActiveRecord
{
    const DEFAULT_TYPE_NAME = 'Визит';

    const PAYMENT_TYPE_NAME = 'Переводы';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_types';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active'], 'integer'],
            [['active'], 'default', 'value' => 1],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Название'),
            'active' => Yii::t('app', 'Активен'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashFlows()
    {
        return $this->hasMany(CashFlow::className(), ['type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['type_id' => 'id']);
    }

    public static function getDefaultType(): CategoryTypes
    {
        return self::getTypeByName(self::DEFAULT_TYPE_NAME);
    }

    public static function getTypeByName($name): CategoryTypes
    {
        $type = CategoryTypes::findOne(['name' => $name]);

        if ($type !== null) {
            return $type;
        }

        $type = new CategoryTypes();
        $type->name = $name;
        $type->active = 1;

        $type->save();

        return $type;
    }
}
