<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visits_products}}`.
 */
class m200209_151929_create_visits_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visits_products}}', [
            'id' => $this->primaryKey(),
            'visit_id' => $this->integer(),
            'product_id' => $this->integer(),
            'category_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'status' => "ENUM('new', 'active', 'done', 'cancelled')",
            'created_at' => $this->dateTime(),
            'payed_at' => $this->dateTime(),
            'visit_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'visit_products_reason_key',
            'visits_products',
            'category_id',
            'visit_reasons',
            'id',
            'SET NULL',
            null
        );

        $this->addForeignKey(
            'visit_products_key',
            'visits_products',
            'visit_id',
            'visits',
            'id'
        );

        $this->addForeignKey(
            'visit_products_product_key',
            'visits_products',
            'product_id',
            'products',
            'id'
        );

        $this->addForeignKey(
            'visit_products_payment_key',
            'visits_products',
            'payment_id',
            'payments',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visits_products}}');
    }
}
