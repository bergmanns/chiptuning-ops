<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int|null $external_id
 * @property int|null $make_id
 * @property int|null $model_id
 * @property int|null $engine_id
 * @property int|null $chassis_id
 * @property int|null $gearbox_id
 * @property int|null $category_id
 * @property int|null $stage
 * @property string|null $name
 * @property string|null $link
 * @property float|null $price
 * @property string|null $description
 * @property string|null $status
 * @property int|null $is_manual
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Chips[] $chips
 * @property CarChassis $chassis
 * @property CarEngines $engine
 * @property CarGearboxes $gearbox
 * @property CarMakeVariants $make
 * @property CarModelVariants $model
 * @property Categories $category
 */
class Products extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chassis_id', 'make_id', 'model_id', 'engine_id', 'category_id', 'gearbox_id', 'is_manual'], 'integer'],
            [['price'], 'number', 'min' => 0],
            [['description', 'status'], 'string'],
            [['status'], 'default', 'value' => 'active'],
            [['name', 'link'], 'string', 'max' => 255],
            [['make_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarMakeVariants::class, 'targetAttribute' => ['make_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModelVariants::class, 'targetAttribute' => ['model_id' => 'id']],
            [['chassis_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarChassis::class, 'targetAttribute' => ['chassis_id' => 'id']],
            [['engine_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarEngines::class, 'targetAttribute' => ['engine_id' => 'id']],
            [['gearbox_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarGearboxes::class, 'targetAttribute' => ['gearbox_id' => 'id']],
            [['price', 'name', 'category_id', 'make_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'chassis_id' => Yii::t('app', 'Кузов'),
            'make_id' => Yii::t('app', 'Марка'),
            'model_id' => Yii::t('app', 'Модель'),
            'engine_id' => Yii::t('app', 'Двигатель'),
            'gearbox_id' => Yii::t('app', 'Коробка'),
            'category_id' => Yii::t('app', 'Категория'),
            'name' => Yii::t('app', 'Название'),
            'link' => Yii::t('app', 'Ссылка'),
            'price' => Yii::t('app', 'Цена'),
            'description' => Yii::t('app', 'Описание'),
            'status' => Yii::t('app', 'Статус'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
            'deleted_at' => Yii::t('app', 'Удален'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getChips()
    {
        return $this->hasMany(Chips::class, ['product_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEngine()
    {
        return $this->hasOne(CarEngines::class, ['id' => 'engine_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChassis()
    {
        return $this->hasOne(CarChassis::class, ['id' => 'chassis_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMakeVariants::class, ['id' => 'make_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModelVariants::class, ['id' => 'model_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getGearbox()
    {
        return $this->hasOne(CarGearboxes::class, ['id' => 'gearbox_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }

    public function getHQShare(Locations $location,  ? int $realPrice = null) :  ? float
    {
        if ($realPrice === null) {
            $realPrice = $this->price;
        }

        return $realPrice * (1 - $location->share / 100);
    }

    public function getShortName()
    {
        $dataName = explode(' ', $this->name);
        $dataName = array_slice($dataName, 0, 2);

        return implode(' ', $dataName);
    }

    public function isGearbox(): bool
    {
        return $this->gearbox_id !== null;
    }

    public function getStatusDescription(): ?string
    {
        switch ($this->status) {
            case 'active':
                return 'Активен';
            case 'obsolete':
                return 'Архив';
            default:
                return null;
        }
    }
}
