<?php

use admin\assets\CarAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarChassis */

CarAsset::register($this);

$name = $model->getSpecific();
$this->title = 'Редактировать: ' . $name;
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="car-models-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
