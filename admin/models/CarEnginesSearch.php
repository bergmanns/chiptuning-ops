<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CarEngines;

/**
 * CarEnginesSearch represents the model behind the search form of `common\models\CarEngines`.
 */
class CarEnginesSearch extends CarEngines
{
    public $make_id;
    public $model_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chassis_id', 'make_id', 'model_id', 'hp', 'nm'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarEngines::find()->where([
            'deleted_at' => null,
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if (empty($this->make_id) === false) {
            $query->joinWith(['chassis'])->andWhere([
                'car_chassis.make_id' => $this->make_id,
            ]);
        }

        if (empty($this->model_id) === false) {
            $query->joinWith(['chassis'])->andWhere([
                'car_chassis.model_id' => $this->model_id,
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'chassis_id' => $this->chassis_id,
            'hp' => $this->hp,
            'nm' => $this->nm,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
