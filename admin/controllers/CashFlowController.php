<?php

namespace admin\controllers;

use common\models\Accounts;
use common\models\Payments;
use Yii;
use common\models\CashFlow;
use admin\models\CashFlowSearch;
use yii\data\ActiveDataProvider;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CashFlowController implements the CRUD actions for CashFlow model.
 */
class CashFlowController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all CashFlow models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashFlowSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CashFlow model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionBalance()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Accounts::find(),
        ]);

        return $this->render('balance', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CashFlow model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CashFlow();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $payments = Yii::$app->request->post('payments');

            /** @var Transaction $transaction */
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
            $model->amount = array_sum(ArrayHelper::getColumn($payments, 'value'));

            Yii::debug($model->amount);

            if ($model->save() === false) {
                $transaction->rollback();

                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            foreach ($payments as $paymentVars) {
                $payment = new Payments();
                $payment->cash_flow_id = $model->id;
                $payment->name = $model->name;
                $payment->account_id = ArrayHelper::getValue($paymentVars, 'account_id');
                $payment->amount = ArrayHelper::getValue($paymentVars, 'value');
                $payment->status = Payments::STATUS_PAID;

                if ($payment->save() === false) {
                    $model->addError('payments', 'Ошибка сохранения оплат');
                    $transaction->rollBack();

                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }

            $transaction->commit();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CashFlow model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $payments = Yii::$app->request->post('payments');
            $newPaymentsIds = [];
            /** @var Transaction $transaction */
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
            $model->amount = array_sum(ArrayHelper::getColumn($payments, 'value'));

            Yii::debug($model->amount);

            if ($model->save() === false) {
                $transaction->rollback();

                return $this->render('update', [
                    'model' => $model,
                ]);
            }

            foreach ($payments as $paymentVars) {
                $id = ArrayHelper::getValue($paymentVars, 'id');
                $payment = null;

                if ($id !== null) {
                    $payment = Payments::find()->where([
                        'id' => $id,
                        'cash_flow_id' => $model->id,
                    ])->one();
                }

                $payment = $payment ?? new Payments();
                $payment->cash_flow_id = $model->id;
                $payment->name = $model->name;
                $payment->account_id = ArrayHelper::getValue($paymentVars, 'account_id');
                $payment->amount = ArrayHelper::getValue($paymentVars, 'value');
                $payment->status = Payments::STATUS_PAID;

                if ($payment->save() === false) {
                    $model->addError('payments', 'Ошибка сохранения платежей');
                    $transaction->rollBack();

                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }

                $newPaymentsIds[] = $payment->id;
             }

            $oldPayments = $model->getPayments()->andWhere([
                'not in',
                'id',
                $newPaymentsIds
            ])->all();

            foreach ($oldPayments as $payment) {
                $payment->status = Payments::STATUS_CANCEL;
                $payment->save();
            }

            $transaction->commit();

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CashFlow model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CashFlow model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CashFlow the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CashFlow::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
