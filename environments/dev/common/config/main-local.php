<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;port=3306;dbname=eurocode',
            'username' => 'eurocode',
            'password' => 'eurocode',
            'charset' => 'utf8',
        ],
        'landing' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;port=3306;dbname=eurocode_landing',
            'username' => 'eurocode',
            'password' => 'eurocode',
            'charset' => 'utf8',
        ],
    ],
];
