<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "accounts_balance_history".
 *
 * @property int $id
 * @property int $account_id
 * @property int|null $payment_id
 * @property int|null $change_by
 * @property float|null $old_balance
 * @property float|null $new_balance
 * @property string|null $created_at
 *
 * @property Staff $changeBy
 * @property Payments $payment
 * @property Accounts $account
 */
class AccountsBalanceHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts_balance_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_id', 'change_by', 'account_id'], 'integer'],
            [['payment_id', 'account_id', 'change_by'], 'required'],
            [['old_balance', 'new_balance'], 'number'],
            [['change_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['change_by' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payments::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::class, 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Payment ID',
            'change_by' => 'Change By',
            'old_balance' => 'Old Balance',
            'new_balance' => 'New Balance',
            'created_at' => 'Created At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'blameable' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => 'change_by',
                ],
            ],
        ];
    }

    /**
     * Gets query for [[ChangeBy]].
     *
     * @return ActiveQuery
     */
    public function getChangeBy()
    {
        return $this->hasOne(User::class, ['id' => 'change_by']);
    }

    /**
     * Gets query for [[Payment]].
     *
     * @return ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payments::class, ['id' => 'payment_id']);
    }

    /**
     * Gets query for [[Accounts]].
     *
     * @return ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::class, ['id' => 'account_id']);
    }
}
