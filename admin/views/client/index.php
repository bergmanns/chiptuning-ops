<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'region_name',
                'value' => function ($model) {
                    /* @var $model \admin\models\Clients */
                    return $model->region->name ?? null;
                },
            ],
            'phone',
            'full_name',
            'email:email',
            //'drive2',
            //'created_at',
            //'deleted_at',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
