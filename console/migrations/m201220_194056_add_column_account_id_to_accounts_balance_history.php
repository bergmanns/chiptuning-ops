<?php

use yii\db\Migration;

/**
 * Class m201220_194056_add_column_account_id_to_accounts_balance_history.
 */
class m201220_194056_add_column_account_id_to_accounts_balance_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('accounts_balance_history', 'account_id', $this->integer()->after('id'));
        $this->addForeignKey(
            'accounts_balance_history_account_id_key',
            'accounts_balance_history',
            'account_id',
            'accounts',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('accounts_balance_history_account_id_key', 'accounts_balance_history');
        $this->dropColumn('accounts_balance_history', 'account_id');
    }
}
