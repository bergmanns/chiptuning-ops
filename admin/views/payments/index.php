<?php

use admin\widgets\DropDownList;
use common\models\{Accounts, Categories, CategoryTypes, Locations, Payments};
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\PaymentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = $this->title;
$role = Yii::$app->user->getIdentity()->role;
?>
<div class="payments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <div class="row form-inline">
            <div class="col-md-6 form-inline">
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'month' ? 'active' : ''?>" href="?PaymentsSearch[created_at]=<?=$searchModel->filterMonthStart . ' - ' . $searchModel->filterMonthEnd?>&PaymentsSearch[createdAtPreset]=month">Месяц</a>
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'week' ? 'active' : ''?>" href="?PaymentsSearch[created_at]=<?=$searchModel->filterWeekStart . ' - ' . $searchModel->filterWeekStart?>&PaymentsSearch[createdAtPreset]=week">Неделя</a>
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'day' ? 'active' : ''?>" href="?PaymentsSearch[created_at]=<?=$searchModel->filterDayStart . ' - ' . $searchModel->filterDayEnd?>&PaymentsSearch[createdAtPreset]=day">День</a>
            </div>
            <div class="col-md-6 form-inline">
                <div class="form-inline form-group pull-right">
                    <span>Всего стоимость: <?=$searchModel->total_amount?> ₽</span>
                </div>
            </div>
        </div>
    </p>
    <p>
        <?= Html::a('Новый платеж', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Выгрузить в csv', ['csv?'. Yii::$app->request->getQueryString()], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => require_once 'columns.php',
    ]); ?>


</div>
