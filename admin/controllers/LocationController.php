<?php

namespace admin\controllers;

use admin\models\LocationSearch;
use admin\models\User;
use common\models\Countries;
use common\models\Regions;
use Throwable;
use Yii;
use common\models\Locations;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * LocationController implements the CRUD actions for Locations model.
 */
class LocationController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Locations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LocationSearch();

        if ($this->user->role === User::ROLE_MANAGER) {
            $searchModel->id = $this->user->location_id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Locations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionBalance()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Locations::find(),
        ]);

        return $this->render('balance', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Locations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $locationModel = new Locations();
        $countryModel = new Countries();
        $regionModel = new Regions();

        if (Yii::$app->request->isPost === true) {
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            try {
                $isLoaded = $locationModel->load(Yii::$app->request->post());
                $isValid = $locationModel->validate(['name', 'share']);

                if ($isLoaded === false || $isValid === false) {
                    $transaction->rollBack();

                    return $this->render('create', [
                        'locationModel' => $locationModel,
                        'countryModel' => $countryModel,
                        'regionModel' => $regionModel,
                    ]);
                }

                if ((int) $locationModel->region_id === 0) {
                    $isRegionLoaded = $regionModel->load(Yii::$app->request->post());

                    if ((int) $regionModel->country_id === 0) {
                        $isCountryLoaded = $countryModel->load(Yii::$app->request->post());
                        $isCountryValid = $countryModel->validate();

                        if ($isCountryLoaded !== false || $isCountryValid !== false) {
                            $countryModel->save(false);
                            $regionModel->country_id = $countryModel->id;
                        } else {
                            $transaction->rollBack();

                            return $this->render('create', [
                                'locationModel' => $locationModel,
                                'countryModel' => $countryModel,
                                'regionModel' => $regionModel,
                            ]);
                        }
                    }

                    $isRegionValid = $regionModel->validate();

                    if ($isRegionLoaded !== false || $isRegionValid !== false) {
                        $regionModel->save(false);
                        $locationModel->region_id = $regionModel->id;
                    } else {
                        $transaction->rollBack();

                        return $this->render('create', [
                            'locationModel' => $locationModel,
                            'countryModel' => $countryModel,
                            'regionModel' => $regionModel,
                        ]);
                    }
                }

                $isSaved = $locationModel->save();

                if ($isSaved === false) {
                    $transaction->rollBack();

                    return $this->render('create', [
                        'locationModel' => $locationModel,
                        'countryModel' => $countryModel,
                        'regionModel' => $regionModel,
                    ]);
                }

                if ($locationModel->save()) {
                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $locationModel->id]);
                }
            } catch (Throwable $e) {
                Yii::error($e);

                $transaction->rollBack();
            }
        }

        return $this->render('create', [
            'locationModel' => $locationModel,
            'countryModel' => $countryModel,
            'regionModel' => $regionModel,
        ]);
    }

    /**
     * Updates an existing Locations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $locationModel = $this->findModel($id);
        $regionModel = $locationModel->region ?? new Regions();
        $countryModel = $locationModel->country ?? new Countries();

        if (Yii::$app->request->isPost === true) {
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            try {
                $isLoaded = $locationModel->load(Yii::$app->request->post());
                $isValid = $locationModel->validate(['name', 'share']);

                if ($isLoaded === false || $isValid === false) {
                    $transaction->rollBack();

                    return $this->render('update', [
                        'locationModel' => $locationModel,
                        'countryModel' => $countryModel,
                        'regionModel' => $regionModel,
                    ]);
                }

                if ((int) $locationModel->region_id === 0) {
                    $isRegionLoaded = $regionModel->load(Yii::$app->request->post());

                    if ((int) $regionModel->country_id === 0) {
                        $isCountryLoaded = $countryModel->load(Yii::$app->request->post());
                        $isCountryValid = $countryModel->validate();

                        if ($isCountryLoaded !== false || $isCountryValid !== false) {
                            $countryModel->save(false);
                            $regionModel->country_id = $countryModel->id;
                        } else {
                            $transaction->rollBack();

                            return $this->render('update', [
                                'locationModel' => $locationModel,
                                'countryModel' => $countryModel,
                                'regionModel' => $regionModel,
                            ]);
                        }
                    }

                    $isRegionValid = $regionModel->validate();

                    if ($isRegionLoaded !== false || $isRegionValid !== false) {
                        $regionModel->save(false);
                        $locationModel->region_id = $regionModel->id;
                    } else {
                        $transaction->rollBack();

                        return $this->render('update', [
                            'locationModel' => $locationModel,
                            'countryModel' => $countryModel,
                            'regionModel' => $regionModel,
                        ]);
                    }
                }

                $isSaved = $locationModel->save();

                if ($isSaved === false) {
                    $transaction->rollBack();

                    return $this->render('update', [
                        'locationModel' => $locationModel,
                        'countryModel' => $countryModel,
                        'regionModel' => $regionModel,
                    ]);
                }

                if ($locationModel->save()) {
                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $locationModel->id]);
                }
            } catch (Throwable $e) {
                Yii::error($e);

                $transaction->rollBack();
            }
        }


        return $this->render('update', [
            'locationModel' => $locationModel,
            'countryModel' => $countryModel,
            'regionModel' => $regionModel,
        ]);
    }

    /**
     * Deletes an existing Locations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Locations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Locations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locations::findOne($id)) !== null) {
            if ($this->user->role === User::ROLE_MANAGER) {
                if ($model->id !== $this->user->location_id) {
                    throw new ForbiddenHttpException('Forbidden');
                }
            }

            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
