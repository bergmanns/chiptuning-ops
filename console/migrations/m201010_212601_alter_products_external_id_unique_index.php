<?php

use yii\db\Migration;

/**
 * Class m201010_212601_alter_products_external_id_unique_index
 */
class m201010_212601_alter_products_external_id_unique_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('products_external_id_idx', 'products');

        $this->createIndex('products_external_id_stage_idx', 'products', ['external_id', 'stage'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('products_external_id_stage_idx', 'products');

        $this->createIndex('products_external_id_idx', 'products', 'external_id', true);
    }
}
