<?php

namespace admin\models;

use common\models\ClientPhones;
use common\models\Clients as BaseClients;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Clients represents the model of `common\models\Clients`.
 */
class Clients extends BaseClients
{
    public $phone;
    public $name_with_phone;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_AFTER_FIND, [$this, 'initializePhone']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['phone'], 'string'],
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'phone' => Yii::t('app', 'Контактный телефон'),
        ]);
    }

    /**
     * @return Clients[]|array|ActiveRecord[]
     */
    public static function getWithPhone()
    {
        return self::find()
            ->select(['clients.id', 'full_name', 'dp.phone', new Expression('CONCAT(dp.phone, " (", full_name, ")") as name_with_phone')])
            ->joinWith('defaultPhone dp')
            ->where(['deleted_at' => null])
            ->all()
        ;
    }

    public function setDefaultPhone($phone): array
    {
        $defaultPhone               = new ClientPhones();
        $phone                      = preg_replace("/[^0-9]/",'', $phone);
        $phone                      = sprintf('+%s', $phone);
        $defaultPhone->phone        = $phone;
        $defaultPhone->client_id    = $this->id;

        if ($defaultPhone->validate() === false) {
            Yii::debug($defaultPhone->getErrors());

            return $defaultPhone->getErrors();
        }

        $defaultPhone->save(false);

        $this->default_phone_id = $defaultPhone->id;

        return $defaultPhone->getErrors();
    }

    public function addLocation($location): void
    {
        $relationExists = (new Query())->select('id')
                  ->from('clients_locations_relation')
                  ->where([
                      'client_id' =>$this->id,
                      'location_id' => $location->id,
                  ])
                  ->exists(self::getDb())
        ;

        if ($relationExists === true) {
            return;
        }

        $this->link('locations', $location);
    }

    protected function initializePhone(): void
    {
        $this->phone = $this->defaultPhone->phone ?? null;
    }
}
