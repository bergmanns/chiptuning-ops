<?php

use common\models\CategoryTypes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Categories */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryTypes::findAll(['active' => 1]), 'id', 'name'))?>

    <?= $form->field($model, 'for_visit')->dropDownList([
        1 => 'Активна',
        0 => 'Неактивна',
    ]) ?>
    <?= $form->field($model, 'active')->dropDownList([
        1 => 'Доступна',
        0 => 'Недоступна',
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
