<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Location balance asset bundle.
 */
class LocationsBalanceAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/location-balance-form.js',
        'js/location-balance.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'kartik\select2\Select2Asset',
        'kartik\select2\ThemeKrajeeAsset',
        'yii\bootstrap\BootstrapAsset',
        'admin\assets\VueAsset',
        'admin\assets\VueResourceAsset',
        'admin\assets\SweetAsset',
    ];
}