<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "car_gearboxes".
 *
 * @property int $id
 * @property int|null $make_id
 * @property int|null $model_id
 * @property int|null $chassis_id
 * @property int|null $engine_id
 * @property string|null $name
 * @property string|null $type
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property CarEngines $engine
 * @property CarChassis $chassis
 * @property CarMakeVariants $make
 * @property CarModelVariants $model
 */
class CarGearboxes extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_gearboxes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_id', 'model_id', 'engine_id', 'chassis_id'], 'integer'],
            [['name', 'type'], 'string', 'max' => 255],
            [['name', 'make_id'], 'required'],
            [['engine_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarEngines::class, 'targetAttribute' => ['engine_id' => 'id']],
            [['make_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarMakeVariants::class, 'targetAttribute' => ['make_id' => 'id']],
            [['model_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarModelVariants::class, 'targetAttribute' => ['model_id' => 'id']],
            [['chassis_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarChassis::class, 'targetAttribute' => ['chassis_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'softdeleteable' => [
                'class' => 'common\behaviors\SoftDeleteableBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'make_id' => 'Марка',
            'model_id' => 'Модель',
            'chassis_id' => 'Кузов',
            'engine_id' => 'Двигатель',
            'name' => 'Название',
            'created_at' => 'Создана',
            'updated_at' => 'Изменена',
            'deleted_at' => 'Удалена'
        ];
    }

    /**
     * Gets query for [[Engine]].
     *
     * @return ActiveQuery
     */
    public function getEngine()
    {
        return $this->hasOne(CarEngines::class, ['id' => 'engine_id']);
    }

    /**
     * Gets query for [[Make]].
     *
     * @return ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMakeVariants::class, ['id' => 'make_id']);
    }

    /**
     * Gets query for [[Model]].
     *
     * @return ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModelVariants::class, ['id' => 'model_id']);
    }

    /**
     * Gets query for [[Chassis]].
     *
     * @return ActiveQuery
     */
    public function getChassis()
    {
        return $this->hasOne(CarChassis::class, ['id' => 'chassis_id']);
    }
}
