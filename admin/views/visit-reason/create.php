<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VisitReasons */

$this->title = 'Create Visit Reasons';
$this->params['breadcrumbs'][] = ['label' => 'Visit Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visit-reasons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
