$( document ).ready(function() {
    $('#debts-location_id').on('change', function (e) {
        let id = this.value;
        let debounce = false;

        if (debounce === false) {
            debounce = true;

            $.ajax({
                url: '/locations/api/get-balance/',
                data: {id: id},
                dataType: 'json',
                cache: false,
                success: function (data) {
                    debounce = false;
                    setBalance(data);
                    showBalance();
                },
                error: function (data) {
                    debounce = false;
                    hideBalance();
                    setBalance(0);
                }
            });
        }

    });

    function hideBalance() {
        $('.location-balance').addClass('hidden');
    }

    function showBalance() {
        $('.location-balance').removeClass('hidden');
    }

    function setBalance(value) {
        $('.location-balance-value').html(value);
    }
});
