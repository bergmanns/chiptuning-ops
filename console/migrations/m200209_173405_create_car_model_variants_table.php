<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_model_variants}}`.
 */
class m200209_173405_create_car_model_variants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_model_variants}}', [
            'id' => $this->primaryKey(),
            'make_id' => $this->integer(),
            'name' => $this->string(),
            'created_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'active' => $this->boolean(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_model_variants}}');
    }
}
