<?php

use admin\widgets\DropDownList;
use common\components\user\AccessChecker;
use common\models\CarChassis;
use common\models\CarMakeVariants;
use common\models\CarModelVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CarEnginesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Двигатели';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-engines-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
            echo Html::a('Добавить', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',

            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'make_id',
                    'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\CarEngines */
                    return $model->chassis->make->name ?? null;
                },
            ],
            [
                'attribute' => 'model_variant_id',
                'label' => 'Модель',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'model_id',
                    'data' => ArrayHelper::map(CarModelVariants::find()->andFilterWhere(['make_id' => $searchModel->make_id])->all(), 'id', 'name')
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\CarEngines */
                    return $model->chassis->model->name ?? null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'label' => 'Кузов',
              'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'chassis_id',
                    'data' => ArrayHelper::map(
                        CarChassis::find()
                            ->andFilterWhere([
                                'make_id' => $searchModel->make_id,
                                'model_id' => $searchModel->model_id,
                            ])
                            ->all()
                        , 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\CarEngines */
                    return $model->chassis->name ?? null;
                },
            ],

            'hp',
            'nm',
            'name',
            //'created_at',
            //'updated_at',
            //'deleted_at',
            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>


</div>
