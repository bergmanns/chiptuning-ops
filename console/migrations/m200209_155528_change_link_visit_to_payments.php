<?php

use yii\db\Migration;

/**
 * Class m200209_155528_change_link_visit_to_payments
 */
class m200209_155528_change_link_visit_to_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('payments_visit_key', 'payments');
        $this->renameColumn('payments', 'visit_id', 'visit_product_id');
        $this->addForeignKey(
            'payments_visit_product_key',
            'payments',
            'visit_product_id',
            'visits_products',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('payments_visit_product_key', 'payments');
        $this->renameColumn('payments', 'visit_product_id', 'visit_id');
        $this->addForeignKey(
            'payments_visit_key',
            'payments',
            'visit_id',
            'visits',
            'id'
        );
    }
}
