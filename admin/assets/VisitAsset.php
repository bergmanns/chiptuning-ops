<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class VisitAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/visit.css',
    ];
    public $js = [
        'js/visit-view.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'admin\assets\PhoneMaskAsset',
        'admin\assets\VueTableAsset',
        'kartik\select2\Select2Asset',
        'kartik\select2\ThemeKrajeeAsset',
        'yii\bootstrap\BootstrapAsset',
        'admin\assets\VueDatePickerAsset',
    ];
}
