<?php

use yii\db\Migration;

/**
 * Class m201220_171017_create_locations_balance_history.
 */
class m201220_171017_create_locations_balance_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%locations_balance_history}}', [
            'id' => $this->primaryKey(),
            'location_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'change_by' => $this->integer(),
            'old_balance' => $this->float(),
            'new_balance' => $this->float(),
            'created_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'locations_balance_history_payment_id_key',
            'locations_balance_history',
            'payment_id',
            'payments',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'locations_balance_history_change_by_key',
            'locations_balance_history',
            'change_by',
            'staff',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'locations_balance_history_location_id_key',
            'locations_balance_history',
            'location_id',
            'locations',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('locations_balance_history');
    }
}
