<?php

declare(strict_types = 1);

namespace common\behaviors;

use Carbon\CarbonImmutable;
use yii\behaviors\TimestampBehavior as BaseTimestampBehavior;
use yii\db\ActiveRecord;

class TimestampBehavior extends BaseTimestampBehavior
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->initializeValue();
    }

    /**
     * {@inheritdoc}
     */
    public function attach($owner)
    {
        parent::attach($owner);

        $this->initializeAttributes();
    }

    /**
     * Initializes attributes.
     */
    private function initializeAttributes()
    {
        /* @var $owner ActiveRecord */
        $owner = $this->owner;

        $this->attributes[ActiveRecord::EVENT_BEFORE_INSERT] = [];
        $this->attributes[ActiveRecord::EVENT_BEFORE_UPDATE] = [];

        if ($owner->hasAttribute('created_at') === true) {
            $this->attributes[ActiveRecord::EVENT_BEFORE_INSERT][] = 'created_at';
        }

        if ($owner->hasAttribute('updated_at') === true) {
            $this->attributes[ActiveRecord::EVENT_BEFORE_INSERT][] = 'updated_at';
            $this->attributes[ActiveRecord::EVENT_BEFORE_UPDATE][] = 'updated_at';
        }
    }

    /**
     * Initializes attribute value.
     */
    private function initializeValue()
    {
        $this->value = function ($event) {
            $time = CarbonImmutable::now()->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT);

            return $time;
        };
    }
}
