<?php

use admin\widgets\DropDownList;
use common\components\user\AccessChecker;
use common\models\CarMakeVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CarModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Модели';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-model-variants-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
            echo Html::a('Добавить марку', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'make_id',
                    'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model \common\models\CarModelVariants */
                    return $model->make->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 20%',
                ],
            ],
            'name',
            'created_at:datetime',
            'deleted_at:datetime',
            'active:boolean',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
