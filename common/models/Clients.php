<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property int|null $region_id
 * @property int $default_phone_id
 * @property string|null $full_name
 * @property string|null $email
 * @property string|null $drive2
 * @property string|null $created_at
 * @property string|null $deleted_at
 *
 * @property Cars[] $cars
 * @property ClientPhones[] $clientPhones
 * @property ClientPhones $defaultPhone
 * @property Regions $region
 * @property Visits[] $visits
 */
class Clients extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'default_phone_id'], 'integer'],
            [['full_name', 'region_id'], 'required'],
            [['full_name', 'email', 'drive2'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::class, 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Офис'),
            'default_phone_id' => Yii::t('app', 'Основной телефон'),
            'full_name' => Yii::t('app', 'ФИО'),
            'email' => Yii::t('app', 'Email'),
            'drive2' => Yii::t('app', 'Drive2'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'deleted_at' => Yii::t('app', 'Удален'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Cars::class, ['client_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClientPhones()
    {
        return $this->hasMany(ClientPhones::class, ['client_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultPhone()
    {
        return $this->hasOne(ClientPhones::class, ['id' => 'default_phone_id']);
    }

    public function getDefaultPhoneNumber()
    {
        return isset($this->defaultPhone) ? $this->defaultPhone->phone : null;
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::class, ['id' => 'region_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visits::class, ['client_id' => 'id']);
    }

    public function getLocations()
    {
        return $this->hasMany(Locations::class, ['id' => 'location_id'])
            ->viaTable('clients_locations_relation', ['client_id' => 'id']);
    }
}
