<?php

declare(strict_types = 1);

namespace common\behaviors;

use yii\db\ActiveRecord;
use yii2tech\ar\softdelete\SoftDeleteBehavior as BaseSoftDeleteBehavior;

class SoftDeleteableBehavior extends BaseSoftDeleteBehavior
{
    /**
     * @inheritdos
     */
    public function init()
    {
        parent::init();

        $this->softDeleteAttributeValues = [
            'deleted_at' => function ($model) {
                /* @var $model ActiveRecord */
                return date(DATE_W3C);
            },
        ];

        $this->replaceRegularDelete = true;
    }
}
