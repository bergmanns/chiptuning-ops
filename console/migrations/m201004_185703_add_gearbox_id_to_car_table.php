<?php

use yii\db\Migration;

/**
 * Class m201004_185703_add_gearbox_id_to_car_table
 */
class m201004_185703_add_gearbox_id_to_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cars', 'gearbox_id', $this->integer()->after('engine_id'));

        $this->addForeignKey(
            'cars_gearbox_id_key',
            'cars',
            'gearbox_id',
            'car_gearboxes',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('cars_gearbox_id_key', 'cars');
        $this->dropColumn('cars', 'gearbox_id');
    }
}
