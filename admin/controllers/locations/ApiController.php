<?php

namespace admin\controllers\locations;

use admin\models\User;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Countries;
use common\models\Locations;
use common\models\Payments;
use Yii;
use yii\db\Expression;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * ApiController for locations entity.
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET'],
                    'get-balance' => ['GET'],
                    'countries' => ['GET'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
        ];
    }

    /**
     * Return info about all allowed Locations models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $locations = Locations::find()->joinWith(['country']);
        $user = Yii::$app->user->getIdentity();

        if ($user->role !== User::ROLE_ADMIN) {
            $locations->where(['locations.id' => $user->location_id]);
        }

        $answer = [];
        $locations = $locations->all();

        foreach ($locations as $location) {
            $answer[] = [
                'id' => $location->id,
                'name' => $location->name,
                'country' => $location->country->toArray(['short', 'gos_number_mask']),
            ];
        }

        return $answer;
    }

    public function actionBalance()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        $location = $this->findModel($id);
        $balance = Yii::$app->request->post('balance');
        $newBalance = Yii::$app->request->post('newBalance');

        $type = CategoryTypes::getTypeByName(CategoryTypes::PAYMENT_TYPE_NAME);
        $category = Categories::getCategoryByName(Categories::BALANCE_FIX_CATEGORY_NAME, $type);

        $balanceFixPayment = new Payments();
        $balanceFixPayment->type_id = $type->id;
        $balanceFixPayment->category_id = $category->id;
        $balanceFixPayment->location_id = $location->id;
        $balanceFixPayment->name = sprintf('Коррекция баланса %s', $location->name);
        $balanceFixPayment->status = Payments::STATUS_PAID;
        $balanceFixPayment->amount = $newBalance - $location->balance;

        Yii::debug($balanceFixPayment->toArray());

        if ($balanceFixPayment->save() === false) {
            Yii::debug($balanceFixPayment->getErrors());

            throw new BadRequestHttpException('Ошибка Базы Данных');
        }

        return $location->toArray();
    }

    public function actionGetBalance($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        return $model->balance;
    }

    /**
     * Displays Locations countries.
     * @return mixed
     */
    public function actionCountries()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $otherCountry = [
            'id' => -1,
            'name' => 'Other',
            'short' => 'Other',
        ];

        $countries = Countries::find()
            ->select(['id', 'short', 'gos_number_mask'])
            ->asArray()
            ->where([
                'deleted_at' => null,
            ])
            ->andWhere(new Expression('gos_number_mask IS NOT NULL'))
            ->andWhere(new Expression('short IS NOT NULL'))
            ->all()
        ;
        $countries[] = $otherCountry;

        return $countries;
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Locations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Locations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
