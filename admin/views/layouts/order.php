<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title></title>
	<meta name="generator" content="LibreOffice 7.1.2.2 (Linux)"/>
	<meta name="author" content="Comp2"/>
	<meta name="created" content="2003-09-18T09:41:28"/>
	<meta name="changedby" content="Павел"/>
	<meta name="changed" content="2021-05-08T13:20:03"/>
	<meta name="AppVersion" content="12.0000"/>
	<meta name="Company" content="AUTO"/>

	<style type="text/css">

body {margin-top: 0px;margin-left: 0px;}
		body,div,table,thead,tbody,tfoot,tr,th,td,p { font-family:"Arial Cyr"; font-size:small }
		a.comment-indicator:hover + comment { background:#ffd; position:absolute; display:block; border:1px solid black; padding:0.5em;  }
		a.comment-indicator { background:red; display:inline-block; border:1px solid black; width:0.5em; height:0.5em;  }
		comment { display:none;  }


.ft0{font: 10px 'Arial';line-height: 13px;}
.ft1{font: 1px 'Arial';line-height: 1px;}
.ft2{font: bold 13px 'Arial';line-height: 16px;}
.ft3{font: bold 13px 'Arial';text-decoration: underline;line-height: 16px;}
.ft4{font: 11px 'Arial';line-height: 14px;}
.ft5{font: 12px 'Arial';line-height: 15px;}
.ft6{font: italic 9px 'Arial';line-height: 12px;}
.ft7{font: italic 10px 'Arial';line-height: 13px;}
.ft8{font: bold 12px 'Arial';line-height: 15px;}
.ft9{font: bold 13px 'Helvetica';line-height: 16px;}
.ft10{font: 10px 'Arial';line-height: 12px;}
.ft11{font: 10px 'Arial';text-decoration: underline;line-height: 12px;}
.ft12{font: 10px 'Arial';margin-left: 2px;line-height: 12px;}
.ft13{font: 10px 'Arial';line-height: 11px;}
.ft14{font: 10px 'Arial';margin-left: 3px;line-height: 11px;}
.ft15{font: 10px 'Arial';margin-left: 3px;line-height: 12px;}
.ft16{font: 11px 'Arial';line-height: 14px;}


.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: right;padding-right: 80px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: right;padding-right: 62px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: right;padding-right: 80px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: left;padding-left: 6px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: right;padding-right: 99px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-right: 86px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p8{text-align: right;padding-right: 26px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p9{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p10{text-align: left;padding-left: 51px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p11{text-align: left;padding-left: 12px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p12{text-align: right;padding-right: 2px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p13{text-align: left;padding-left: 81px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p14{text-align: left;padding-left: 1px;margin-top: 1px;margin-bottom: 0px;}
.p15{text-align: left;padding-left: 1px;padding-right: 114px;margin-top: 0px;margin-bottom: 0px;}
.p16{text-align: left;padding-left: 1px;padding-right: 90px;margin-top: 0px;margin-bottom: 0px;}
.p17{text-align: left;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;}
.p18{text-align: left;padding-left: 1px;padding-right: 56px;margin-top: 0px;margin-bottom: 0px;}
.p19{text-align: left;padding-left: 1px;margin-top: 39px;margin-bottom: 0px;}
.p20{text-align: center;padding-right: 105px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p21{text-align: center;padding-right: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p22{text-align: left;padding-left: 198px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p23{text-align: center;padding-left: 1px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p24{text-align: center;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}

	</style>

</head>

<body>

<?=$content;?>

<!-- ************************************************************************** -->
</body>

</html>
