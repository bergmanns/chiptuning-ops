<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cash_flow}}`.
 */
class m200809_101914_create_cash_flow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cash_flow}}', [
            'id' => $this->primaryKey(),
            'visit_id' => $this->integer(),
            'type_id' => $this->integer(),
            'category_id' => $this->integer(),
            'location_id' => $this->integer(),
            'amount' => $this->float(),
            'hq_share' => $this->float(),
            'name' => $this->string()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'paid_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addColumn('payments', 'cash_flow_id', $this->integer()->after('id'));
        $this->addForeignKey(
            'payments_cash_flow_id_key',
            'payments',
            'cash_flow_id',
            'cash_flow',
            'id'
        );

        $this->addForeignKey(
            'cash_flow_visit_id_key',
            'cash_flow',
            'visit_id',
            'visits',
            'id'
        );
        $this->addForeignKey(
            'cash_flow_category_id_key',
            'cash_flow',
            'category_id',
            'categories',
            'id'
        );
        $this->addForeignKey(
            'cash_flow_location_id_key',
            'cash_flow',
            'location_id',
            'locations',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash_flow}}');
    }
}
