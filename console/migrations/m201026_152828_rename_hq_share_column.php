<?php

use yii\db\Migration;

/**
 * Class m201026_152828_rename_hq_share_column.
 */
class m201026_152828_rename_hq_share_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('locations', 'hq_share', 'share');
        $this->renameColumn('cash_flow', 'hq_share', 'share');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('locations', 'share', 'hq_share');
        $this->renameColumn('cash_flow', 'share', 'hq_share');
    }
}
