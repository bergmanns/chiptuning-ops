<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $locationModel common\models\Locations */
/* @var $countryModel common\models\Countries */
/* @var $regionModel common\models\Regions */

$this->title = 'Добавить Филиал';
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="locations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'locationModel' => $locationModel,
        'countryModel' => $countryModel,
        'regionModel' => $regionModel,
    ]) ?>

</div>
