<?php

namespace admin\models;

use common\models\Accounts;
use common\models\Parts;
use Yii;
use common\models\Debts as BaseDebts;
use yii\helpers\ArrayHelper;

/**
 * DebtsSearch represents the model behind the search form of `common\models\Debts`.
 */
class Debts extends BaseDebts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'paid_by', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['name', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['amount'], 'number'],
            [['amount', 'type_id', 'category_id', 'location_id', 'name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'type_id' => Yii::t('app', 'Тип'),
            'category_id' => Yii::t('app', 'Категория'),
            'location_id' => Yii::t('app', 'Филиал'),
            'visit_id' => Yii::t('app', 'Визит'),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::class, ['id' => 'account_id'])->via('cashFlow');
    }

    public function getParts()
    {
        return $this->hasMany(Parts::class, ['debt_id' => 'id']);

    }
}
