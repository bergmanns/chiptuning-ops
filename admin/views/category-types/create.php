<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CategoryTypes */

$this->title = 'Добавить Тип Категории';
$this->params['breadcrumbs'][] = ['label' => 'Типы Категорий', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
