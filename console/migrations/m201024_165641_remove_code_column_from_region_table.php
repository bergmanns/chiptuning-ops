<?php

use yii\db\Migration;

/**
 * Class m201024_165641_remove_code_column_from_region_table
 */
class m201024_165641_remove_code_column_from_region_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('regions', 'code');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201024_165641_remove_code_column_from_region_table cannot be reverted.\n";

        return false;
    }
}
