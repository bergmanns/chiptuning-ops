<?php

use yii\db\Migration;

/**
 * Class m201108_150141_remove_cash_flow_id_from_tables
 */
class m201108_150141_remove_cash_flow_id_from_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('payments', 'cash_flow_id');
        $this->dropColumn('debts', 'cash_flow_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201108_150141_remove_cash_flow_id_from_tables cannot be reverted.\n";

        return false;
    }
}
