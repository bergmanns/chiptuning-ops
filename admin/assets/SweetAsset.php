<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Vue framework asset bundle.
 */
class SweetAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/sweetalert/dist';

    public $js = [
        'sweetalert.min.js',
        //'https://unpkg.com/sweetalert/dist/sweetalert.min.js',
    ];

    public $css = [
        '/css/sweetalert.css',
        //'https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css',
    ];

    public $depends = [
    ];
}
