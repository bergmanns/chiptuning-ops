<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%visits_products}}`.
 */
class m200216_141446_add_comment_column_to_visits_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('visits_products', 'comment', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('visits_products', 'comment');
    }
}
