<?php

use yii\db\Migration;

/**
 * Class m201004_185718_add_gearbox_id_to_products_table
 */
class m201004_185718_add_gearbox_id_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'gearbox_id', $this->integer()->after('engine_id'));

        $this->addForeignKey(
            'products_gearbox_id_key',
            'products',
            'gearbox_id',
            'car_gearboxes',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('products_gearbox_id_key', 'cars');
        $this->dropColumn('products', 'gearbox_id');
    }
}
