<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%accounts}}`.
 */
class m201212_172930_add_balance_column_to_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('accounts', 'balance', $this->float()->after('id')->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('accounts', 'balance');
    }
}
