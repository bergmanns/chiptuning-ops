<?php

use admin\models\Clients;
use admin\widgets\DropDownList;
use common\models\Locations;
use common\models\Products;
use common\models\VisitReasons;
use common\models\VisitsProducts;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Visits */
/* @var $form yii\widgets\ActiveForm */

$productModels = [new VisitsProducts()];
?>

<div class="container-fluid">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-12">
        <div class="visits-form col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>

            <div class="form-group">
                <?= Html::submitButton('Соханить', ['class' => 'btn btn-success']) ?>
            </div>
            <div class="col-lg-4 client-info-block">
                <h3>Клиент</h3>
                <?= $form->field($model, 'client_id')->widget(DropDownList::class, [
                    'data' => ArrayHelper::merge(
                            [ '0' => 'Добавить пользователя'],
                            ArrayHelper::map(
                                Clients::getWithPhone(), 'id', 'name_with_phone')
                    ),
                    'options' => [
                        'placeholder' => 'Выберите клиента',
                    ],
                ]) ?>
                <?= $form->field($model, 'client_phone')->textInput([
                    'disabled' => 'disabled',
                    'class' => 'phone_mask form-control'
                ]) ?>
                <?= $form->field($model, 'client_name')->textInput([
                    'disabled' => 'disabled'
                ]) ?>

            </div>
            <div class="col-lg-4 client-car-block">
                <h3>Машина</h3>
<!--                --><?//= $form->field($model, 'car_id', [
//                    'options' => [
//                        'class' => 'form-control hidden',
//                    ]
//                ])->hiddenInput() ?>
                <?= $form->field($model, 'car_id')
                    ->widget(DropDownList::class, [
                        'data' => ArrayHelper::merge(['-1' => 'Добавить машину'] , ArrayHelper::map($model->getCar()->all(), 'id', 'fullName')),
                        'options' => [
                            'placeholder' => 'Выберите машину клиента',
                        ],
                ])
                ?>
                <?= $form->field($model, 'client_car_make')->dropDownList([]) ?>
                <?= $form->field($model, 'client_car_model')->dropDownList([]) ?>
                <?= $form->field($model, 'vin_number')->textInput([
                    'disabled' => 'disabled'
                ]) ?>
                <?= $form->field($model, 'gos_number')->textInput([
                    'disabled' => 'disabled'
                ]) ?>
                <?= $form->field($model, 'current_mileage')->textInput() ?>

            </div>
            <div class="col-lg-4 location-info-block">
                <h3>Локация</h3>
                <?= $form->field($model, 'location_id')->widget(DropDownList::class, [
                    'data' => ArrayHelper::map(Locations::find()->all(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите офис',
                    ],
                ]) ?>

                <?= $form->field($model, 'source')->textInput() ?>

                <?= $form->field($model, 'payment_status')->checkbox() ?>
            </div>
                <div class="col-lg-12">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Продукт</th>
                                        <th>Цель</th>
                                        <th>Дата</th>
                                        <th>Комментарий</th>
                                    </tr>
                                </thead>
                                <tbody class="products-info-block">
                                <?php foreach ($productModels as $index => $productModel) { ?>
                                    <tr class="product-row" date-product-index="<?= $index?>" date-product-id="<?= $productModel->id?>">
                                        <td class="product-row-control"><button type="button" class="btn btn-danger delete-product-row">Удалить</button></td>
                                        <td><?= $form->field($productModel, 'product_id', [
                                                'template' => "{input}\n{hint}\n{error}",
                                            ])->dropDownList(ArrayHelper::map(Products::find()->all(), 'id', 'name'), [
                                                'name' => sprintf('VisitsProducts[%s][product_id]', $index)
                                            ])?></td>
                                        <td>
                                            <?= $form->field($productModel, 'category_id', [
                                                'template' => "{input}\n{hint}\n{error}",
                                            ])->dropDownList(ArrayHelper::map(VisitReasons::find()->all(), 'id', 'name'), [
                                                    'name' => sprintf('VisitsProducts[%s][category_id]', $index)
                                            ]) ?></td>
                                            <td><?= $form->field($productModel, 'visit_at', [
                                                'template' => "{input}\n{hint}\n{error}",
                                            ])->input('datetime-local', [
                                                    'id' =>  sprintf('visitsproducts-visit_at_%s', $index),
                                                    'name' => sprintf('VisitsProducts[%s][visit_at]', $index),
                                                ])?></td>
                                            <td>
                                                <?= $form->field($productModel, 'comment', [
                                                    'template' => "{input}\n{hint}\n{error}",
                                                ])->textarea([
                                                    'rows' => 1,
                                                    'id' => sprintf('visitsproducts-comment_%s', $index),
                                                    'name' => sprintf('VisitsProducts[%d][comment]', $index)
                                                ]) ?>
                                            </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <button type="button" class="btn btn-success add-product">Добавить продукт</button>
                    </div>
                </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

