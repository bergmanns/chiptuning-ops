<?php

use admin\widgets\DropDownList;
use common\models\Countries;
use common\models\Regions;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $locationModel common\models\Locations */
/* @var $countryModel common\models\Countries */
/* @var $regionModel common\models\Regions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="locations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($locationModel, 'region_id')->widget(DropDownList::class, [
        'data' => ArrayHelper::merge([ '0' => 'Добавить регион'], ArrayHelper::map(Regions::find()->all(), 'id', 'name')),
    ]) ?>

    <div class="country-form">
        <?= $form->field($regionModel, 'country_id')->widget(DropDownList::class, [
            'data' => ArrayHelper::merge([ '0' => 'Добавить страну'], ArrayHelper::map(Countries::find()->all(), 'id', 'name')),
        ]) ?>
    </div>

    <div class="new-country-form">
        <?= $form->field($countryModel, 'name', ['enableClientValidation' => false])->textInput()->label('Название страны'); ?>
    </div>

    <div class="region-form">
        <?= $form->field($regionModel, 'name', ['enableClientValidation' => false])->textInput()->label('Название региона'); ?>
    </div>
    <?= $form->field($locationModel, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($locationModel, 'share')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
