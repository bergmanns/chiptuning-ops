<?php

use admin\widgets\DropDownList;
use common\components\user\AccessChecker;
use common\models\CarMakeVariants;
use common\models\CarChassis;
use common\models\CarModelVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CarChassisSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Кузов';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-models-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
            echo Html::a('Добавить кузов', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'make_id',
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0,
                    ],
                    'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model CarChassis */
                    return $model->make->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 20%',
                ],
            ],
            [
                'attribute' => 'model_id',
                'filter' => DropDownList::widget([
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 0,
                    ],
                    'model' => $searchModel,
                    'attribute' => 'model_id',
                    'data' => ArrayHelper::map(CarModelVariants::find()->andFilterWhere(['make_id' => $searchModel->make_id])->all(), 'id', 'name')
                ]),
                'value' => function ($model) {
                    /* @var $model CarChassis */
                    return $model->model->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 20%',
                ],
            ],
            'name',
//            'stock_power',
//            'injection',
//            'engine_name',
//            'engine_volume',
//            'gearbox_model',
//            'gearbox_vers',
//            'engine_type',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
