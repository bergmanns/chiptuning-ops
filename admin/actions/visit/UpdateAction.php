<?php

declare (strict_types = 1);

namespace admin\actions\visit;

use admin\models\Clients;
use admin\models\User;
use admin\models\Visits;
use common\models\Cars;
use common\models\VisitsProducts;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class UpdateAction extends BaseVisitAction
{
    /**
     * @var array
     */
    private $post = [];

    /**
     * Updates the Visits model.
     * If updated is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $this->post = Yii::$app->request->post();
        $this->model = Visits::find()->where([
            'id' => ArrayHelper::getValue($this->post, 'Visits.id'),
        ])->one();

        if ($this->model === null) {
            throw new BadRequestHttpException('Visit with this id not found');
        }

        if (Yii::$app->user->getIdentity()->role === User::ROLE_MANAGER) {
            if ($this->model->contact_id !== Yii::$app->user->getId()) {
                throw new ForbiddenHttpException('Visit belong to another manager');
            }

            if ($this->model->completed_at !== null) {
                throw new ForbiddenHttpException('Визит уже завершен');
            }

            $this->scenario = Visits::SCENARIO_UPDATE_BY_MANAGER;
        } else if (Yii::$app->user->getIdentity()->role === User::ROLE_ADMIN) {
            $this->scenario = Visits::SCENARIO_UPDATE_BY_ADMIN;
        }

        return $this->handleVisit();
    }

    protected function getVisitProducts($visitId):  ? VisitsProducts
    {
        return VisitsProducts::find()->where([
            'visit_id' => $visitId,
        ])->one();
    }

    protected function initializeCar(Visits &$model,  ? Clients $client) :  ? Cars
    {
        $carId = ArrayHelper::getValue($this->post, 'Visits.car_id');

        if (empty($carId) === true || $carId === 'undefined') {
            return null;
        }

        $vin = ArrayHelper::getValue($this->post, 'Visits.vin_number');
        $vin = empty($vin) === true ? null : $vin;
        $gos = ArrayHelper::getValue($this->post, 'Visits.gos_number');
        $gos = empty($gos) === true ? null : $gos;
        $engineModel = ArrayHelper::getValue($this->post, 'Visits.engine_model');

        if ($carId === '-1') {
            if ($this->model->isComplete() === true) {
                $model->addError('car_id', 'Визит уже завершен');

                return null;
            }

            $car = new Cars;
            $car->client_id = $client->id;
            $car->engine_id = ArrayHelper::getValue($this->post, 'Visits.engine_id');
            $car->gearbox_id = ArrayHelper::getValue($this->post, 'Visits.gearbox_id');
            $car->engine_model = $engineModel;
            $car->vin_number = $vin;
            $car->gos_number = $gos;

            if ($car->save() === false) {
                $model->addError('car_id', 'Проверьте данные машины');
            }

            return $car;
        }

        $car = Cars::findOne(['id' => $carId, 'client_id' => $client->id]);

        if ($car === null) {
            $model->addError('car_id', 'Проверьте данные машины');

            return null;
        }

        if ($this->model->isComplete() === false && $this->scenario === Visits::SCENARIO_UPDATE_BY_ADMIN) {
            $car->engine_id = ArrayHelper::getValue($this->post, 'Visits.engine_id');
            $car->gearbox_id = ArrayHelper::getValue($this->post, 'Visits.gearbox_id');
            $car->engine_model = $engineModel;
        }

        $car->vin_number = $vin;
        $car->gos_number = $gos;

        if ($car->save() === false) {
            $model->addError('car_id', 'Проверьте данные машины');
        }

        return $car;
    }
}
