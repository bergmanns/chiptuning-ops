<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class InputMultiMaskAsset extends AssetBundle
{
    public $sourcePath = '@vendor/andr-04/jquery.inputmask-multi/js';
    public $js = [
        'jquery.inputmask-multi.js',
    ];
    public $depends = [
        'admin\assets\InputMaskAsset',
    ];
}
