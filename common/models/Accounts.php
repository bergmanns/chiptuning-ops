<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property float $balance
 * @property string|null $name
 * @property string|null $account_num
 * @property int|null $table_visible
 *
 * @property Payments[] $payments
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['table_visible'], 'integer'],
            [['balance'], 'number'],
            [['name', 'account_num'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'balance' => 'Баланс',
            'name' => 'Название',
            'account_num' => 'Позиция',
            'table_visible' => 'Отображение в таблице',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::class, ['account_id' => 'id']);
    }
}
