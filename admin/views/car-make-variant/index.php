<?php

use common\components\user\AccessChecker;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Марки';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-make-variants-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
            echo Html::a('Добавить марку', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'created_at:datetime',
            'deleted_at:datetime',
            'active:boolean',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
