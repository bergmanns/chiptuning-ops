<?php

use admin\widgets\DropDownList;
use common\models\CarMakeVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarModelVariants */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-model-variants-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'make_id')->widget(DropDownList::class, [
        'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
    ]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'active')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
