<?php

use yii\db\Migration;

/**
 * Class m200712_121957_alter_payments_table
 */
class m200712_121957_alter_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('payments', 'category_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('payments', 'category_id');
    }
}
