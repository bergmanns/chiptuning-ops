<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "visits_products".
 *
 * @property int $id
 * @property int|null $visit_id
 * @property int|null $product_id
 * @property int|null $category_id
 * @property int|null $payment_id
 * @property string|null $status
 * @property string|null $created_at
 * @property string|null $paid_at
 * @property string|null $visit_at
 * @property string|null $comment

 * @property Visits $visit
 * @property Payments $payment
 * @property Products $product
 * @property Categories $category
 * @property Locations $location
 * @property Cars $car
 * @property Clients $client
 * @property Parts $parts
 */
class VisitsProducts extends ActiveRecord
{
    public const STATUS_NEW = 'new';
    public const STATUS_CANCELLED = 'cancelled';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DONE = 'done';
    public const STATUS_PAID = 'paid';

    public $car_models;
    public $car_model_variants;
    public $car_make_variants;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visits_products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visit_id', 'product_id', 'category_id', 'payment_id'], 'integer'],
            [['status', 'comment'], 'string'],
            [['status'], 'default', 'value' => self::STATUS_NEW],
            [['visit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Visits::class, 'targetAttribute' => ['visit_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payments::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::class, 'targetAttribute' => ['product_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::class, 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'visit_id' => 'Визит',
            'product_id' => 'Продукт',
            'category_id' => 'Категория',
            'payment_id' => 'Платеж',
            'status' => 'Статус',
            'created_at' => 'Добавлен',
            'paid_at' => 'Оплачен',
            'visit_at' => 'Записан на',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(Visits::class, ['id' => 'visit_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id'])->via('visit');
    }

    /**
     * @return ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::class, ['id' => 'car_id'])->via('visit');
    }

    /**
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id'])->via('visit');
    }

    /**
     * @return ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payments::class, ['id' => 'payment_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParts()
    {
        return $this->hasMany(Parts::class, ['visit_product_id' => 'id']);
    }

    public function getAllPartsPrice(): int
    {
        $total = 0;

        foreach ($this->parts as $part) {
            $total += $part['amount'] * $part['count'];
        }

        return $total;
    }

    public function removeAllParts(): void
    {
        $this->unlinkAll('parts', true);
    }
}
