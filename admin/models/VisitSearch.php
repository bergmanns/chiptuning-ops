<?php

namespace admin\models;

use common\models\VisitsProducts;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * VisitSearch represents the model behind the search form of `common\models\Visits`.
 */
class VisitSearch extends VisitsProducts
{
    public $location_id;
    public $client_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'car_id', 'client_id', 'contact_id', 'location_id', 'category_id', 'mileage', 'current_mileage', 'status', 'source', 'visit_time', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VisitsProducts::find()->orderBy('visit_at ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'car_id' => $this->car_id,
//            'client_id' => $this->client_id,
//            'contact_id' => $this->contact_id,
//            'location_id' => $this->location_id,
            'category_id' => $this->category_id,
//            'mileage' => $this->mileage,
//            'current_mileage' => $this->current_mileage,
//            'visit_time' => $this->visit_time,
            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
//            'deleted_at' => $this->deleted_at,
        ]);

//        $query->andFilterWhere(['like', 'status', $this->status])
//            ->andFilterWhere(['like', 'source', $this->source]);

        return $dataProvider;
    }
}
