<?php

use admin\widgets\DropDownList;
use common\models\CarChassis;
use common\models\CarEngines;
use common\models\CarGearboxes;
use common\models\CarMakeVariants;
use common\models\CarModelVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\SearchProduct */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',

            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'make_id',
                    'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Products */
                    return $model->make->name ?? null;
                },
            ],
            [
                'attribute' => 'model_id',
                'label' => 'Модель',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'model_id',
                    'data' => ArrayHelper::map(CarModelVariants::find()->andFilterWhere(['make_id' => $searchModel->make_id])->all(), 'id', 'name')
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Products */
                    return $model->model->name ?? null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'label' => 'Кузов',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'chassis_id',
                    'data' => ArrayHelper::map(
                            CarChassis::find()
                                ->andFilterWhere([
                                    'make_id' => $searchModel->make_id,
                                    'model_id' => $searchModel->model_id,
                                ])
                                ->all()
                            , 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model \common\models\Products */
                    return $model->chassis->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
            ],
            [
                'attribute' => 'engine_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'engine_id',
                    'data' => ArrayHelper::map(CarEngines::find()->andFilterWhere([
                        'chassis_id' => $searchModel->chassis_id,
                    ])->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Products */

                    if ($model->engine !== null) {
                        return $model->engine->fullName;
                    }

                     return null;
                },
            ],

            [
                'attribute' => 'gearbox_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'gearbox_id',
                    'data' => ArrayHelper::map(CarGearboxes::find()->andFilterWhere([
                        'chassis_id' => $searchModel->chassis_id,
                        'make_id' => $searchModel->make_id,
                        'model_id' => $searchModel->model_id,
                    ])->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Products */

                    if ($model->gearbox_id !== null) {
                        return $model->gearbox->name;
                    }

                    return null;
                },
            ],
            'name',
//            'link',
            'price',
            //'description:ntext',
            [
                'attribute' => 'status',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'status',
                    'data' => ['active' => 'Активен', 'obsolete' => 'Архив'],
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Products */
                    return $model->getStatusDescription();
                },
            ],
            //'created_at',
            'updated_at:datetime',
            //'deleted_at',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
