<?php

use yii\db\Migration;

/**
 * Class m200830_200809_midify_debts_table
 */
class m200830_200809_midify_debts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('debts', 'visit_id', $this->integer()->after('cash_flow_id'));
        $this->addColumn('debts', 'type_id', $this->integer()->after('visit_id'));
        $this->addColumn('debts', 'category_id', $this->integer()->after('type_id'));
        $this->addColumn('debts', 'location_id', $this->integer()->after('category_id'));

        $this->addForeignKey(
            'debts_visit_id_key',
            'debts',
            'visit_id',
            'visits',
            'id'
        );

        $this->addForeignKey(
            'debts_type_id_key',
            'debts',
            'type_id',
            'category_types',
            'id'
        );

        $this->addForeignKey(
            'debts_category_id_key',
            'debts',
            'category_id',
            'categories',
            'id'
        );

        $this->addForeignKey(
            'debts_location_id_key',
            'debts',
            'location_id',
            'locations',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('debts', 'location_id');
        $this->dropColumn('debts', 'category_id');
        $this->dropColumn('debts', 'type_id');
        $this->dropColumn('debts', 'visit_id');
    }
}
