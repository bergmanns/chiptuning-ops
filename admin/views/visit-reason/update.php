<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VisitReasons */

$this->title = 'Update Visit Reasons: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Visit Reasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="visit-reasons-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
