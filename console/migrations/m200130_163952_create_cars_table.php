<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cars}}`.
 */
class m200130_163952_create_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cars}}', [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'chip_id' => $this->integer(),
            'issue_year' => $this->char(4),
            'vin_number' => $this->string(),
            'gos_number' => $this->string(),
            'eng_chip_status' => "ENUM('stock', 'stage_1', 'stage_2', 'stage_3')",
            'gear_chip_status' => "ENUM('stock', 'chip')",
            'files_link' => $this->string(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'cars_client_key',
            'cars',
            'client_id',
            'clients',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cars}}');
    }
}
