<?php

use admin\widgets\DropDownList;
use common\models\Locations;
use common\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'location_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'location_id',
                    'data' => ArrayHelper::map(Locations::find()->all(), 'id', 'name'),
                ]),

                'value' => function ($model) {
                    /* @var $model User */
                    return $model->location->name ?? null;
                },
            ],
            'username',
            'phone',
//            'password_hash',
            [
                'attribute' => 'role',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'role',
                    'data' => [
                            User::ROLE_ADMIN => 'Админ',
                            User::ROLE_MANAGER => 'Менеджер',
                            User::ROLE_GUEST => 'Гость',
                    ],
                ]),

                'value' => function ($model) {
                    /* @var $model User */

                    switch ($model->role) {
                        case User::ROLE_ADMIN:
                            return 'Админ';
                        case User::ROLE_MANAGER:
                            return 'Менеджер';
                        case User::ROLE_GUEST:
                            return 'Гость';
                    }

                    return null;
                },
            ],
            //'created_at',
            //'updated_at',
            [
                'attribute' => 'deleted_at',
                'label' => 'Отключен',
                'format' => 'boolean',
                'filter' => null,
                'value' => function ($model) {
                    /* @var $model User */

                    return $model->deleted_at !== null;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
