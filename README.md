## The Problem
The client owns a car chip tuning business, which operates as a network of franchisees (dealers) across main cities of Russia, Belarus and Kazakhstan.

All the customer relationship, operational and finance issues are managed in a manual and decentralized manner: no common client/car data, GoogleSheets and telegram chats for coordination with franchisees.
Was good and agile for the start, but became a bottleneck for scaling of the business.

There were no system on the market which allowed to organise the process more effectively considering all the specific of the business and data.

## The Solution
The owner of the business asked to automate and digitalize the processes.
As an output we agreed to build a system which will allow:
1)CRM - the client + car data and visit status are centralized. All the products and car models are in the database for quick selection and synced with landing page for the clients.
2)There is a tool to automatically calculate dealer debt and balance according to all the services he has executed.
3)There is a possibility to control inventory, HQ office finance and HR management
