<?php

use yii\db\Migration;

/**
 * Class m200927_102140_rework_cars
 */
class m200927_102140_rework_cars extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('car_models','stock_power');
        $this->dropColumn('car_models','injection');
        $this->dropColumn('car_models','engine_name');
        $this->dropColumn('car_models','engine_volume');
        $this->dropColumn('car_models','gearbox_vers');
        $this->dropColumn('car_models','gearbox_model');
        $this->dropColumn('car_models','engine_type');

        $this->renameTable('car_models', 'car_chassis');
        $this->renameColumn('car_chassis', 'body', 'name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200927_102140_rework_cars cannot be reverted.\n";

        return false;
    }
}
