<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarEngines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-engines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'make_id')->dropDownList([
        $model->chassis->make_id ?? null => $model->chassis->make->name ?? null
    ], [
        'class' => 'car-make-selector form-control',
    ])?>

    <?= $form->field($model, 'model_id')->dropDownList([
        $model->chassis->model_id ?? null => $model->chassis->model->name ?? null
    ], [
        'class' => 'car-model-variants-selector form-control',
    ])?>

    <?= $form->field($model, 'chassis_id')->dropDownList([
        $model->chassis_id ?? null => $model->chassis->name ?? null
    ], [
        'class' => 'car-chassis-selector form-control',
    ])?>

    <?= $form->field($model, 'hp')->textInput() ?>

    <?= $form->field($model, 'nm')->textInput() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
