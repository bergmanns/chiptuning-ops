<?php

use admin\assets\LocationsBalanceAsset;
use admin\assets\PaymentFormAsset;
use admin\widgets\DropDownList;
use common\models\Accounts;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Locations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Debts */

PaymentFormAsset::register($this);
LocationsBalanceAsset::register($this);

$this->title = 'Погашение долга';
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['location/index']];
$this->params['breadcrumbs'][] = ['label' => 'Долги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::findAll(['deleted_at' => null]), 'id', 'name')) ?>
            <p class="location-balance help-block hidden">Баланс: <span class="location-balance-value"></span> ₽</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryTypes::findAll(['active' => 1]), 'id', 'name')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::findAll(['active' => 1]), 'id', 'name')) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-2">
            <label>Счет</label>
            <?= DropDownList::widget([
                'name' => 'account_id_selector',
                'data' => ArrayHelper::map(Accounts::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выбрать счет ...',
                    'class' => 'payment-account-input'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <label>Сумма</label>
            <?= Html::input('text', 'payment-amount-input', null, [
                'class' => 'form-control payment-amount-input',
            ]) ?>
        </div>
        <div class="col-md-2">  <?= Html::button('Добавить', ['class' => 'btn btn-success add-payment-form', 'style' => 'margin-top: 25px;']) ?></div>
    </div>
    <div class="form-group payments-result container-fluid" data-payments-count="<?=$model->getPayments()->count() ?>">
    </div>
    <div class="form-group">
        <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Погасить долг', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
