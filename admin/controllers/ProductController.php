<?php

namespace admin\controllers;

use common\components\product\LandingImporter;
use common\models\User;
use Yii;
use common\models\Products;
use admin\models\SearchProduct;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ProductController implements the CRUD actions for Products model.
 */
class ProductController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProduct();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Products model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Products();
        $model->is_manual = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Products model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGet()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $makeId = Yii::$app->request->get('make_id');
        $chassisId = Yii::$app->request->get('chassis_id');
        $modelId = Yii::$app->request->get('model_id');
        $engineId = Yii::$app->request->get('engine_id');
        $gearboxId = Yii::$app->request->get('gearbox_id');
        $name = Yii::$app->request->get('term');

        $query = Products::find()
            ->andWhere(
                [
                    'or',
                    [
                        'chassis_id' => $chassisId,
                        'make_id' => $makeId,
                        'model_id' => $modelId,
                        'engine_id' => $engineId,
                    ],
                    [
                        'chassis_id' => $chassisId,
                        'make_id' => $makeId,
                        'model_id' => $modelId,
                        'gearbox_id' => $gearboxId,
                    ],
                    [
                        'make_id' => null,
                        'gearbox_id' => $gearboxId,
                        'model_id' => null,
                        'chassis_id' => null,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => $makeId,
                        'gearbox_id' => $gearboxId,
                        'model_id' => null,
                        'chassis_id' => null,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => $makeId,
                        'gearbox_id' => $gearboxId,
                        'model_id' => $modelId,
                        'chassis_id' => null,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => $makeId,
                        'gearbox_id' => $gearboxId,
                        'model_id' => $modelId,
                        'chassis_id' => $chassisId,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => $makeId,
                        'model_id' => null,
                        'chassis_id' => null,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => null,
                        'model_id' => $modelId,
                        'chassis_id' => null,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => null,
                        'model_id' => null,
                        'gearbox_id' => null,
                        'chassis_id' => $chassisId,
                        'engine_id' => null,
                    ],
                    [
                        'make_id' => null,
                        'model_id' => null,
                        'chassis_id' => null,
                        'gearbox_id' => null,
                        'engine_id' => $engineId,
                    ],
                    [
                        'make_id' => null,
                        'model_id' => null,
                        'chassis_id' => null,
                        'engine_id' => null,
                        'gearbox_id' => null,
                    ],
            ])
        ;

        $query
            ->andFilterWhere(['like', 'name', $name]);

        $availableProducts = $query->andWhere([
            'status' => 'active',
        ])->all();

        $result = [];
        $groups = [];

        /** @var Products $product */
        foreach ($availableProducts as $product) {
            $categoryName = $product->category->name ?? '';
            $groups[$categoryName][] = [
                'id' => $product->id,
                'text' => sprintf('%s (%s ₽)', $product->name, $product->price),
            ];
        }

        foreach ($groups as $groupName => $children) {
            $result[] = [
                'text' => $groupName,
                'children' => $children,
            ];
        }

        return $result;
    }

    public function actionImport()
    {
        if (Yii::$app->user->getIdentity()->role !== User::ROLE_ADMIN) {
            throw new ForbiddenHttpException('Only for admin');
        }

        /** @var LandingImporter $importer */
        $importer = Yii::$app->landingImporter;

        $importer->import();
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
