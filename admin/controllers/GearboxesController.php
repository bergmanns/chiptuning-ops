<?php

namespace admin\controllers;

use Yii;
use common\models\CarGearboxes;
use admin\models\CarGearboxesSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * GearboxesController implements the CRUD actions for CarGearboxes model.
 */
class GearboxesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionGet($make_id = null, $model_id = null, $chassis_id = null, $name = null)
    {
        $makeQuery = CarGearboxes::find()
            ->select([
                'id',
                'name as text'
            ])
            ->andWhere([
                'deleted_at' => null,
            ])
            ->andWhere(
                [
                    'or',
                    [
                        'make_id' => $make_id,
                        'model_id' => $model_id,
                        'chassis_id' => $chassis_id,
                    ],
                    [
                        'make_id' => $make_id,
                        'model_id' => null,
                        'chassis_id' => null,
                    ],
                ]
            )
        ;

        if ($name !== null) {
            $makeQuery->andWhere(['like', 'name', $name]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $make = $makeQuery->asArray()->all();

        return [
            'items' => $make,
        ];
    }

    /**
     * Lists all CarGearboxes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarGearboxesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarGearboxes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarGearboxes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarGearboxes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CarGearboxes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) === true) {
            if (empty(ArrayHelper::getValue(Yii::$app->request->post(), 'CarGearboxes.engine_id')) === true) {
                $model->engine_id = null;
            }

            if (empty(ArrayHelper::getValue(Yii::$app->request->post(), 'CarGearboxes.chassis_id')) === true) {
                $model->chassis_id = null;
            }

            if (empty(ArrayHelper::getValue(Yii::$app->request->post(), 'CarGearboxes.model_id')) === true) {
                $model->model_id = null;
            }

            if ($model->save() === true) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CarGearboxes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarGearboxes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarGearboxes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarGearboxes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
