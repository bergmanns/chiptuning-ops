<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Account Balance asset bundle.
 */
class AccountBalanceAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/account-balance-form.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'kartik\select2\Select2Asset',
        'kartik\select2\ThemeKrajeeAsset',
        'yii\bootstrap\BootstrapAsset',
        'admin\assets\VueAsset',
        'admin\assets\VueResourceAsset',
        'admin\assets\SweetAsset',
    ];
}
