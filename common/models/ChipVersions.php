<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "chip_versions".
 *
 * @property int $id
 * @property string|null $num
 * @property string|null $file_link
 *
 * @property Chips[] $chips
 */
class ChipVersions extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chip_versions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num', 'file_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'num' => Yii::t('app', 'Num'),
            'file_link' => Yii::t('app', 'File Link'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getChips()
    {
        return $this->hasMany(Chips::class, ['ver_id' => 'id']);
    }
}
