<?php

use admin\widgets\DropDownList;
use common\components\user\AccessChecker;
use common\models\CarChassis;
use common\models\CarEngines;
use common\models\CarGearboxes;
use common\models\CarMakeVariants;
use common\models\CarModelVariants;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CarGearboxesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Коробки';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-gearboxes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
            echo Html::a('Добавить коробку', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-default']) ?>

    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'make_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'make_id',
                    'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    return $model->make->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'attribute' => 'model_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'model_id',
                    'data' => ArrayHelper::map(CarModelVariants::find()->andFilterWhere(['make_id' => $searchModel->make_id])->all(), 'id', 'name')
                ]),
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    return $model->model->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'attribute' => 'chassis_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'chassis_id',
                    'data' => ArrayHelper::map(
                        CarChassis::find()
                            ->andFilterWhere([
                                'make_id' => $searchModel->make_id,
                                'model_id' => $searchModel->model_id,
                            ])
                            ->all()
                        , 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    return $model->chassis->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],

            [
                'attribute' => 'engine_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'engine_id',
                    'data' => ArrayHelper::map(
                        CarEngines::find()
                            ->andFilterWhere([
                                'chassis_id' => $searchModel->chassis_id,
                            ])
                            ->all()
                        , 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    return $model->engine->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],

            [
                'attribute' => 'type',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'type',
                    'data' => ArrayHelper::map(CarGearboxes::find()->select('type')->distinct()->andFilterWhere([
                        'make_id' => $searchModel->make_id,
                        'model_id' => $searchModel->model_id,
                        'chassis_id' => $searchModel->chassis_id,
                    ])->all(), 'type', 'type'),
                ]),
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    if ($model->type !== null) {
                        return $model->type;
                    }

                    return null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            'name',
            //'create_at',
            //'update_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
