<?php

use yii\db\Migration;

/**
 * Class m200704_094625_add_accounts_table
 */
class m200704_094625_add_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('accounts', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'account_num' => $this->string(),
            'table_visible' => $this->boolean()->defaultValue(1),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->insert('accounts', [
            'name' => 'Карта',
            'account_num' => '1',
        ]);

        $this->insert('accounts', [
            'name' => 'Карта Олег',
            'account_num' => '2',
        ]);

        $this->insert('accounts', [
            'name' => 'Касса',
            'account_num' => '3',
        ]);

        $this->addForeignKey(
            'payments_account_key',
            'payments',
            'account_id',
            'accounts',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts');
    }
}
