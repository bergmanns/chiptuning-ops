<?php

declare(strict_types = 1);

namespace admin\actions\visit\api;

use admin\models\Debts;
use admin\models\User;
use Throwable;
use Yii;
use yii\base\Action;
use yii\db\ActiveQuery;
use yii\web\Response;

class PaymentsAction extends Action
{
    /**
     * @var ActiveQuery
     */
    private $query;

    /**
     * @var User
     */
    private $user;


    /**
     * Search Payments by visit ID
     * @return mixed
     */
    public function run($id)
    {
        $this->initializeUser();
        $this->initializeQuery($id);
        $repayment = true;
        $result = [];

        Yii::debug($this->user->location->share);

        if ($this->user->role === User::ROLE_MANAGER && $this->user->location->share > 0) {
            $repayment = false;
        }

        /** @var Debts $payment */
        foreach ($this->query->each() as $payment) {
            try {
                if ($payment->paid_amount >= $payment->amount) {
                    $status = 'Оплачен';
                } elseif ($payment->paid_amount > 0) {
                    $status = 'Частично оплачен';
                } else {
                    $status = 'Не оплачен';
                }
                Yii::debug($payment);

                if (in_array($status, ['Не оплачен', 'Частично оплачен']) === true && $repayment) {
                    $repayment = true;
                } else {
                    $repayment = false;
                }

                $result[] = [
                    'id' => $payment->id,
                    'name' => $payment->name,
                    'status' => $status,
                    'amount' => $payment->amount,
                    'paid_amount' => $payment->paid_amount,
                    'created_at' => $payment->created_at,
                    'repayment' => $repayment,
                    'type' => [
                        'id' => $payment->type_id,
                        'name' => $payment->type->name ?? null,
                    ],
                    // todo Переделать долги, что б было понятно с какого аккаунта был погашен

//                    'account' => [
//                        'id' => $payment->account_id,
//                        'name' => $payment->account->name ?? null,
//                    ],
                    'category' => [
                        'id' => $payment->category_id,
                        'name' => $payment->category->name ?? null,
                    ],
                ];
            } catch (Throwable $e) {
                Yii::error($e);
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $result;
    }

    private function initializeQuery($id)
    {
        $this->query = Debts::find()->where([
            'visit_id' => $id,
        ])->orderBy('created_at');


        if ($this->user->role === User::ROLE_MANAGER) {
            $this->query->joinWith(['visit'])->andWhere([
                'visits.location_id' => $this->user->location_id,
            ]);
        }
    }

    private function initializeUser()
    {
        $this->user = Yii::$app->user->getIdentity();
    }
}
