<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chip_versions}}`.
 */
class m200130_164108_create_chip_versions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chip_versions}}', [
            'id' => $this->primaryKey(),
            'num' => $this->string(),
            'file_link' => $this->string(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'chips_version_key',
            'chips',
            'ver_id',
            'chip_versions',
            'id'
        );

        $this->addForeignKey(
            'cars_chip_key',
            'cars',
            'chip_id',
            'chips',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('chips_version_key', 'chips');

        $this->dropTable('{{%chip_versions}}');
    }
}
