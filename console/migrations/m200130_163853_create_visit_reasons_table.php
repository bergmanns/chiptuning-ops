<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visit_reasons}}`.
 */
class m200130_163853_create_visit_reasons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visit_reasons}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'visit_reason_key',
            'visits',
            'category_id',
            'visit_reasons',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visit_reasons}}');
    }
}
