<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Phone InputMask asset bundle.
 */
class PhoneMaskAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    ];
    public $js = [
        'js/input-phone-component.js'
    ];
    public $depends = [
        'admin\assets\VueAsset',
        'admin\assets\VueResourceAsset',
        'admin\assets\InputMultiMaskAsset',
    ];
}
