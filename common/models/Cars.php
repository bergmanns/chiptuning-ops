<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cars".
 *
 * @property int $id
 * @property int $engine_id
 * @property int $gearbox_id
 * @property int $client_id
 * @property int|null $chip_id
 * @property string|null $issue_year
 * @property string|null $vin_number
 * @property string|null $gos_number
 * @property string|null $eng_chip_status
 * @property string|null $gear_chip_status
 * @property string|null $files_link
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property string|null $model_name
 * @property string|null $make_name
 * @property string|null $fullName
 *
 * @property Chips $chip
 * @property Clients $client
 * @property CarChassis $chassis
 * @property CarEngines $engine
 * @property CarGearboxes $gearbox
 * @property CarMakeVariants $make
 * @property CarModelVariants $model
 * @property Logs[] $logs
 * @property Visits[] $visits
 */
class Cars extends ActiveRecord
{
    public $make_id;
    public $model_id;
    public $chassis_id;
    public $engine_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cars';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'softdeleteable' => [
                'class' => 'common\behaviors\SoftDeleteableBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['engine_id', 'client_id'], 'required'],
            [['engine_id', 'client_id', 'chip_id', 'gearbox_id'], 'integer'],
            [['eng_chip_status', 'gear_chip_status'], 'string'],
            [['eng_chip_status', 'gear_chip_status'], 'default', 'value' => 'stock'],
            [['issue_year'], 'date', 'format' => 'YYYY'],
            [['vin_number', 'gos_number', 'files_link'], 'string', 'max' => 255],
            [['engine_model'], 'string', 'max' => 20],
            [['chip_id'], 'exist', 'skipOnError' => true, 'targetClass' => Chips::class, 'targetAttribute' => ['chip_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::class, 'targetAttribute' => ['client_id' => 'id']],
            [['engine_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarEngines::class, 'targetAttribute' => ['engine_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'engine_id' => Yii::t('app', 'Двигатель'),
            'make_id' => Yii::t('app', 'Марка'),
            'model_id' => Yii::t('app', 'Модель'),
            'chassis_id' => Yii::t('app', 'Кузов'),
            'gearbox_id' => Yii::t('app', 'Коробка'),
            'client_id' => Yii::t('app', 'Владелец'),
            'chip_id' => Yii::t('app', 'Чип'),
            'issue_year' => Yii::t('app', 'Год выпуска'),
            'engine_model' => Yii::t('app', 'Модель двигателя'),
            'vin_number' => Yii::t('app', 'VIN'),
            'gos_number' => Yii::t('app', 'Гос. номер'),
            'eng_chip_status' => Yii::t('app', 'Чип статус мотора'),
            'gear_chip_status' => Yii::t('app', 'Чип статус коробки'),
            'files_link' => Yii::t('app', 'Files Link'),
            'created_at' => Yii::t('app', 'Добавлена'),
            'updated_at' => Yii::t('app', 'Изменена'),
            'deleted_at' => Yii::t('app', 'Удалена'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isAttributeChanged('vin_number')) {
            $this->issue_year = self::issueYearByVinNumber($this->vin_number);
        }

        return parent::beforeSave($insert);
    }

    public function getFullName(): string
    {
        return sprintf('%s %s', $this->chassis->name, $this->engine->fullName);
    }

    public function getSpecific(): string
    {
        if ($this->chassis === null) {
            return '';
        }

        return $this->chassis->getSpecific();
    }

    /**
     * @return ActiveQuery
     */
    public function getChip()
    {
        return $this->hasOne(Chips::class, ['id' => 'chip_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChassis()
    {
        return $this->hasOne(CarChassis::class, ['id' => 'chassis_id'])->via('engine');
    }

    /**
     * @return ActiveQuery
     */
    public function getEngine()
    {
        return $this->hasOne(CarEngines::class, ['id' => 'engine_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(CarModelVariants::class, ['id' => 'model_id'])->via('chassis');
    }

    /**
     * @return ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMakeVariants::class, ['id' => 'make_id'])->via('chassis');
    }

    /**
     * @return ActiveQuery
     */
    public function getGearbox()
    {
        return $this->hasOne(CarGearboxes::class, ['id' => 'gearbox_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Logs::class, ['car_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visits::class, ['car_id' => 'id']);
    }

    public function getLastMileAge()
    {
        $visit = Visits::find()->where([
            'car_id' => $this->id,
        ])->orderBy('created_at DESC')->one();

        return $visit->current_mileage ?? null;
    }

    public function setVin_number($vinNumber)
    {
        $this->vin_number = $vinNumber;
        $this->issue_year = self::issueYearByVinNumber($vinNumber);
    }

    public static function issueYearByVinNumber($vinNumber)
    {
        $years = [
            'Y' => 2000,
            '1' => 2001,
            '2' => 2002,
            '3' => 2003,
            '4' => 2004,
            '5' => 2005,
            '6' => 2006,
            '7' => 2007,
            '8' => 2008,
            '9' => 2009,
            'A' => 2010,
            'B' => 2011,
            'C' => 2012,
            'D' => 2013,
            'E' => 2014,
            'F' => 2015,
            'G' => 2016,
            'H' => 2017,
            'J' => 2018,
            'K' => 2019,
            'L' => 2020,
            'M' => 2021,
            'N' => 2022,
            'P' => 2023,
            'R' => 2024,
            'S' => 2025,
            'T' => 2026,
            'V' => 2027,
            'W' => 2028,
            'X' => 2029,
        ];
        if (is_string($vinNumber) && isset($vinNumber[9]) && isset($years[$vinNumber[9]])) {
            return $years[$vinNumber[9]];
        } else {
            return null;
        }
    }

    public static function normalizeEngineModel($value)
    {
        $value = preg_replace('/[\x{0410}-\x{042F}]+.*[\x{0410}-\x{042F}]+/iu', '', $value);
        $value = substr($value, 0, 20);
        return $value;
    }
}
