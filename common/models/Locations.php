<?php

namespace common\models;

use Carbon\CarbonTimeZone;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "locations".
 *
 * @property int $id
 * @property float $balance
 * @property int $region_id
 * @property string $name
 * @property int|null $share
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Regions $region
 * @property Countries $country
 * @property Payments[] $payments
 * @property User[] $user
 * @property Visits[] $visits
 * @property Clients[] $clients
 */
class Locations extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'name'], 'required'],
            [['region_id', 'share'], 'integer'],
            [['balance'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['share'], 'number', 'max' => 100, 'min' => 0],
            [['region_id'], 'exist', 'skipOnError' => true, 'targetClass' => Regions::class, 'targetAttribute' => ['region_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Регион'),
            'balance' => Yii::t('app', 'Баланс'),
            'name' => Yii::t('app', 'Название'),
            'share' => Yii::t('app', 'Доля'),
            'created_at' => Yii::t('app', 'Добавлен'),
            'updated_at' => Yii::t('app', 'Изменен'),
            'deleted_at' => Yii::t('app', 'Удален'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getRegion()
    {
        return $this->hasOne(Regions::class, ['id' => 'region_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::class, ['id' => 'country_id'])->via('region');
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::class, ['location_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasMany(User::class, ['location_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisits()
    {
        return $this->hasMany(Visits::class, ['location_id' => 'id']);
    }

    public function getTimeZone(): CarbonTimeZone
    {
        //todo Добавить поддержку разных тайм зон для офиса

        return new CarbonTimeZone('Europe/Moscow');
    }

    public function getClients()
    {
        return $this->hasMany(Clients::class, ['id' => 'client_id'])
                    ->viaTable('clients_locations_relation', ['location_id' => 'id']);
    }
}
