<?php

use yii\db\Migration;

/**
 * Class m210328_160907_add_landing_order_id
 */
class m210328_160907_add_landing_order_id extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('visits', 'external_id', $this->string());
        $this->addColumn('visits', 'total_amount', $this->float());
        $this->addColumn('visits', 'real_amount', $this->float());
        $this->addColumn('visits', 'is_landing_paid', $this->boolean()->defaultValue(0));
        $this->addColumn('visits', 'is_amount_confirmed', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('visits_products', 'amount');

        $this->dropColumn('visits', 'is_amount_confirmed');
        $this->dropColumn('visits', 'landing_paid');
        $this->dropColumn('visits', 'confirmed_amount');
        $this->dropColumn('visits', 'total_amount');
        $this->dropColumn('visits', 'external_id');

    }
}
