<?php

declare(strict_types = 1);

namespace admin\controllers;

use admin\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends Controller
{
    /**
     * @var User
     */
    protected $user;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function init(): void
    {
        $this->initializeUser();

        parent::init();
    }

    private function initializeUser(): void
    {
        $this->user = Yii::$app->user->getIdentity();
    }
}
