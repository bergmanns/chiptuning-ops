<?php

use yii\db\Migration;

/**
 * Class m201013_214752_fix_visit_reason_fk
 */
class m201013_214752_fix_visit_reason_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('visit_products_reason_key', 'visits_products');

        $this->addForeignKey(
            'visit_products_category_id_key',
            'visits_products',
            'category_id',
            'categories',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('visit_products_category_id_key', 'visits_products');
    }
}
