<?php

use common\components\user\AccessChecker;
use common\models\CarEngines;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CarEngines */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = ['label' => 'Двигатели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="car-engines-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::EDIT_CAR_ITEM) === true) {
            echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ;
            echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ;
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'value' => function ($model) {
                    /* @var $model CarEngines */
                    if ($model->chassis !== null) {
                        return $model->chassis->make->name;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'model_id',
                'label' => 'Модель',
                'value' => function ($model) {
                    /* @var $model CarEngines */
                    if ($model->chassis !== null) {
                        return $model->chassis->model->name;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'value' => function ($model) {
                    /* @var $model CarEngines */
                    if ($model->chassis !== null) {
                        return $model->chassis->name;
                    }

                    return null;
                },
            ],
            'hp',
            'nm',
            'name',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
    ]) ?>

</div>
