<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Vue framework asset bundle.
 */
class VueTableAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/vuetable/dist';
    public $js = [
        'vue-table.js',
    ];
    public $depends = [
        'admin\assets\VueAsset',
        'admin\assets\VueResourceAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
