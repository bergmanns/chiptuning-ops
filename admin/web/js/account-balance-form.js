// start app
new Vue({
    el: '#account-balance',
    data: {
        accounts: [
            {
                id: 1,
                name: "test",
                balance: 3500,
            },
            {
                id: 1,
                name: "35000",
                balance: 35000,
            }
        ],
        currentAccount: {
            id: null,
            name: null,
            balance: null,
        },
        selectedAccountId: null,
        newAccountBalance: null,
        showModal: false
    },
    created: function () {
        console.log(this.showModal);
        this.initAccounts();
    },
    methods: {
        initAccounts: function() {
            let self = this;
            let url = '/accounts/api/index';

            this.$http.get(url).then(function (response) {
                self.accounts = response.data;

                if (self.accounts.length > 0) {
                    self.currentAccount = response.data[0];
                }
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        updateAccountBalance: function() {
            let self = this;
            let url = '/accounts/api/balance';
            let param = $('meta[name=csrf-param]').attr("content");
            let token = $('meta[name=csrf-token]').attr("content");
            let body = {
                id: self.currentAccount.id,
                balance: self.currentAccount.balance,
                newBalance: self.newAccountBalance,
            };

            body[param] = token;
            this.$http.post(url, body, {
                emulateJSON: true
            }).then(function (response) {
                window.location.reload();
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
    }
})