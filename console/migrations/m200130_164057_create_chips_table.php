<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%chips}}`.
 */
class m200130_164057_create_chips_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%chips}}', [
            'id' => $this->primaryKey(),
            'staff_id' => $this->integer()->notNull(),
            'visit_id' => $this->integer(),
            'product_id' => $this->integer(),
            'ver_id' => $this->integer(),
            'payment_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'cancelled_at' => $this->dateTime(),
            'status' => "ENUM('chiped', 'fail', 'in_progress', 'cancelled')",
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'chips_staff_key',
            'chips',
            'staff_id',
            'staff',
            'id'
        );

        $this->addForeignKey(
            'chips_visits_key',
            'chips',
            'visit_id',
            'visits',
            'id'
        );

        $this->addForeignKey(
            'chips_products_key',
            'chips',
            'product_id',
            'products',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%chips}}');
    }
}
