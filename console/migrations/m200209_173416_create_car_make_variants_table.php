<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_make_variants}}`.
 */
class m200209_173416_create_car_make_variants_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_make_variants}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            'created_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'active' => $this->boolean(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_make_variants}}');
    }
}
