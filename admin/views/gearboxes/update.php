<?php

use admin\assets\CarAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CarGearboxes */

CarAsset::register($this);

$this->title = 'Изменить Коробку: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = ['label' => 'Коробки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="car-gearboxes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
