<?php

use yii\db\Migration;

/**
 * Class m200809_103230_add_type_to_categories_table
 */
class m200809_103230_add_type_to_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('categories', 'type_id', $this->integer()->after('id'));

        $this->createTable('category_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'active' => $this->boolean()->defaultValue(1),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'categories_type_key',
            'categories',
            'type_id',
            'category_types',
            'id'
        );

        $this->addForeignKey(
            'cash_flow_type_id_key',
            'cash_flow',
            'type_id',
            'category_types',
            'id'
        );

        $this->insert('category_types', [
            'name' => 'Визит',
            'active' => 1,
        ]);
        $this->insert('category_types', [
            'name' => 'Закупки',
            'active' => 1,
        ]);
        $this->insert('category_types', [
            'name' => 'Переводы',
            'active' => 1,
        ]);
        $this->insert('category_types', [
            'name' => 'Офис',
            'active' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category_types');
        $this->dropColumn('categories', 'type_id');
    }
}
