<?php

namespace common\components\exporter;

use Yii;
use yii\base\Component;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\i18n\Formatter;

class CsvExporter extends Component implements ExporterInterface
{
    /**
     * @var ActiveDataProvider
     */
    private $provider;
    /**
     * @var false|mixed|resource
     */
    private $file;
    /**
     * @var array|mixed
     */
    private $headers;
    /**
     * @var mixed|string
     */
    private $delimiter;
    /**
     * @var mixed|string
     */
    private $enclosure;

    public function formatProvider(ActiveDataProvider $provider, array $headers)
    {
        $this->provider = $provider;
        $this->file = $this->initFile();
        $this->headers = $headers;
        $this->setCsvControl();

        return $this->getExportedData();
    }

    /**
     * Sets the delimiter and escape character for CSV.
     */
    public function setCsvControl(string $delimiter = ';', string $enclosure = '"')
    {
        $this->delimiter = $delimiter;
        $this->enclosure = $enclosure;
    }

    public function getExportedData()
    {
        $handle = $this->file;
        $models = $this->provider->getModels();
        $headerRow = $this->getHeaderRow($models[0]);

        fputcsv($handle, $headerRow, $this->delimiter, $this->enclosure);

        foreach ($models as $model) {
            $row = [];

            foreach ($this->headers as $column) {
                $format = null;
                $value = null;
                $attribute = null;

                if (is_array($column)) {
                    $attribute = ArrayHelper::getValue($column, 'attribute');
                    $format = ArrayHelper::getValue($column, 'format');
                    $value = ArrayHelper::getValue($column, 'value');
                }

                if (is_string($column)) {
                    $format = null;
                    $value = null;
                    $attribute = $column;
                }

                $value = $this->getValue($model, $attribute, $value);
                $value = $this->getFormatted($model, $format, $value);
                $row[] = $value;
            }

            fputcsv($handle, $row, $this->delimiter, $this->enclosure);
        }

        rewind($handle);
        $output = stream_get_contents($handle);
        fclose($handle);

        return $output;
    }

    private function initFile($fileName = null)
    {
        if ($fileName !== null) {
            return fopen($fileName . '.csv', 'w');
        }

        return fopen('php://memory', 'r+b');
    }

    private function getValue(ActiveRecord $model, $attribute, $value = null)
    {
        if (is_callable($value) === true) {
            return call_user_func($value, $model);
        }

        return $model->getAttribute($attribute);
    }

    private function getFormatted($model, $format, $value)
    {
        if (is_callable($format) === true) {
            return call_user_func($format, $model, $value);
        }

        if (is_string($format)) {
            /** @var Formatter $formatter */
            $formatter = Yii::$app->getFormatter();

            return $formatter->format($value, $format);
        }

        return $value;
    }

    private function getHeaderRow(ActiveRecord $model)
    {
            $row = [];

            foreach ($this->headers as $column) {
                $label = null;
                $attribute = null;

                if (is_array($column)) {
                    $attribute = ArrayHelper::getValue($column, 'attribute');
                    $label = ArrayHelper::getValue($column, 'label');
                }

                if (is_string($column)) {
                    $attribute = $column;
                }

                $label = $label ?? $model->getAttributeLabel($attribute);
                $row[] = $label;
            }

            return $row;
    }
}