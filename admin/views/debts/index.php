<?php

use common\models\Categories;
use common\models\CategoryTypes;
use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\DebtsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Долги по филиалам';
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['location/index']];
$this->params['breadcrumbs'][] = $this->title;
$role = Yii::$app->user->getIdentity()->role;
?>
<div class="debts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row form-inline">
        <div class="col-md-6 form-inline">
            <?php if ($role === User::ROLE_ADMIN) {
                echo Html::a('Добавить долг', ['create'], ['class' => 'btn btn-success']);
            } ?>
            <?= Html::a('Погасить долг', ['repayment'], ['class' => 'btn btn-default']) ?>
        </div>
        <div class="col-md-6 form-inline">
            <div class="form-inline form-group pull-right">
                <span>Всего долг: <?=$searchModel->total_amount?> ₽</span>
            </div>
        </div>
    </div>
    </p>
    <p>

    </p>
    <p>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Выгрузить в csv', ['csv?'. Yii::$app->request->getQueryString()], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => require_once 'debtsColumns.php',
    ]); ?>


</div>
