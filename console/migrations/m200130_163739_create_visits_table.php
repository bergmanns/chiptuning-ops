<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%visits}}`.
 */
class m200130_163739_create_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%visits}}', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'contact_id' => $this->integer()->notNull(),
            'location_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'mileage' => $this->integer(),
            'current_mileage' => $this->integer(),
            'status' => "ENUM('new', 'cancelled', 'done', 'active')",
            'source' => $this->string(),
            'visit_time' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'visits_location_key',
            'visits',
            'location_id',
            'locations',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%visits}}');
    }
}
