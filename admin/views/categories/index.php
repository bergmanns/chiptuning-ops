<?php

use admin\widgets\DropDownList;
use common\models\Categories;
use common\models\CategoryTypes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории платежа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>

    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'type_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'type_id',
                    'data' => ArrayHelper::map(
                        CategoryTypes::findAll(['active' => 1]),
                        'id', 'name'
                    ),
                ]),
                'headerOptions' => [
                    'style' => 'width: 15%',
                ],
                'value' => function ($model) {
                    /* @var $model Categories */
                    return $model->type->name ?? null;
                },
            ],
            'name',
            'active:boolean',
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>


</div>
