<?php

use admin\assets\CarAsset;
use common\models\Categories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */

CarAsset::register($this);
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'make_id')->dropDownList(
        [
            $model->chassis->make_id ?? null => $model->chassis->make->name ?? null
        ]
        , [
        'class' => 'car-make-selector form-control',
    ])?>

    <?= $form->field($model, 'model_id')->dropDownList([
        $model->chassis->model_id ?? null => $model->chassis->model->name ?? null
    ], [
        'class' => 'car-model-variants-selector form-control',
    ])?>

    <?= $form->field($model, 'chassis_id')->dropDownList([
        $model->chassis->id ?? null => $model->chassis->name ?? null
    ], [
        'class' => 'car-chassis-selector form-control',
    ])?>

    <?= $form->field($model, 'engine_id')->dropDownList([
        $model->engine_id ?? null => $model->engine->fullName ?? null
    ], [
        'class' => 'car-engine-selector form-control',
    ])?>

    <?= $form->field($model, 'gearbox_id')->dropDownList([
        $model->gearbox_id ?? null => $model->gearbox->name ?? null
    ], [
        'class' => 'car-gearbox-selector form-control',
    ])?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::findAll(['active' => 1, 'for_visit' => 1]), 'id', 'name'))?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
            'obsolete' => 'Устаревший',
            'active' => 'Доступен',
        ], ['prompt' => '']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
