<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gearboxes}}`.
 */
class m201004_173340_create_gearboxes_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_gearboxes}}', [
            'id' => $this->primaryKey(),
            'make_id' => $this->integer(),
            'model_id' => $this->integer(),
            'chassis_id' => $this->integer(),
            'engine_id' => $this->integer(),
            'name' => $this->string(255),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'car_gearboxes_make_id_key',
            'car_gearboxes',
            'make_id',
            'car_make_variants',
            'id'
        );
        $this->addForeignKey(
            'car_gearboxes_model_id_key',
            'car_gearboxes',
            'model_id',
            'car_model_variants',
            'id'
        );
        $this->addForeignKey(
            'car_gearboxes_chassis_id_key',
            'car_gearboxes',
            'chassis_id',
            'car_chassis',
            'id'
        );
        $this->addForeignKey(
            'car_gearboxes_engine_id_key',
            'car_gearboxes',
            'engine_id',
            'car_engines',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gearboxes}}');
    }
}
