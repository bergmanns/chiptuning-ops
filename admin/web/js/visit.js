
$( document ).ready(function() {
    console.log('ready');
    let clientIdSelector = $('#visits-client_id');
    var self = this;
    this.client = {};
    this.cars = [];
    this.selectedCar = {};

    initializeClient(clientIdSelector.val());

    $('.add-product').on('click', function(event){
        addProductRow();
    });
    $('.products-info-block').on('click', '.delete-product-row', function(event) {
        console.log(this);
        deleteRow(event, this);
    });

    function clearProductRow(productRow) {

    }
    clientIdSelector.on('select2:select', function(event){
        let clientId = event.params.data.id;

        initializeClient(clientId);
    });

    $('.client-car-block').find('#visits-car_id').on('select2:select', function (e) {
        let carId = parseInt(e.params.data.id);
        let selectedCar = self.cars.find(car => car.id === carId);
        if(typeof(selectedCar) != "undefined" && selectedCar !== null) {
            self.selectedCar = selectedCar;

            renderSelectedCar(self.selectedCar);
        } else {
            initializeAddCar();
        }
    });

    function addProductRow() {
        let lastProductRow = $('.product-row').last();
        let index = parseInt(lastProductRow.attr('date-product-index'));
        let newProductRow = lastProductRow.clone();
        newProductRow.attr('date-product-index', index + 1);
        newProductRow.find(`select[name='VisitsProducts[${index}][product_id]']`).attr('name', `VisitsProducts[${index + 1}][product_id]`).val(null);
        newProductRow.find(`select[name='VisitsProducts[${index}][category_id]']`).attr('name', `VisitsProducts[${index + 1}][category_id]`).val(null);
        newProductRow.find(`input[name='VisitsProducts[${index}][visit_at]']`).attr('name', `VisitsProducts[${index + 1}][visit_at]`).val(null);
        newProductRow.find(`#visitsproducts-visit_at_${index}`).attr('id', `visitsproducts-visit_at_${index + 1}`);
        newProductRow.find(`textarea[name='VisitsProducts[${index}][comment]']`).attr('name', `VisitsProducts[${index + 1}][comment]`).val(null);
        newProductRow.find(`#visitsproducts-comment_${index}`).attr('id', `visitsproducts-comment_${index + 1}`);
        // newProductRow.find(`VisitsProducts[][product_id]`);
        console.log(index);

        console.log(lastProductRow);
        newProductRow.removeClass('hidden').appendTo('.products-info-block');

        console.log(newProductRow);
    }

    function renderClientFields(client) {
        let clientBlock = $('.client-info-block');
        clientBlock.find('#visits-client_phone').removeClass('hidden').val(client.phone).attr('disabled', true);
        clientBlock.find('#visits-client_name').removeClass('hidden').val(client.name).attr('disabled', true);
    }

    function finalizeS2Field(selector, data, disable = true, selected = true, ) {
        selector.removeClass('hidden');

        if (selector.find("option[value='" + data.id + "']").length) {
            selector.val(data.id).trigger('change');
        } else {
            let newOption = new Option(data.text, data.id, selected, selected);

            selector.append(newOption).trigger('change');
        }

        if (disable) {
            selector.attr('disabled', disable);
        }
    }

    function renderSelectedCar(car) {
        let carBlock = $('.client-car-block');

        finalizeS2Field(carBlock.find('#visits-car_id'), {id: car.id, text: car.name}, false);
        finalizeS2Field(carBlock.find('#visits-client_car_make'), {id: car.make_name, text: car.make_name});
        finalizeS2Field(carBlock.find('#visits-client_car_model'), {id: car.make_name, text: car.model_name + ' ' + car.model_body});
        carBlock.find('#visits-vin_number').removeClass('hidden').attr('disabled', true).val(car.vin_number);
        carBlock.find('#visits-gos_number').removeClass('hidden').attr('disabled', true).val(car.gos_number);
    }

    function initializeAddCar() {
        let carBlock = $('.client-car-block');

        $("#visits-client_car_make").select2({
            theme: 'krajee',
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/car-make-variant/get",
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    console.log(params);
                    return {
                        make: params.term, // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.items,
                    };
                },
                cache: true
            },
            placeholder: 'Выберите марку',
            minimumInputLength: 0,
        }).on('select2:select', function (e) {
            var makeSelected = e.params.data.id;
            $('#visits-client_car_model').val(null).select2({
                theme: 'krajee',
                allowClear: true,
                width: '100%',
                ajax: {
                    url: "/car-model/get",
                    dataType: 'json',
                    delay: 500,
                    data: function (params) {
                        console.log(params);
                        return {
                            model: params.term,
                            make_id: makeSelected,
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: data.items,
                        };
                    },
                    cache: true
                },
                placeholder: 'Выберите модель',
                minimumInputLength: 0,
            });
        });

        carBlock.find('#visits-client_car_make').removeClass('hidden').removeAttr('disabled');
        carBlock.find('#visits-client_car_model').removeClass('hidden').val('').removeAttr('disabled');
        carBlock.find('#visits-vin_number').removeClass('hidden').val('').removeAttr('disabled');
        carBlock.find('#visits-gos_number').removeClass('hidden').val('').removeAttr('disabled');
        carBlock.find('#visits-current_mileage').removeClass('hidden').val('').removeAttr('disabled');
    }

    function renderClientCarsBlock(cars) {
        let carBlock = $('.client-car-block');

        if (cars.length > 1) {
            let carIdBlock = carBlock.find('#visits-car_id');

            cars.forEach(element => {
                finalizeS2Field(carIdBlock, {id: element.id, text: element.name}, false, false);
            });
        } else if ((cars.length === 1)) {
            renderSelectedCar(cars[0]);
        } else {
            initializeAddCar();
        }
    }

    function resetCarsBlock() {
        $('#visits-car_id').val(null).trigger('change');
        $('#visits-client_car_make').val(null).trigger('change');
        $('#visits-client_car_model').val(null).trigger('change');
        $('#visits-vin_number').removeClass('hidden').val(null).attr('disabled', true);
        $('#visits-gos_number').removeClass('hidden').val(null).attr('disabled', true);
        $('#visits-current_mileage').removeClass('hidden').val(null).attr('disabled', true);
    }

    function initializeClient(clientId) {
        if (parseInt(clientId) === 0) {
            $('#visits-client_phone').removeClass('hidden').val('').removeAttr('disabled');
            $('#visits-client_name').removeClass('hidden').val('').removeAttr('disabled');

            resetCarsBlock();
            renderClientCarsBlock([])
        } else {
            $.ajax({
                url: '/client/info/',
                data: {
                    'id': clientId,
                },
                type: 'GET',
                success: function(data) {
                    console.log(data);
                    self.client = data.client;
                    self.cars = data.cars;

                    renderClientFields(self.client);
                    renderClientCarsBlock(self.cars)
                },
                error: function(error, eer) {
                    console.log(error);
                    console.log( $('#field-visits-client_id ')
                        .removeClass('has-success')
                        .addClass('has-error'))
                    ;
                }
            });
        }
    }

    function addProduct(event) {

    }

    function deleteRow(event, rowElement) {
        let productRowCount = $('.product-row').length;
        console.log(productRowCount);
        let  rowSelector = $(rowElement).closest('.product-row');
        console.log(rowSelector);
        if (productRowCount > 1) {
            console.log('delete');
            rowSelector.remove();
        } else {
            let index = parseInt(rowSelector.attr('date-product-index'));
            rowSelector.attr('date-product-index', 0);
            rowSelector
                .find(`select[name='VisitsProducts[${index}][product_id]']`)
                .val(null)
                .attr('name', `VisitsProducts[0][product_id]`)
            ;
            rowSelector
                .find(`select[name='VisitsProducts[${index}][category_id]']`)
                .val(null)
                .attr('name', `VisitsProducts[0][category_id]`)
            ;
            rowSelector.find(`input[name='VisitsProducts[${index}][visit_at]']`)
                .attr('name', `VisitsProducts[0][visit_at]`)
                .val(null)
            ;
            rowSelector
                .find(`textarea[name='VisitsProducts[${index}][comment]']`)
                .val(null)
                .attr('id', `visitsproducts-visit_at_0`)
                .attr('name', `VisitsProducts[0][comment]`)
            ;
        }
    }
});



//         let dateTimePiker = lastProductRow.find('.visitsproducts_visit_at').datetimepicker({
//             'language': 'ru',
//             'todayHighlight': false,
//             'autoclose' : true,
//             'minuteStep' : 10,
//             'weekStart' : 1,
//             'startDate' : new Date(),
//             'daysOfWeekDisabled' : [0, 6],
//             'datesDisabled' : ['2020-02-20 14:50'],
// //           'minutesDisabled' : [0, 1, 2, 3, 4, 5],
//             'hoursDisabled' : [0, 1, 2, 3, 4, 5, 20, 21, 22, 23]
//         });