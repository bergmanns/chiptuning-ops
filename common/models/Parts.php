<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "parts".
 *
 * @property int $id
 * @property int|null $visit_product_id
 * @property int|null $debt_id
 * @property int|null $location_id
 * @property string|null $name
 * @property string|null $serial
 * @property int|null $count
 * @property float|null $amount
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property float|null $price
 * @property Debts $debt
 * @property Locations $location
 * @property VisitsProducts $visitProduct
 */
class Parts extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visit_product_id', 'debt_id', 'location_id', 'count'], 'integer'],
            [['amount'], 'number'],
            [['name', 'serial'], 'string', 'max' => 255],
            [['debt_id'], 'exist', 'skipOnError' => true, 'targetClass' => Debts::class, 'targetAttribute' => ['debt_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::class, 'targetAttribute' => ['location_id' => 'id']],
            [['visit_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => VisitsProducts::class, 'targetAttribute' => ['visit_product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'visit_product_id' => 'Визит',
            'debt_id' => 'Долг',
            'location_id' => 'Филиал',
            'name' => 'Название',
            'serial' => 'Серийник',
            'count' => 'Количество',
            'amount' => 'Стоимость',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
            'deleted_at' => 'Удалено',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * Gets query for [[Debt]].
     *
     * @return ActiveQuery
     */
    public function getDebt()
    {
        return $this->hasOne(Debts::class, ['id' => 'debt_id']);
    }

    /**
     * Gets query for [[Location]].
     *
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id']);
    }

    /**
     * Gets query for [[Visit]].
     *
     * @return ActiveQuery
     */
    public function getVisitProduct()
    {
        return $this->hasOne(VisitsProducts::class, ['id' => 'visit_product_id']);
    }

    public function getPrice()
    {
        return $this->count ? $this->amount * $this->count : null;
    }
}
