<?php

use admin\assets\LocationsBalanceAsset;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баланс филиалов';
$this->params['breadcrumbs'][] = $this->title;

LocationsBalanceAsset::register($this);
?>
<div class="locations-index" id="location-balance">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="modal fade" id="locations-balance-modal" tabindex="-1" role="dialog" aria-labelledby="locations-balance-modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="locations-balance-modal-label">Коррекция баланса</h4>
                    <p class=divider></p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="location-name" class="control-label">Аккаунт:</label>
                                    <select type="text" v-model="currentLocation" class="form-control" id="location-name">
                                        <template v-for="location in locations">
                                            <option v-bind:value="location">{{location.name}}</option>
                                        </template>
                                    </select>
                                <p class="help-block">Баланс: {{ currentLocation.balance }} ₽</p>
                            </div>
                            <div class="form-group">
                                <label for="account-value" class="control-label">Новый баланс:</label>
                                <input v-model="newLocationBalance"  type="text" class="form-control" id="account-value">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-primary" @click="updateLocationBalance">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <p>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#locations-balance-modal">
            Коррекция
        </button>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'contentOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'attribute' => 'balance',
                'format' => 'currency',
                'contentOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
        ],
    ]); ?>


</div>
