$( document ).ready(function() {
    let paymentItems = [];
    let index = $('.payments-result').attr('data-payments-count');

    $('.add-payment-form').on('click', addPaymentItem);
    $(document).on('click', '.remove-item', removeItem);

    function clearError(element) {
        element.closest('.form-group').removeClass('has-error');
        element.parent().find('.help-block').remove();
    }

    function addError(element, description) {
        element.closest('.form-group').addClass('has-error');
        element.after('<div class="help-block">' + description + '</div>')
    }

    function addItem(accountId, accountName, sumValue) {
        let template = `<div class="payment-row">
            <input type="hidden" name="payments[${index}][account_id]" value="${accountId}">
            <input type="hidden" name="payments[${index}][value]" value="${sumValue}">
            <span class="account-id"><span class="badge">New</span> ${accountName}: </span><span class="account-value">${sumValue} ₽</span>
            <button account-id="${accountId}" type="button" class="btn btn-sm btn-default remove-item"><i class="glyphicon glyphicon-trash"></i></button>
        </div>`

        $('.payments-result').append(template);

        index++;
    }

    function removeItem(e) {
        let element = $(e.target);
        console.log(element);
        element.closest('.payment-row').remove();
    }

    function addPaymentItem(e) {
        let account = $('.payment-account-input');
        let sum = $('.payment-amount-input');
        let accountId = account.val();
        let sumValue = sum.val();

        clearError(sum);

        console.log(accountId);
        console.log(sumValue);

        if (accountId === '') {
            addError(sum, 'Необходимо выбрать счет');

            return;
        }

        let accountName = account.select2('data')[0]['text'];
        console.log(accountName);

        if (sumValue === '') {
            addError(sum, 'Необходимо заполнить');

            return;
        }

        if (isNaN(parseInt(sumValue))) {
            addError(sum, 'Необходимо указать число');

            return;
        }

        addItem(accountId, accountName, sumValue);
        account.val(null).trigger('change');
        sum.val(null);
    }
});
