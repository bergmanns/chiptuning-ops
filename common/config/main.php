<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'log',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'landingImporter' => [
            'class' => 'common\components\product\LandingImporter',
        ],
        'telegramTransport' => [
            'class' => 'common\components\transport\TelegramTransport',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'file' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => [
                        'error',
                        'warning',
                    ],
                ],
                'telegram' => [
                    'class' => 'common\components\log\TelegramTarget',
                    'levels' => [
                        'error',
                    ],
                    'except' => [
//                        'yii\web\HttpException:*',
                    ],
                ],
                'sentry' => [
                    'class' => 'common\components\log\SentryTarget',
                    'levels' => [
                        'error',
                    ],
                    'except' => [
//                        'yii\web\HttpException:400',
//                        'yii\web\HttpException:404',
//                        'yii\web\HttpException:403',
//                        'yii\web\HttpException:401',
                    ],
                ],
            ],
        ],
    ],
];
