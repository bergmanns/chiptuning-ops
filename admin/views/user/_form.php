<?php

use admin\models\User;
use common\models\Locations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true,  'class' => 'phone_mask form-control']) ?>
    <?= $form->field($model, 'location_id')->dropDownList(
        ArrayHelper::map(Locations::findAll(['deleted_at' => null]), 'id', 'name')
    )?>
    <?= $form->field($model, 'role')->dropDownList(
        [
            User::ROLE_ADMIN => 'Админ',
            User::ROLE_MANAGER => 'Менеджер',
            User::ROLE_GUEST => 'Гость',
        ]
    )?>
    <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'new_password_confirm')->passwordInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
