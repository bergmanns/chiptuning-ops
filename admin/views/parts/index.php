<?php

use admin\widgets\DropDownList;
use common\models\Locations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\SearchParts */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запчасти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'location_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'location_id',
                    'data' => ArrayHelper::map(Locations::find()->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 20%',
                ],
                'value' => function ($model) {
                    /* @var $model \common\models\Parts} */
                    return $model->location->name ?? null;
                },
            ],
            'name',
            'serial',
            'count',
            'amount',
            'created_at:datetime',
            //'updated_at',
            //'deleted_at',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => Yii::$app->user->getIdentity()->role ?? null,
            ],
        ],
    ]); ?>


</div>
