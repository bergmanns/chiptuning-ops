<?php

declare(strict_types = 1);

namespace admin\actions\visit\api;

use admin\models\Debts;
use Carbon\CarbonImmutable;
use common\models\Categories;
use common\models\Payments;
use common\models\Visits;
use common\models\VisitsProducts;
use Yii;
use yii\base\Action;
use yii\db\Transaction;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class PaymentsSaveAction extends Action
{
    /**
     * @var VisitsProducts
     */
    private $visit;

    /**
     * Creates a new Payments model.
     * @return array
     */
    public function run()
    {
        $id = Yii::$app->request->post('visit_id');

        $this->initializeVisit($id);

        if ($this->visit === null) {
            Yii::$app->response->setStatusCode(404);

            return [
                'error' => 'Визит не найден',
            ];
        }

        if (Yii::$app->user->accessChecker->checkAccessToVisit($this->visit->visit) === false) {
            throw new ForbiddenHttpException('Не ваш визит');
        }

        $result = $this->savePayment();

        Yii::$app->response->format = Response::FORMAT_JSON;

        return $result;
    }

    private function initializeVisit($id)
    {
        $this->visit = VisitsProducts::find()
            ->joinWith([
                'visit',
            ])
            ->where([
            'visits_products.id' => $id,
            'visits.deleted_at' => null,
        ])->one();
    }

    private function savePayment()
    {
        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);
        $name = Yii::$app->request->post('name');
        $accountId = Yii::$app->request->post('account_id');
        $amount =  Yii::$app->request->post('amount');
        $category = Categories::findOne(['id' => Yii::$app->request->post('category_id')]);
        $debtId = Yii::$app->request->post('debt_id');
        $debt = Debts::findOne(['id' => $debtId]);

        if ($debt === null) {
            $transaction->rollBack();

            throw new BadRequestHttpException('debt not found');
        }

        $debt->paid_by = Yii::$app->user->getId();
        $debt->amount = $debt->amount ?? $amount;
        $debt->paid_amount = $debt->paid_amount + $amount;
        $debt->paid_at = CarbonImmutable::now()->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT);

        if ($debt->paid_amount >= $debt->amount) {
            $debt->paid_amount = $debt->amount;
            $debt->visit->status = Visits::STATUS_PAID;
            $debt->visit->fillStatusDates();
            $debt->visitProduct->status = Visits::STATUS_PAID;
            $debt->visitProduct->save();
            $debt->visit->save();
        }

        if ($debt->save() === false) {
            Yii::error($debt->getErrors(), Debts::class);

            $transaction->rollBack();
            Yii::$app->response->setStatusCode(400);

            return [
                'error' => 'Ошибка сохранения в базу',
            ];
        }

        $payment = new Payments();
        $payment->visit_id = $this->visit->visit_id;
        $payment->category_id = $debt->category_id;
        $payment->type_id = $debt->type_id;
        $payment->location_id = $debt->location_id;
        $payment->account_id = $accountId;
        $payment->amount = $amount;
        $payment->name = $name;
        $payment->status = Payments::STATUS_PAID;

        if ($payment->validate() === false) {
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(400);

            return $payment->getErrors();
        }

        if ($payment->save(false) === false) {
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(400);

            return [
                'error' => 'Ошибка сохранения в базу',
            ];
        }

        $debt->link('payments', $payment);
        $transaction->commit();

        return [
            'id' => $debt->id,
            'name' => $debt->name,
            'status' => 'Оплачен',
            'amount' => $debt->amount,
            'paid_amount' => $debt->paid_amount,
            'created_at' => $debt->created_at,
            'type' => [
                'id' => $debt->type_id,
                'name' => $debt->type->name ?? null,
            ],
            'category' => [
                'id' => $debt->category_id,
                'name' => $debt->category->name ?? null,
            ],
        ];
     }
}
