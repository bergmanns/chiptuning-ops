<?php

use admin\assets\PaymentFormAsset;
use Carbon\CarbonImmutable;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CashFlow */


PaymentFormAsset::register($this);

$this->title = 'Изменить Cash Flow: ' . sprintf('%s, %s', $model->name, CarbonImmutable::parse($model->created_at)->format('d.m.Y'));;
$this->params['breadcrumbs'][] = ['label' => 'Cash Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="cash-flow-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
