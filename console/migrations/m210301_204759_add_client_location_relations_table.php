<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m210301_204759_add_client_location_relations_table.
 */
class m210301_204759_add_client_location_relations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%clients_locations_relation}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'location_id' => $this->integer(),
            'created_at' => $this->timestamp()->defaultValue(New Expression('NOW()')),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'clients_locations_relation_client_id_key',
            'clients_locations_relation',
            'client_id',
            'clients',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'clients_locations_relation_location_id_key',
            'clients_locations_relation',
            'location_id',
            'locations',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'clients_locations_relation_client_id_location_id_uindex',
            'clients_locations_relation',
            ['location_id', 'client_id'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%clients_locations_relation}}');
    }

}
