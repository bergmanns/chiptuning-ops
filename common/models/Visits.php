<?php

namespace common\models;

use Carbon\CarbonImmutable;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "visits".
 *
 * @property int $id
 * @property int $car_id
 * @property int $client_id
 * @property int $contact_id
 * @property int $location_id
 * @property string $external_id
 * @property float $total_amount
 * @property float $real_amount
 * @property bool $is_landing_paid
 * @property bool $is_amount_confirmed
 * @property int|null $current_mileage
 * @property string|null $status
 * @property string|null $source
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property string $paid_at
 * @property string $completed_at
 * @property string $client_order_id
 *
 * @property Chips[] $chips
 * @property Payments[] $payments
 * @property VisitsProducts[] $products
 * @property Cars $car
 * @property Clients $client
 * @property Locations $location
 * @property User $contact

 */
class Visits extends ActiveRecord
{
    const STATUS_NEW = 'new';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_ACTIVE = 'active';
    const STATUS_DONE = 'done';
    const STATUS_PAID = 'paid';

    public function fillStatusDates(): void
    {
        switch ($this->status) {
            case self::STATUS_PAID:
                $this->paid_at = CarbonImmutable::now();
                break;
            case self::STATUS_DONE:
                $this->completed_at = CarbonImmutable::now();
                break;
            case self::STATUS_CANCELLED:
                $this->deleted_at = CarbonImmutable::now();
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visits';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'location_id'], 'required'],
            [['car_id', 'client_id', 'contact_id', 'location_id', 'current_mileage'], 'integer'],
            [['status'], 'string'],
            [['total_amount', 'real_amount'], 'number'],
            [['is_landing_paid', 'is_amount_confirmed'], 'boolean'],
            [['status'], 'default', 'value' => self::STATUS_NEW],
            [['source'], 'string', 'max' => 255],
            [['client_order_id'], 'string', 'max' => 10],
            ['client_order_id', 'unique'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cars::class, 'targetAttribute' => ['car_id' => 'id']],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::class, 'targetAttribute' => ['client_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::class, 'targetAttribute' => ['location_id' => 'id']],
            [['contact_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['contact_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'blameable' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'contact_id',
                'updatedByAttribute' => null,
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'car_id' => Yii::t('app', 'Авто клиента'),
            'client_id' => Yii::t('app', 'Клиент'),
            'contact_id' => Yii::t('app', 'Менеджер'),
            'location_id' => Yii::t('app', 'Офис'),
            'current_mileage' => Yii::t('app', 'Текущий пробег, км'),
            'status' => Yii::t('app', 'Статус'),
            'source' => Yii::t('app', 'Окуда узнал о нас'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getChips()
    {
        return $this->hasMany(Chips::class, ['visit_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::class, ['visit_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(VisitsProducts::class, ['visit_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Cars::class, ['id' => 'car_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id']);
    }

    public function getDebts()
    {
        return $this->hasMany(Debts::class, ['visit_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(User::class, ['id' => 'contact_id']);
    }

    public function genClientOrder()
    {
        if (!$this->client_order_id) {
            $transaction = Yii::$app->db->beginTransaction();
            $orderCreatedAt = strtotime($this->created_at); // время визита
            try {
                $prefix = date('dmy', $orderCreatedAt) . '/';
                $countOrders = self::find()
                    ->andWhere(['DATE(created_at)' => date('Y-m-d', $orderCreatedAt)]) // оптимизация запроса
                    ->andWhere(['like', 'client_order_id', $prefix])
                    ->count();
                $this->client_order_id = $prefix . ++$countOrders;
                $this->save();
                $transaction->commit();
            } catch (Exception $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return $this->client_order_id;
    }
}
