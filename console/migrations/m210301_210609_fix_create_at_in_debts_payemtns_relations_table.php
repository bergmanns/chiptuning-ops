<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m210301_210609_fix_create_at_in_debts_payemtns_relations_table
 */
class m210301_210609_fix_create_at_in_debts_payemtns_relations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('debts_payments_relation', 'created_at', $this->timestamp()->defaultValue(New Expression('NOW()')));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('debts_payments_relation', 'created_at', $this->dateTime());
    }
}
