<?php

use yii\db\Migration;

/**
 * Class m210418_090739_add_visit_status_at_columns
 */
class m210418_090739_add_visit_status_at_columns extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('visits', 'paid_at', $this->dateTime()->after('created_at'));
        $this->addColumn('visits', 'completed_at', $this->dateTime()->after('paid_at'));

        $this->renameColumn('visits_products', 'payed_at', 'paid_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('visits', 'paid_at');
        $this->dropColumn('visits', 'completed_at');

        $this->renameColumn('visits_products', 'paid_at', 'payed_at');
    }
}
