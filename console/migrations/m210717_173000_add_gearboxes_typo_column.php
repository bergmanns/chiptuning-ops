<?php

use yii\db\Migration;

/**
 * Class m210717_173000_add_gearboxes_typo_column.
 */
class m210717_173000_add_gearboxes_typo_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_gearboxes', 'type', $this->string()->after('name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_gearboxes', 'type');
    }
}
