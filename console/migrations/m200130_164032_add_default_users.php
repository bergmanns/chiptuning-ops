<?php

use Carbon\CarbonImmutable;
use common\models\Countries;
use common\models\Locations;
use common\models\Regions;
use common\models\User;
use yii\db\Migration;

/**
 * Class m200130_164032_add_default_users
 */
class m200130_164032_add_default_users extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function up()
    {
        $this->insert('countries', [
            'name' => 'РФ',
            'created_at' => CarbonImmutable::now(),
        ]);

        $this->insert('regions', [
            'name' => 'Москва',
            'code' => 'MSK',
            'country_id' => Countries::findOne(['name' => 'РФ'])->id,
            'created_at' => CarbonImmutable::now(),
        ]);

        $this->insert('locations', [
            'name' => 'Москва №1',
            'hq_share' => 1,
            'region_id' => Regions::findOne(['code' => 'MSK'])->id,
            'created_at' => CarbonImmutable::now(),
        ]);

        $this->insert('staff', [
            'phone' => '+79859999999',
            'location_id' => Locations::findOne(['name' => 'Москва №1'])->id,
            'username' => 'Admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'role' => User::ROLE_ADMIN,
            'created_at' => CarbonImmutable::now(),
            'updated_at' => CarbonImmutable::now(),
        ]);

        $this->insert('staff', [
            'phone' => '+79859999988',
            'location_id' => Locations::findOne(['name' => 'Москва №1'])->id,
            'username' => 'Manager',
            'password_hash' => Yii::$app->security->generatePasswordHash('manager'),
            'role' => User::ROLE_MANAGER,
            'created_at' => CarbonImmutable::now(),
            'updated_at' => CarbonImmutable::now(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('staff', ['phone' => '+79859999999']);
        $this->delete('staff', ['phone' => '+79859999988']);
    }
}
