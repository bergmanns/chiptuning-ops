<?php

declare(strict_types = 1);

namespace Deployer;

require 'recipe/yii2-app-advanced.php';

// Project repository
set('repository', 'git@gitlab.com:evilspirit171_side_projects/internal_admin_panel.git');

set('branch', 'master');

set('keep_releases', 2);

set('default_stage', 'production');

set('http_user', 'www-data');

set('http_group', 'www-data');

set('writable_use_sudo', false);

set('allow_anonymous_stats', false);

set('writable_mode', 'chmod');

set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --profile');

// Shared files/dirs between deploys
set('shared_files', []);
set('shared_dirs', [
    'admin/runtime',
    'console/runtime',
]);

// Writable dirs by web server
add('writable_dirs', [
    'admin/runtime',
    'admin/web/assets',
    'console/runtime',
]);

set('allow_anonymous_stats', false);

// Hosts

host('206.189.49.11')
    ->stage('production')
    ->user('root')
    ->identityFile('~/.ssh/eurocode_prod')
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->set('deploy_path', '/var/www')
;

// Deploy tasks

desc('Deploy application');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:vendors',
    'deploy:init',
    'deploy:initialize_configs',
    'deploy:shared',
    'deploy:writable',
    'deploy:chown',
    'deploy:run_migrations',
    'deploy:symlink',
    'deploy:reload_php_fpm',
    'deploy:reload_nginx',
    'deploy:flush_cache',
    'deploy:flush_db_cache',
    'deploy:unlock',
    'deploy:release_log',
    'cleanup',
]);

desc('Log release version');
task('deploy:release_log', function () {
    run('echo {{release_name}}');
});

desc('Initialize application');
task('deploy:init', function () {
    run('{{bin/php}} {{release_path}}/init --env=Production --overwrite=y');
});

desc('Initialize configs');
task('deploy:initialize_configs', function () {
    $configs = [
        '{{release_path}}/common/config/main-local.php',
        '{{release_path}}/common/config/params-local.php',
        '{{release_path}}/console/config/main-local.php',
        '{{release_path}}/console/config/params-local.php',
        '{{release_path}}/admin/config/main-local.php',
        '{{release_path}}/admin/config/params-local.php',
    ];

    foreach ($configs as $config) {
        run(sprintf('envsubst < %s > /tmp/config-local', $config));
        run(sprintf('cp /tmp/config-local %s', $config));
    }
});

desc('Reload PHP-FPM service');
task('deploy:reload_php_fpm', function () {
    run('service php7.4-fpm restart');
});

desc('Reload Nginx service');
task('deploy:reload_nginx', function () {
    run('service nginx restart -HUP');
});

desc('Flush cache');
task('deploy:flush_cache', function () {
    run('{{bin/php}} {{release_path}}/yii cache/flush-all --interactive=0');
});

desc('Flush db cache');
task('deploy:flush_db_cache', function () {
    run('{{bin/php}} {{release_path}}/yii cache/flush-schema --interactive=0');
});

desc('Set own');
task('deploy:chown', function () {
    run('chown -hR www-data:www-data {{deploy_path}}');
});

desc('Log release version');
task('deploy:release_log', function () {
    //todo Connect release name with sentry release
    run('echo {{release_name}}');
});

// When deploy fails automatically unlock
after('deploy:failed', 'deploy:unlock');
