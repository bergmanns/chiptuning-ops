$( document ).ready(function() {
    function initVinMask() {
        let maskOpts = {
            definitions: {
                "a": {
                    validator: "[0-9AaBbCcDdEeFfGgHhJjKkLlMmNnPpRrSsTtUuVvWwXxYyZz]",
                    casing: "upper"
                },
            },
        };

        $("#cars-vin_number").inputmask('a{17}', maskOpts);
    }

    initVinMask();

    let gearbox = $(".car-gearbox-selector");
    let model = $('.car-model-variants-selector');
    let make = $(".car-make-selector");
    let chassis = $('.car-chassis-selector');
    let engine = $('.car-engine-selector');
    let engineModel = $('.car-engine-model-selector');
    let makeSelected = make.val();
    let modelSelected = model.val();
    let chassisSelected = chassis.val();
    let engineSelected = engine.val();
    let engineModelSelected = engineModel.val();
    let gearboxSelected = null;

    gearbox.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/gearboxes/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    make_id: makeSelected,
                    model_id: modelSelected,
                    chassis_id: chassisSelected,
                    name: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите коробку',
        minimumInputLength: 0,
    })

    model.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/car-model-variant/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                console.log(params);
                return {
                    model: params.term,
                    make_id: makeSelected,
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите модель',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        modelSelected = e.params.data.id;
        engine.val(null).trigger('change');
        engineModel.val(null).trigger('change');
        chassis.val(null).trigger('change');
    });

    chassis.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/chassis/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                console.log(params);
                return {
                    model: params.term,
                    model_id: modelSelected,
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите кузов',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        chassisSelected = e.params.data.id;
        engine.val(null).trigger('change');
    });

    engine.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/engine/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    model: params.term,
                    chassis_id: chassisSelected,
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите двигатель',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        engineSelected = e.params.data.id;
    });

    engineModel.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/car/get-engine-models",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    model: params.term,
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите модель двигателя',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        engineModelSelected = e.params.data.id;
    });

    make.select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/car-make-variant/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    make: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите марку',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        makeSelected = e.params.data.id;
        model.val(null).trigger('change');
        chassis.val(null).trigger('change');
        engine.val(null).trigger('change');
        engineModel.val(null).trigger('change');
    });
});
