<?php

use common\models\CarGearboxes;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CarGearboxes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = ['label' => 'Коробки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="car-gearboxes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'make_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    if ($model->make !== null) {
                        $html = Html::a($model->make->name, ['car-make-variant/view', 'id' => $model->make_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'model_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    if ($model->model !== null) {
                        $html = Html::a($model->model->name, ['car-model-variant/view', 'id' => $model->model_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    if ($model->chassis !== null) {
                        $html = Html::a($model->chassis->name, ['chassis/view', 'id' => $model->chassis_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'engine_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarGearboxes */
                    if ($model->engine !== null) {
                        $html = Html::a($model->engine->fullName, ['engine/view', 'id' => $model->engine_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'type',
            'name',
            'created_at:datetime',
            'updated_at:datetime',
            'deleted_at:datetime',
        ],
    ]) ?>

</div>
