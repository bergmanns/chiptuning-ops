<?php

use yii\db\Migration;

/**
 * Class m200212_105204_add_engine_volume_to_car_models_table
 */
class m200212_105204_add_engine_volume_to_car_models_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_models', 'engine_volume', $this->float()->after('engine_name'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_models', 'engine_volume');
    }
}
