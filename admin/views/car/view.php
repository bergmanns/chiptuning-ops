<?php

use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cars */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->gos_number;
YiiAsset::register($this);
?>
<div class="cars-view">

    <h1><?=Html::encode($this->title)?></h1>

    <p>
        <?=Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?=Html::a('Удалить', ['delete', 'id' => $model->id], [
    'class' => 'btn btn-danger',
    'data' => [
        'confirm' => 'Are you sure you want to delete this item?',
        'method' => 'post',
    ],
])?>
    </p>

    <?=DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'model_make',
            'label' => 'Марка',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->chassis !== null) {
                    $html = Html::a($model->chassis->make, ['car-make-variant/view', 'id' => $model->chassis->make_id]);
                    return $html;
                }

                return null;
            },
        ],

        [
            'attribute' => 'model_model',
            'label' => 'Модель',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->chassis !== null) {
                    $modelName = $model->chassis->model->name ?? null;
                    $html = Html::a($modelName, ['car-model-variant/view', 'id' => $model->chassis->model_id]);
                    return $html;
                }

                return null;
            },
        ],
        [
            'attribute' => 'chassis_id',
            'label' => 'Кузов',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->chassis !== null) {
                    $modelName = $model->chassis->name ?? null;
                    $html = Html::a($modelName, ['chassis/view', 'id' => $model->engine->chassis_id]);
                    return $html;
                }

                return null;
            },
        ],
        [
            'attribute' => 'engine_id',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->engine !== null) {
                    $modelName = $model->engine->fullName ?? null;
                    $html = Html::a($modelName, ['engine/view', 'id' => $model->engine_id]);
                    return $html;
                }

                return null;
            },
        ],
        'engine_model',
        [
            'attribute' => 'gearbox_id',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->gearbox_id !== null) {
                    $modelName = $model->gearbox->name ?? null;
                    $html = Html::a($modelName, ['gearboxes/view', 'id' => $model->gearbox_id]);
                    return $html;
                }

                return null;
            },
        ],
        [
            'attribute' => 'client_id',
            'format' => 'HTML',
            'value' => function ($model) {
                /* @var $model Cars */
                if ($model->client !== null) {
                    $html = Html::a($model->client->full_name, ['client/view', 'id' => $model->client_id]);
                    return $html;
                }

                return null;
            },
        ],
        'chip_id',
        'issue_year',
        'vin_number',
        'gos_number',
        'eng_chip_status',
        'gear_chip_status',
        'files_link',
        'created_at',
        'updated_at',
        'deleted_at',
    ],
])?>

</div>
