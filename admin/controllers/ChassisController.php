<?php

namespace admin\controllers;

use Yii;
use common\models\CarChassis;
use admin\models\CarChassisSearch;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CarModelController implements the CRUD actions for CarModels model.
 */
class ChassisController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all CarModels models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarChassisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionGet($model_id, $model = null)
    {
        $makeQuery = CarChassis::find()
            ->select([
                'id',
                'name as text'
            ])
            ->where([
                'model_id' => $model_id,
                'deleted_at' => null,
            ]);

        if ($model !== null) {
            $makeQuery->andWhere(['like', 'name', $model]);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        $make = $makeQuery->asArray()->all();

        return [
            'items' => $make,
        ];
    }

    /**
     * Displays a single CarModels model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarModels model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CarChassis();

        if (Yii::$app->request->isPost === true) {
            $isLoad = $model->load(Yii::$app->request->post());
            $model->make_id = $model->model->make_id ?? null;
            $isValid = $model->validate();

            if ($isLoad === true && $isValid === true) {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CarModels model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost === true) {
            $isLoad = $model->load(Yii::$app->request->post());
            $model->make_id = $model->model->make_id ?? null;
            $isValid = $model->validate();

            if ($isLoad === true && $isValid === true) {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CarModels model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CarModels model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarChassis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarChassis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
