<?php

use admin\widgets\DropDownList;
use common\models\Categories;
use common\models\CategoryTypes;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CashFlowSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cash Flow';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-flow-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <div class="row form-inline">
            <div class="col-md-6 form-inline">
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'month' ? 'active' : ''?>" href="?CashFlowSearch[created_at]=<?=$searchModel->filterMonthStart . ' - ' . $searchModel->filterMonthEnd?>&CashFlowSearch[createdAtPreset]=month">Месяц</a>
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'week' ? 'active' : ''?>" href="?CashFlowSearch[created_at]=<?=$searchModel->filterWeekStart . ' - ' . $searchModel->filterWeekStart?>&CashFlowSearch[createdAtPreset]=week">Неделя</a>
                <a class="btn btn-default <?= $searchModel->createdAtPreset === 'day' ? 'active' : ''?>" href="?CashFlowSearch[created_at]=<?=$searchModel->filterDayStart . ' - ' . $searchModel->filterDayEnd?>&CashFlowSearch[createdAtPreset]=day">День</a>
            </div>
            <div class="col-md-6 form-inline">
                <div class="form-inline form-group pull-right">
                    <span>Всего стоимость: <?=$searchModel->total_amount?> ₽</span>
                </div>
            </div>
        </div>
        </p>
    <p>
        <?= Html::a('Новая запись', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'amount',
            [
                'attribute' => 'category_id',
                'format' => 'HTML',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'category_id',
                    'data' => ArrayHelper::map(Categories::find()->where(['active' => 1])->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model \common\models\CashFlow */
                    if ($model->category !== null) {
                        $html = Html::a($model->category->name, ['categories/view', 'id' => $model->category_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'type_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'type_id',
                    'data' => ArrayHelper::map(CategoryTypes::find()->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model \common\models\CashFlow */
                    return $model->type->name ?? null;
                },
            ],
            [
                'attribute' => 'location_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'location_id',
                    'data' => ArrayHelper::map(\common\models\Locations::find()->where(['deleted_at' => null])->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model \common\models\CashFlow */
                    return $model->location->name ?? null;
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d H:i',
                        ],
                    ],
                ]),
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}'
            ],
        ],
    ]); ?>


</div>
