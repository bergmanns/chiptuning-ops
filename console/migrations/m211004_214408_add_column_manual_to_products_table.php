<?php

use yii\db\Migration;

/**
 * Class m211004_214408_add_column_manual_to_products_table.
 */
class m211004_214408_add_column_manual_to_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'is_manual', $this->boolean()->after('status'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'is_manual');
    }
}
