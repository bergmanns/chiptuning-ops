<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "car_model_variants".
 *
 * @property int $id
 * @property int|null $make_id
 * @property string|null $name
 * @property string|null $created_at
 * @property string|null $deleted_at
 * @property int|null $active
 *
 * @property CarMakeVariants $make
 * @property CarChassis[] $carModels
 */
class CarModelVariants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_model_variants';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'softdeleteable' => [
                'class' => 'common\behaviors\SoftDeleteableBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['make_id', 'active'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['make_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarMakeVariants::className(), 'targetAttribute' => ['make_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'make_id' => 'Марка',
            'name' => 'Название',
            'created_at' => 'Добавлена',
            'deleted_at' => 'Удалена',
            'active' => 'Активна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMake()
    {
        return $this->hasOne(CarMakeVariants::className(), ['id' => 'make_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarModels()
    {
        return $this->hasMany(CarChassis::className(), ['id' => 'id']);
    }

    public function __toString(): string
    {
        return (string) $this->name;
    }
}
