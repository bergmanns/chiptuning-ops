<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=${DB_HOST};port=${DB_PORT};dbname=${DB_NAME}',
            'username' => '${DB_USER}',
            'password' => '${DB_PASSWORD}',
            'charset' => 'utf8',
        ],
        'landing' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=${DB_LANDING_HOST};port=${DB_LANDING_PORT};dbname=${DB_LANDING_NAME}',
            'username' => '${DB_LANDING_USER}',
            'password' => '${DB_LANDING_PASSWORD}',
            'charset' => 'utf8',
        ],
        'telegramTransport' => [
            'botToken' => '${TELEGRAM_BOT_TOKEN}',
        ],
        'log' => [
            'targets' => [
                'telegram' => [
                    'channel' => '${TELEGRAM_ERROR_CHAT_ID}',
                ],
                'sentry' => [
                    'dsn' => '${COMMON_SENTRY_DSN_BACKEND}',
                ],
            ],
        ],
    ],
];
