<?php

use yii\db\Migration;

/**
 * Class m200830_224021_remove_excess_fields_in_payments_table
 */
class m200830_224021_remove_excess_fields_in_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('payments_account_key', 'payments');
        $this->dropForeignKey('payments_categories_key', 'payments');
        $this->dropForeignKey('payments_location_key', 'payments');
        $this->dropForeignKey('payments_visit_product_key', 'payments');
        $this->dropColumn('payments', 'visit_product_id');
        $this->dropColumn('payments', 'type');
        $this->dropColumn('payments', 'category_id');
        $this->dropColumn('payments', 'location_id');
        $this->dropColumn('payments', 'hq_share');

        $this->addColumn('payments', 'visit_id', $this->integer()->after('cash_flow_id'));

        $this->addForeignKey(
            'payments_visit_id_key',
            'payments',
            'visit_id',
            'visits',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200830_224021_remove_excess_fields_in_payments_table cannot be reverted.\n";

        return false;
    }
}
