<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class InputMaskAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/inputmask/dist';
    public $js = [
        'jquery.inputmask.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
