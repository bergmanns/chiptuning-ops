/**
 * Override the default yii confirm dialog. This function is
 * called by yii when a confirmation is requested.
 *
 * @param message the message to display
 * @param okCallback triggered when confirmation is true
 * @param cancelCallback callback triggered when canceled
 */
yii.confirm = function (message, okCallback, cancelCallback) {
    swal({
        title: message,
        // text: message,
        icon: "warning",
        buttons: ["Нет", "Да, удалить"],
        dangerMode: true,
    }).then(
        (result) => { 
            if (result) { // <-- if confirmed
                okCallback()
            } else {
                cancelCallback()
            }
        }
    );
};