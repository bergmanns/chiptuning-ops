<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Chips */

$this->title = 'Create Chips';
$this->params['breadcrumbs'][] = ['label' => 'Chips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chips-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
