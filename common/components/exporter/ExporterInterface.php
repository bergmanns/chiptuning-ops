<?php

namespace common\components\exporter;

use yii\data\ActiveDataProvider;

interface ExporterInterface
{
    public function formatProvider(ActiveDataProvider $provider, array $headers);
}