<?php

use yii\db\Migration;

/**
 * Class m201108_145458_remove_cash_flow_table
 */
class m201108_145458_remove_cash_flow_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('cash_flow_visit_id_key', 'cash_flow');
        $this->dropForeignKey('cash_flow_type_id_key', 'cash_flow');
        $this->dropForeignKey('cash_flow_location_id_key', 'cash_flow');
        $this->dropForeignKey('cash_flow_category_id_key', 'cash_flow');
        $this->dropForeignKey('debts_cash_flow_key', 'debts');
        $this->dropForeignKey('payments_cash_flow_id_key', 'payments');

        $this->dropTable('cash_flow');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201108_145458_remove_cash_flow_table cannot be reverted.\n";

        return false;
    }
}
