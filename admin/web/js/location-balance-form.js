// start app
new Vue({
    el: '#location-balance',
    data: {
        locations: [
        ],
        currentLocation: {
            id: null,
            name: null,
            balance: null,
        },
        selectedLocationId: null,
        newLocationBalance: null,
        showModal: false
    },
    created: function () {
        this.initLocations();
    },
    methods: {
        initLocations: function() {
            let self = this;
            let url = '/locations/api/index';

            this.$http.get(url).then(function (response) {
                self.locations = response.data;

                if (self.locations.length > 0) {
                    self.currentLocation = response.data[0];
                }
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
        updateLocationBalance: function() {
            let self = this;
            let url = '/locations/api/balance';
            let param = $('meta[name=csrf-param]').attr("content");
            let token = $('meta[name=csrf-token]').attr("content");
            let body = {
                id: self.currentLocation.id,
                balance: self.currentLocation.balance,
                newBalance: self.newLocationBalance,
            };

            body[param] = token;
            this.$http.post(url, body, {
                emulateJSON: true
            }).then(function (response) {
                window.location.reload();
            }, function (response) {
                swal({
                    title: "Что-то пошло не так!",
                    text: response.data,
                    icon: "error",
                });
            });
        },
    }
})