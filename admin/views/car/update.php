<?php

use admin\assets\CarAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cars */

CarAsset::register($this);

$this->title = 'Редактировать: ' . vsprintf('%s %s', [
        $model->engine->fullName,
        $model->gos_number,
    ]);
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->gos_number, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cars-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
