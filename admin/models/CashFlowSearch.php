<?php

namespace admin\models;

use Carbon\CarbonImmutable;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CashFlow;
use yii\db\ActiveQuery;

/**
 * CashFlowSearch represents the model behind the search form of `common\models\CashFlow`.
 *
 *  @property string $filterMonthStart
 *  @property string $filterMonthEnd
 *  @property string $filterDayStart
 *  @property string $filterDayEnd
 *  @property string $filterWeekStart
 *  @property string $filterWeekEnd
 */
class CashFlowSearch extends CashFlow
{
    public $createdAtPreset;

    public $total_amount;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'visit_id', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['created_at'], 'safe'],
            [['amount', 'share'], 'number'],
            [['createdAtPreset'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function getFilterMonthStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterMonthEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterWeekStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfWeek()->format('Y-m-d H:i');
    }

    public function getFilterWeekEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfWeek()->startOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterDayStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfDay()->format('Y-m-d H:i');
    }

    public function getFilterDayEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfDay()->startOfMonth()->format('Y-m-d H:i');
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CashFlow::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->initializeDateFilter($query);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'visit_id' => $this->visit_id,
            'type_id' => $this->type_id,
            'category_id' => $this->category_id,
            'location_id' => $this->location_id,
            'amount' => $this->amount,
            'share' => $this->share,
        ]);

        $this->initializeTotalAmount($query);

        return $dataProvider;
    }

    private function initializeDateFilter(ActiveQuery $query): ActiveQuery
    {
        if (!empty($this->created_at)) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);

            $query->andWhere([
                'between',
                'created_at',
                $start_date,
                $end_date,
            ]);
        }

        return $query;
    }

    private function initializeTotalAmount(ActiveQuery $query)
    {
        $this->total_amount = (int) $query->sum('amount');
    }
}
