<?php

use yii\db\Migration;

/**
 * Class m210707_130739_add_client_order_id_column_to_visit_table
 */
class m210707_130739_add_client_order_id_column_to_visit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('visits', 'client_order_id', $this->string(10)->null()->unique());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('visits', 'client_order_id');
    }
}
