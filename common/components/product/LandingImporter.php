<?php

namespace common\components\product;

use common\models\CarChassis;
use common\models\CarEngines;
use common\models\CarGearboxes;
use common\models\CarMakeVariants;
use common\models\CarModelVariants;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Products;
use Yii;
use yii\base\Component;
use yii\db\Connection;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class LandingImporter extends Component
{
    const DEFAULT_TYPE_NAME = 'Визит';
    const DEFAULT_CATEGORY_NAME = 'Чип';

    /**
     * @var Connection
     */
    private $landingDb;

    public function import()
    {
        /** @var Connection $landingDb */
        $this->landingDb = Yii::$app->landing;
        $productsQuery = $this->initializeLandingProductsQuery();
        $batches = $productsQuery->batch(100, $this->landingDb);
        $category = $this->getDefaultCategory();

        $this->archiveProducts();

        foreach ($batches as $batch) {
            foreach ($batch as $chip) {
                $transaction = Yii::$app->db->beginTransaction();

                try {
                    $externalId = ArrayHelper::getValue($chip, 'id');
                    $makeName = ArrayHelper::getValue($chip, 'Make');
                    $modelName = ArrayHelper::getValue($chip, 'Model');
                    $chassisName = ArrayHelper::getValue($chip, 'Chassis');
                    $engineName = ArrayHelper::getValue($chip, 'Engine');
                    $gearboxType = ArrayHelper::getValue($chip, 'gearbox_type');
                    $gearboxStatus = ArrayHelper::getValue($chip, 'gearbox1') !== '0';
                    $stockHP = ArrayHelper::getValue($chip, 'Stock_hp');
                    $stockNM = ArrayHelper::getValue($chip, 'Stock_nm');

                    echo "Run $externalId $makeName $modelName $chassisName $engineName $stockHP $stockNM" . PHP_EOL;

                    $make = $this->getMake($makeName);
                    $model = $this->getModel($modelName, $make);
                    $chassis = $this->getChassis($chassisName, $make, $model);

                    if ($gearboxStatus === true) {
                        $gearbox = $this->getGearbox(
                            $engineName,
                            $gearboxType,
                            $make,
                            $model,
                            $chassis
                        );

                        $stage1 = $this->getGearboxProductStage($chip, $make, $model, $chassis, $gearbox, 1);
                        $stage2 = $this->getGearboxProductStage($chip, $make, $model, $chassis, $gearbox, 2);
                    } else {
                        $engine = $this->getEngine(
                            $engineName,
                            $stockHP,
                            $stockNM,
                            $chassis
                        );

                        $stage1 = $this->getProductStage($chip, $make, $model, $chassis, $engine, 1);
                        $stage2 = $this->getProductStage($chip, $make, $model, $chassis, $engine, 2);
                    }

                    $stage1->category_id = $category->id;
                    $stage2->category_id = $category->id;

                    $isStage1New = $stage1->isNewRecord;
                    $isStage2New = $stage2->isNewRecord;

                    if ($stage1->save() === false) {
                        echo "$stage1->name create failed" . PHP_EOL;

                        Yii::error($stage1->getErrors());
                    }

                    if ($isStage1New === true) {
                        echo "$stage1->name create" . PHP_EOL;
                    } else {
                        echo "$stage1->name update" . PHP_EOL;
                    }

                    if ($stage2->save() === false) {
                        echo "$stage2->name create failed" . PHP_EOL;

                        Yii::error($stage2->getErrors());
                    }

                    if ($isStage2New === true) {
                        echo "$stage2->name create" . PHP_EOL;
                    } else {
                        echo "$stage2->name update" . PHP_EOL;
                    }

                    $transaction->commit();
                } catch (\Throwable $e) {
                    Yii::error($e);

                    $transaction->rollBack();
                }
            }
        }
    }

    private function initializeLandingProductsQuery(): Query
    {
        $query = (new Query())
            ->select([
                'id',
                'Make',
                'Model',
                'Chassis',
                'Engine',
                'Text',
                'Stock_hp',
                'Stock_nm',
                'St1_hp',
                'St1_nm',
                'St2_hp',
                'St2_nm',
                'St1_price',
                'St2_price',
                'gearbox1',
                'gearbox2',
                'gearbox3',
                'gearbox4',
                'gearbox_model',
                'gearbox_type',
            ])
            ->from('chip')
        ;

        return $query;
    }

    private function getMake($makeName): CarMakeVariants
    {
        $make = CarMakeVariants::find()->where([
            'LOWER(name)' => mb_strtolower($makeName),
        ])->one();

        if ($make === null) {
            echo "Make $makeName not found, create" . PHP_EOL;

            $make = new CarMakeVariants();
            $make->name = $makeName;
            $make->active = 1;
        }

        $make->deleted_at = null;
        $make->save();

        return $make;
    }

    private function getModel($modelName, CarMakeVariants $make)
    {
        $model = CarModelVariants::find()->where([
            'LOWER(name)' => mb_strtolower($modelName),
            'make_id' => $make->id,
        ])->one();

        if ($model === null) {
            echo "Model $modelName not found, create" . PHP_EOL;

            $model = new CarModelVariants();
            $model->name = $modelName;
            $model->make_id = $make->id;
            $model->active = 1;
        }

        $model->deleted_at = null;
        $model->save();

        return $model;
    }

    private function getChassis($name, CarMakeVariants $make, CarModelVariants $model)
    {
        $row = CarChassis::find()->where([
            'LOWER(name)' => mb_strtolower($name),
            'make_id' => $make->id,
            'model_id' => $model->id,
        ])->one();

        if ($row === null) {
            echo "Chassis $name not found, create" . PHP_EOL;

            $row = new CarChassis();
            $row->name = $name;
            $row->make_id = $make->id;
            $row->model_id = $model->id;
        }

        $row->deleted_at = null;
        $row->save();

        return $row;
    }

    private function getEngine($name, $hp, $nm, CarChassis $chassis)
    {
        $row = CarEngines::find()->where([
            'LOWER(name)' => mb_strtolower($name),
            'hp' => $hp,
            'nm' => $nm,
            'chassis_id' => $chassis->id
        ])->one();

        if ($row === null) {
            echo "Engine $name $hp $nm not found, create" . PHP_EOL;

            $row = new CarEngines();
            $row->name = $name;
            $row->hp = $hp;
            $row->nm = $nm;
            $row->chassis_id = $chassis->id;

        }

        $row->deleted_at = null;
        $row->save();

        return $row;
    }

    private function getProductStage(array $chip, CarMakeVariants $make, CarModelVariants $model, CarChassis $chassis, CarEngines $engine, $stage)
    {
        $externalId = ArrayHelper::getValue($chip, 'id');

        $product = Products::find()
            ->where([
                'external_id' => $externalId,
                'make_id' => $make->id,
                'model_id' => $model->id,
                'chassis_id' => $chassis->id,
                'engine_id' => $engine->id,
                'stage' => $stage,
            ])->one()
        ;

        $hp = ArrayHelper::getValue($chip, sprintf('St%d_hp', $stage));
        $nm = ArrayHelper::getValue($chip, sprintf('St%d_nm', $stage));
        $text = ArrayHelper::getValue($chip, 'Text');
        $uri = $this->getUriFromText($text);

        if ($product === null) {
            $product = new Products();
            $product->external_id = $externalId;
            $product->make_id = $make->id;
            $product->model_id = $model->id;
            $product->chassis_id = $chassis->id;
            $product->engine_id = $engine->id;
            $product->stage = $stage;
            $product->is_manual = 0;
        }

        $product->name = sprintf('Stage %d %shp %snm', $stage, $hp, $nm);

        if ($uri !== null) {
            $product->link = sprintf('https://www.eurocode-tuning.ru/%s.html', $uri);
        }

        $priceKey = sprintf('St%d_price', $stage);
        $product->price = (float) ArrayHelper::getValue($chip, $priceKey, 0);
        $product->deleted_at = null;
        $product->status = 'active';

        return $product;
    }

    private function getGearboxProductStage(array $chip, CarMakeVariants $make, CarModelVariants $model, CarChassis $chassis, CarGearboxes $gearbox, $stage)
    {
        $externalId = ArrayHelper::getValue($chip, 'id');

        $product = Products::find()
            ->where([
                'external_id' => $externalId,
                'make_id' => $make->id,
                'model_id' => $model->id,
                'chassis_id' => $chassis->id,
                'gearbox_id' => $gearbox->id,
                'stage' => $stage,
            ])->one()
        ;

        $text = ArrayHelper::getValue($chip, 'Text');
        $uri = $this->getUriFromText($text);

        if ($product === null) {
            $product = new Products();
            $product->external_id = $externalId;
            $product->make_id = $make->id;
            $product->model_id = $model->id;
            $product->chassis_id = $chassis->id;
            $product->gearbox_id = $gearbox->id;
            $product->stage = $stage;
            $product->is_manual = 0;
        }

        $product->name = sprintf('Stage %d %s', $stage, $gearbox->name);

        if ($uri !== null) {
            $product->link = sprintf('https://www.eurocode-tuning.ru/%s.html', $uri);
        }

        $priceKey = sprintf('St%d_price', $stage);
        $product->price = (float) ArrayHelper::getValue($chip, $priceKey, 0);
        $product->deleted_at = null;
        $product->status = 'active';

        return $product;
    }

    private function getUriFromText($text)
    {
        $re = '/.*href="(\/+([\w\-0-9]+).html)"/m';

        preg_match_all($re, $text, $matches);

        return $matches[2][0] ?? null;
    }

    private function getDefaultCategory(): Categories
    {
        $category =  Categories::findOne(['name' => self::DEFAULT_CATEGORY_NAME]);

        if ($category !== null) {
            return $category;
        }

        $type = $this->getDefaultType();

        $category = new Categories();
        $category->name = self::DEFAULT_CATEGORY_NAME;
        $category->type_id = $type->id;
        $category->for_visit = 1;
        $category->active = 1;

        $category->save();

        return $category;
    }

    private function getDefaultType(): CategoryTypes
    {
        $type = CategoryTypes::findOne(['name' => self::DEFAULT_TYPE_NAME]);

        if ($type !== null) {
            return $type;
        }

        $type = new CategoryTypes();
        $type->name = self::DEFAULT_TYPE_NAME;
        $type->active = 1;

        $type->save();

        return $type;
    }

    private function getGearbox($name, $type, CarMakeVariants $make, CarModelVariants $model, CarChassis $chassis)
    {
        $row = CarGearboxes::find()->where([
            'LOWER(name)' => mb_strtolower($name),
            'type' => mb_strtolower($type),
            'make_id' => $make->id,
            'model_id' => $model->id,
            'chassis_id' => $chassis->id
        ])->one();

        if ($row === null) {
            echo "Gearbox $name not found, create" . PHP_EOL;

            $row = new CarGearboxes();
            $row->name = $name;
            $row->type = $type;
            $row->make_id = $make->id;
            $row->model_id = $model->id;
            $row->chassis_id = $chassis->id;

            $row->save();
        }

        return $row;
    }

    private function archiveProducts()
    {
        $archiveQuery = <<<SQL
    UPDATE eurocode.products SET eurocode.products.status = 'obsolete', eurocode.products.deleted_at = NOW() WHERE eurocode.products.status = 'active' and (is_manual = 0 or is_manual is null)
SQL;
        Yii::$app->db->createCommand($archiveQuery)->execute();
    }
}
