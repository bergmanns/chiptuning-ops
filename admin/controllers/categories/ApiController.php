<?php

namespace admin\controllers\categories;

use common\models\Categories;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * VisitController implements the CRUD actions for Visits model.
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
        ];
    }

    /**
     * Return info about all Visits Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $categories = Categories::find()->asArray()->where([
            'active' => 1,
            'for_visit' => 1,
        ])->all();

        return $categories;
    }
}
