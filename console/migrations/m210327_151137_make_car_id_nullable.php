<?php

use yii\db\Migration;

/**
 * Class m210327_151137_make_car_id_nullable
 */
class m210327_151137_make_car_id_nullable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('visits', 'car_id', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('visits', 'car_id', $this->integer()->notNull());
    }
}
