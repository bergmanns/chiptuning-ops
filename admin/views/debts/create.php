<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model admin\models\Debts */

$this->title = 'Добавить долг';
$this->params['breadcrumbs'][] = ['label' => 'Филиалы', 'url' => ['location/index']];
$this->params['breadcrumbs'][] = ['label' => 'Долги', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="debts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
