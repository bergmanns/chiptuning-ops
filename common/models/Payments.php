<?php

namespace common\models;

use Throwable;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Transaction;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int|null $visit_id
 * @property int $type_id
 * @property int $category_id
 * @property int $location_id
 * @property int|null $account_id
 * @property string|null $name
 * @property string|null $status
 * @property float|null $amount
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Chips[] $chips
 * @property Accounts $account
 * @property Categories $category
 * @property CategoryTypes $type
 * @property Locations $location
 * @property Visits $visit
 * @property Debts $debts

 */
class Payments extends ActiveRecord
{
    const STATUS_PAID = 'paid';
    const STATUS_CANCEL = 'cancel';
    const STATUS_NOT_PAID = 'not_paid';
    const STATUS_LOCAL_PAY = 'local_pay';

    /**
     * @var Transaction|null
     */
    private $balanceTransaction;

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'balanceUpdateStart']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateAccountBalance']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateLocationBalance']);
        $this->on(self::EVENT_AFTER_INSERT, [$this, 'balanceUpdateEnd']);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visit_id', 'account_id', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['type_id', 'category_id', 'amount'], 'required'],
            [['status'], 'string'],
            [['amount'], 'number'],
            ['amount', 'compare', 'compareValue' => 0, 'operator' => '!==', 'type' => 'number'],
            [['name'], 'string', 'max' => 255],
            [['status'], 'default', 'value' => self::STATUS_PAID],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::class, 'targetAttribute' => ['account_id' => 'id']],
            [['visit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Visits::class, 'targetAttribute' => ['visit_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryTypes::class, 'targetAttribute' => ['type_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::class, 'targetAttribute' => ['category_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::class, 'targetAttribute' => ['location_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'visit_id' => Yii::t('app', 'Визит'),
            'account_id' => Yii::t('app', 'Счет'),
            'type_id' => Yii::t('app', 'Тип'),
            'category_id' => Yii::t('app', 'Категория'),
            'location_id' => Yii::t('app', 'Филиал'),
            'name' => Yii::t('app', 'Комментарий'),
            'status' => Yii::t('app', 'Статус'),
            'amount' => Yii::t('app', 'Сумма'),
            'created_at' => Yii::t('app', 'Создан'),
            'updated_at' => Yii::t('app', 'Изменен'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getChips()
    {
        return $this->hasMany(Chips::class, ['payment_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::class, ['id' => 'account_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(Visits::class, ['id' => 'visit_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }
    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CategoryTypes::class, ['id' => 'type_id']);
    }

    public function getDebts()
    {
        return $this->hasMany(Debts::class, ['id' => 'debt_id'])
            ->viaTable('debts_payments_relation', ['payment_id' => 'id']);
    }

    public function balanceUpdateStart()
    {
        $this->balanceTransaction = $this->getDb()->beginTransaction(Transaction::SERIALIZABLE);
    }

    public function balanceUpdateEnd()
    {
        $this->balanceTransaction->commit();
    }

    public function updateAccountBalance()
    {
        if (empty($this->account_id) === true) {
            return;
        }

        $transaction = $this->getDb()->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $history = new AccountsBalanceHistory();
            $history->payment_id = $this->id;
            $history->account_id = $this->account_id;
            $history->old_balance = $this->account->balance;
            $history->new_balance = $this->account->balance + $this->amount;

            $this->account->balance = $history->new_balance;

            if ($history->save() && $this->account->save()) {
                $transaction->commit();

                return;
            }

            Yii::debug($history->getErrors());
            Yii::debug($this->account->getErrors());

            throw new Exception('Balance update failed');
        } catch (Throwable $e) {
            $transaction->rollBack();
            Yii::error($e);

            throw new Exception('Balance update failed');
        }
    }

    public function updateLocationBalance()
    {
        if (empty($this->location_id) === true) {
            return;
        }

        if ($this->location->share === 0) {
            $type = CategoryTypes::getTypeByName(CategoryTypes::PAYMENT_TYPE_NAME);

            if ($this->type_id !== $type->id) {
                return;
            }
        }

        $transaction = $this->getDb()->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $history = new LocationsBalanceHistory();
            $history->payment_id = $this->id;
            $history->location_id = $this->location_id;
            $history->old_balance = $this->location->balance;
            $history->new_balance = $this->location->balance + $this->amount;

            $this->location->balance = $history->new_balance;

            if ($history->save() && $this->location->save()) {
                $transaction->commit();

                return;
            }

            Yii::debug($history->getErrors());
            Yii::debug($this->location->getErrors());

            throw new Exception('Balance update failed');
        } catch (Throwable $e) {
            $transaction->rollBack();
            Yii::error($e);

            throw new Exception('Balance update failed');
        }
    }

    public function rollbackDependencies()
    {
        $this->rollbackLocationBalance();
        $this->rollbackAccountBalance();

        $debts = $this->debts;
        $credit = $this->amount;

        /** @var Debts $debt */
        foreach ($debts as $debt) {
            $debtPaid = $debt->paid_amount;
            $debtAmountDiff = $debtPaid - $credit;
            $visit = $debt->visit;

            if ($debtAmountDiff > 0) {
                $debt->paid_amount = $debtPaid - $credit;
                $debt->save();

                if ($visit !== null) {
                    $visit->status = Visits::STATUS_DONE;;
                    $debt->visitProduct->status = Visits::STATUS_DONE;

                    $debt->visitProduct->save();
                    $visit->save();
                }

                break;
            }

            if ($debtAmountDiff <= 0) {
                $debt->paid_amount = 0;
                $debt->paid_at = null;
                $debt->paid_by = null;
                $debt->save();

                $credit -= $debtPaid;

                if ($visit !== null) {
                    $visit->status = Visits::STATUS_DONE;;
                    $visit->fillStatusDates();
                    $debt->visitProduct->status = VisitsProducts::STATUS_DONE;

                    $debt->visitProduct->save();
                    $visit->save();
                }
            }
        }
    }

    private function rollbackLocationBalance()
    {
        if (empty($this->location_id) === true) {
            return;
        }

        $history = new LocationsBalanceHistory();
        $history->payment_id = $this->id;
        $history->location_id = $this->location_id;
        $history->old_balance = $this->location->balance;
        $history->new_balance = $this->location->balance - $this->amount;

        $this->location->balance = $history->new_balance;

        if ($history->save() === false || $this->location->save() === false) {
            Yii::error(VarDumper::dumpAsString($history->getErrors()), __METHOD__);
            Yii::error(VarDumper::dumpAsString($this->location->getErrors()), __METHOD__);

            throw new Exception('location balance update failed');
        }
    }

    private function rollbackAccountBalance()
    {
        if (empty($this->account_id) === true) {
            return;
        }

        $history = new AccountsBalanceHistory();
        $history->payment_id = $this->id;
        $history->account_id = $this->account_id;
        $history->old_balance = $this->account->balance;
        $history->new_balance = $this->account->balance - $this->amount;

        $this->account->balance = $history->new_balance;

        if ($history->save() === false || $this->account->save() === false) {
            Yii::error(VarDumper::dumpAsString($history->getErrors()), __METHOD__);
            Yii::error(VarDumper::dumpAsString($this->account->getErrors()), __METHOD__);

            throw new Exception('Account balance update failed');
        }
    }
}
