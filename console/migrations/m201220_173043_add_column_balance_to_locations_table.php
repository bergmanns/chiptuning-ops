<?php

use yii\db\Migration;

/**
 * Class m201220_173043_add_column_balance_to_locations_table.
 */
class m201220_173043_add_column_balance_to_locations_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('locations', 'balance', $this->float()->defaultValue(0)->after('id'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('locations', 'balance');
    }
}
