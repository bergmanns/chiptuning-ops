<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payments}}`.
 */
class m200130_164234_create_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payments}}', [
            'id' => $this->primaryKey(),
            'visit_id' => $this->integer(),
            'location_id' => $this->integer(),
            'name' => $this->string(),
            'type' => "ENUM('visit', 'dealer', 'operations', 'return', 'part')",
            'status' => "ENUM('not_paid', 'local_pay', 'paid')",
            'amount' => $this->float(),
            'hq_share' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'payments_visit_key',
            'payments',
            'visit_id',
            'visits',
            'id'
        );
        $this->addForeignKey(
            'payments_location_key',
            'payments',
            'location_id',
            'locations',
            'id'
        );
        $this->addForeignKey(
            'chips_payment_key',
            'chips',
            'payment_id',
            'payments',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payments}}');
    }
}
