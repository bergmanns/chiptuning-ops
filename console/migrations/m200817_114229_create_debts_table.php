<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%debts}}`.
 */
class m200817_114229_create_debts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('debts', [
            'id' => $this->primaryKey(),
            'cash_flow_id' => $this->integer(),
            'paid_by' => $this->integer(),
            'name' => $this->string()->notNull(),
            'amount' => $this->float(),
            'paid_amount' => $this->float()->defaultValue(0),
            'paid_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'debts_cash_flow_key',
            'debts',
            'cash_flow_id',
            'cash_flow',
            'id'
        );

        $this->addForeignKey(
            'debts_paid_by_key',
            'debts',
            'paid_by',
            'staff',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%debts}}');
    }
}
