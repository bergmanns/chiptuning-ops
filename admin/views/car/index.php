<?php

use admin\models\Clients;
use admin\widgets\DropDownList;
use common\components\user\AccessChecker;
use common\models\CarChassis;
use common\models\CarEngines;
use common\models\CarMakeVariants;
use common\models\CarModelVariants;
use common\models\Cars;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel admin\models\CarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Машины';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cars-index">

    <h1><?=Html::encode($this->title)?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::ADD_CAR_ITEM) === true) {
    echo Html::a('Добавить', ['create'], ['class' => 'btn btn-success']);
}?>
        <?=Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success'])?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=GridView::widget([
    'options' => [
        'class' => 'table-responsive',
    ],
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        [
            'attribute' => 'client_id',
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'client_id',
                'data' => ArrayHelper::map(
                    Clients::getWithPhone(),
                    'id', 'name_with_phone'
                ),
            ]),
            'headerOptions' => [
                'style' => 'width: 15%',
            ],
            'value' => function ($model) {
                /* @var $model \common\models\Cars */
                return $model->client->full_name ?? null;
            },
        ],
        [
            'attribute' => 'make_id',
            'label' => 'Марка',
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'make_id',
                'data' => ArrayHelper::map(CarMakeVariants::find()->all(), 'id', 'name'),
            ]),
            'headerOptions' => [
                'style' => 'width: 10%',
            ],
            'value' => function ($model) {
                /* @var $model \common\models\Cars */
                return $model->chassis->make->name ?? null;
            },
        ],
        [
            'attribute' => 'model_variant_id',
            'label' => 'Модель',
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'model_variant_id',
                'data' => ArrayHelper::map(CarModelVariants::find()->andFilterWhere(['make_id' => $searchModel->make_id])->all(), 'id', 'name'),
            ]),
            'headerOptions' => [
                'style' => 'width: 10%',
            ],
            'value' => function ($model) {
                /* @var $model \common\models\Cars */
                return $model->chassis->model->name ?? null;
            },
        ],
        [
            'attribute' => 'chassis_id',
            'label' => 'Кузов',
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'chassis_id',
                'data' => ArrayHelper::map(
                    CarChassis::find()
                    ->andFilterWhere([
                        'make_id' => $searchModel->make_id,
                        'model_id' => $searchModel->model_variant_id,
                    ])
                    ->all()
                    , 'id', 'name'),
            ]),
            'headerOptions' => [
                'style' => 'width: 10%',
            ],
            'value' => function ($model) {
                /* @var $model \common\models\Cars */
                return $model->chassis->name ?? null;
            },
        ],

        [
            'attribute' => 'engine_id',
            'headerOptions' => [
                'style' => 'width: 20%',
            ],
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'engine_id',
                'data' => ArrayHelper::map(CarEngines::find()->andFilterWhere([
                    'chassis_id' => $searchModel->chassis_id,
                ])->all(), 'id', 'name'),
            ]),
            'value' => function ($model) {
                /* @var $model \common\models\Cars */
                return $model->engine->fullName ?? null;
            },
        ],
        [
            'attribute' => 'engine_model',
            'headerOptions' => [
                'style' => 'width: 20%',
            ],
            'filter' => DropDownList::widget([
                'model' => $searchModel,
                'attribute' => 'engine_model',
                'data' => ArrayHelper::map(Cars::find()->select('engine_model')->distinct()->cache(60)->all(), 'engine_model', 'engine_model'),
            ]),
        ],
//            'chip_id',
        'issue_year',
        'vin_number',
        'gos_number',
        'eng_chip_status',
        'gear_chip_status',
        //'files_link',
        'created_at',
        //'updated_at',
        //'deleted_at',

        [
            'class' => 'admin\components\grid\ActionWithAccessColumn',
            'role' => Yii::$app->user->getIdentity()->role ?? null,
        ],
    ],
]);?>


</div>
