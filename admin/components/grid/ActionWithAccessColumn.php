<?php

declare(strict_types = 1);

namespace admin\components\grid;

use admin\models\User;
use yii\grid\ActionColumn;

class ActionWithAccessColumn extends ActionColumn
{
    public $role;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        $this->changeTemplateByRole();
    }

    private function changeTemplateByRole()
    {
        if ($this->role === User::ROLE_MANAGER) {
            $this->template = '{view}';
        }
    }
}