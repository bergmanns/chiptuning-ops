<?php

use admin\assets\CarAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cars */

CarAsset::register($this);

$this->title = 'Добавить';
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cars-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
