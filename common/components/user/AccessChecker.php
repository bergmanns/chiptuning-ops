<?php

namespace common\components\user;

use common\models\Visits;
use common\models\User;
use Yii;
use yii\base\Component;
use yii\rbac\CheckAccessInterface;

class AccessChecker extends Component implements CheckAccessInterface
{
    public const ADD_CAR_ITEM = 'add_car_item';
    public const EDIT_CAR_ITEM = 'edit_car_item';

    private $managerPermits = [
        User::ROLE_MANAGER,
    ];
    /**
     * @inheritDoc
     */
    public function checkAccess($userId, $permissionName, $params = []): bool
    {
        $user = User::findIdentity($userId);

        if ($user === null) {
            return false;
        }

        if ($user->role === User::ROLE_ADMIN) {
            return true;
        }

        return $this->checkManagerAccess($permissionName, $user);
    }

    public function checkAccessToVisit(Visits $visit): bool
    {
        /** @var \common\models\User $user */
        $user = Yii::$app->user->getIdentity();

        if ($user->role === User::ROLE_MANAGER) {
            if ($visit->contact_id !== $user->id) {
                return false;
            }
        }

        return true;
    }

    public function checkManagerAccess(string $permissionName, User $user): bool
    {
        if ($user->role !== User::ROLE_MANAGER) {
            return false;
        }

        if (in_array($permissionName, $this->managerPermits, true) === true) {
            return true;
        }

        return false;
    }
}
