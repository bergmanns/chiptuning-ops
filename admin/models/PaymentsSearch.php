<?php

namespace admin\models;

use Carbon\CarbonImmutable;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payments;
use yii\db\ActiveQuery;

/**
 * PaymentsSearch represents the model behind the search form of `common\models\Payments`.
 */
class PaymentsSearch extends Payments
{
    public $createdAtPreset;

    public $total_amount;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['account_id','amount', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['name', 'status', 'created_at'], 'safe'],
            [['total_amount'], 'number'],
            [['createdAtPreset', 'created_at'], 'string'],
        ];
    }

    public function getFilterMonthStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterMonthEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterWeekStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfWeek()->format('Y-m-d H:i');
    }

    public function getFilterWeekEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfWeek()->startOfMonth()->format('Y-m-d H:i');
    }

    public function getFilterDayStart(): string
    {
        $now = CarbonImmutable::now();

        return $now->startOfDay()->format('Y-m-d H:i');
    }

    public function getFilterDayEnd(): string
    {
        $now = CarbonImmutable::now();

        return $now->endOfDay()->startOfMonth()->format('Y-m-d H:i');
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payments::find()
            ->with([
                'visit',
                'account',
                'location',
                'type',
                'category',
            ])
        ;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        Yii::debug($params);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'account_id' => $this->account_id,
            'type_id' => $this->type_id,
            'category_id' => $this->category_id,
            'location_id' => $this->location_id,
            'amount' => $this->amount,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query = $this->initializeDateFilter($query);
        $this->initializeTotalAmount($query);
        $query->orderBy('created_at DESC');

        return $dataProvider;
    }

    private function initializeDateFilter(ActiveQuery $query): ActiveQuery
    {
        if (!empty($this->created_at)) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);

            $query->andWhere([
                'between',
                'created_at',
                $start_date,
                $end_date,
            ]);
        }

        return $query;
    }

    private function initializeTotalAmount(ActiveQuery $query)
    {
        $this->total_amount = (int) $query->sum('amount');
    }
}
