<?php

namespace admin\controllers;

use admin\models\Payments;
use admin\models\PaymentsSearch;
use admin\models\User;
use common\components\exporter\CsvExporter;
use Throwable;
use Yii;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PaymentsController implements the CRUD actions for Payments model.
 */
class PaymentsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    /**
     * Lists all Payments models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PaymentsSearch();
        $role = $this->user->role;
        if ($role === User::ROLE_MANAGER) {
            $searchModel->location_id = $this->user->location_id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCsv()
    {
        $role = $this->user->role;
        $searchModel = new PaymentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination(false);

        if ($role === User::ROLE_MANAGER) {
            $searchModel->location_id = $this->user->location_id;
        }

        $columns = require_once Yii::getAlias('@admin') . '/views/payments/columns.php';
        $csvExporter = new CsvExporter();
        $data = $csvExporter->formatProvider($dataProvider, $columns);

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->set('Content-Type', 'application/csv');
        Yii::$app->response->headers->set('Content-Disposition', sprintf(
                'attachment;filename=%s.%s',
                'Платежи',
                'csv')
        );

        return $data;
    }
    /**
     * Displays a single Payments model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Payments model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Payments();

        if ($model->load(Yii::$app->request->post())) {
            $absAmount = abs($model->amount);

            if ((int) $model->direction === 0) {
                $model->amount = -$absAmount;
            } else {
                $model->amount = $absAmount;
            }

            if ($model->save() === true) {
                return $this->redirect(['payments/index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Payments model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Payments model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $model->status = Payments::STATUS_CANCEL;
            $model->rollbackDependencies();

            if ($model->save() === true) {
                $transaction->commit();
            }
        } catch (Throwable $e) {
            Yii::error($e);
            Yii::$app->session->setFlash('error', 'Ошибка при удалении платежа');

            $transaction->rollBack();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Payments model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Payments the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Payments::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
