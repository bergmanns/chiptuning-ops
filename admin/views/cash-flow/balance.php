<?php

use admin\assets\AccountBalanceAsset;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баланс на аккаунтах';
$this->params['breadcrumbs'][] = $this->title;

AccountBalanceAsset::register($this);
?>
<div class="accounts-index" id="account-balance">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="modal fade" id="account-balance-modal" tabindex="-1" role="dialog" aria-labelledby="account-balance-modal-label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="account-balance-modal-label">Коррекция баланса</h4>
                    <p class=divider></p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="account-name" class="control-label">Аккаунт:</label>
                                    <select type="text" v-model="currentAccount" class="form-control" id="account-name">
                                        <template v-for="account in accounts">
                                            <option v-bind:value="account">{{account.name}}</option>
                                        </template>
                                    </select>
                                <p class="help-block">Баланс: {{ currentAccount.balance }} ₽</p>
                            </div>
                            <div class="form-group">
                                <label for="account-value" class="control-label">Новый баланс:</label>
                                <input v-model="newAccountBalance"  type="text" class="form-control" id="account-value">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                    <button type="button" class="btn btn-primary" @click="updateAccountBalance">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
    <p>
        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#account-balance-modal">
            Коррекция
        </button>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'name',
                'contentOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'attribute' => 'balance',
                'format' => 'currency',
                'contentOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
        ],
    ]); ?>


</div>
