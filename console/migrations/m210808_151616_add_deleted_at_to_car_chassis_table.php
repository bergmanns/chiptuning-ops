<?php

use yii\db\Migration;

/**
 * Class m210808_151616_add_deleted_at_to_car_chassis_table.
 */
class m210808_151616_add_deleted_at_to_car_chassis_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_chassis', 'deleted_at', $this->dateTime());
        $this->addColumn('car_chassis', 'created_at', $this->dateTime());
        $this->addColumn('car_chassis', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('car_chassis', 'deleted_at');
       $this->dropColumn('car_chassis', 'created_at');
       $this->dropColumn('car_chassis', 'updated_at');
    }
}
