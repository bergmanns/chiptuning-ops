<?php

use admin\assets\CashFlowAsset;
use common\models\Accounts;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Locations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Payments */
/* @var $form yii\widgets\ActiveForm */

CashFlowAsset::register($this);

?>

<div class="payments-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryTypes::findAll(['active' => 1]), 'id', 'name'), [
                'class' => 'type-selector form-control',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->where(['active' => 1])->andFilterWhere(['type_id' => $model->type_id])->all(), 'id', 'name'), [
                'class' => 'category-selector form-control',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::findAll(['deleted_at' => null]), 'id', 'name'), ['prompt' => 'Выбрать филиал']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'account_id')->dropDownList(ArrayHelper::map(Accounts::find()->all(), 'id', 'name')) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'amount')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-1">
            <?= $form->field($model, 'direction')->label('')->radioList([
                1 => 'Приход',
                0 => 'Расход'
            ]) ?>
        </div>
    </div>




    <div class="form-group">
        <div class="form-group constant-fluid">
            <?= Html::a('Назад к списку', ['index'], ['class' => 'btn btn-default']) ?>
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
