<?php

use yii\db\Migration;

/**
 * Class m210108_161557_alter_status_column_in_payments_table
 */
class m210108_161557_alter_status_column_in_payments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('payments', 'status', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210108_161557_alter_status_column_in_payments_table cannot be reverted.\n";

        return false;
    }
}
