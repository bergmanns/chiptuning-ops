<?php

namespace admin\controllers;

use common\components\exporter\CsvExporter;
use Throwable;
use Yii;
use admin\models\{User, Debts, DebtsSearch};
use yii\db\{Exception, StaleObjectException, Transaction};
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DebtsController implements the CRUD actions for Debts model.
 */
class DebtsController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return [
            'repayment' => 'admin\actions\debts\RepaymentAction',
        ];
    }

    /**
     * Lists all Debts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DebtsSearch();

        if ($this->user->role === User::ROLE_MANAGER) {
            $searchModel->location_id = $this->user->location_id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCsv()
    {
        $searchModel = new DebtsSearch();
        $role = $this->user->role;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setPagination(false);

        if ($role === User::ROLE_MANAGER) {
            $searchModel->location_id = $this->user->location_id;
        }

        $columns = require_once Yii::getAlias('@admin') . '/views/debts/debtsColumns.php';
        $csvExporter = new CsvExporter();
        $data = $csvExporter->formatProvider($dataProvider, $columns);

        Yii::$app->response->format = Response::FORMAT_RAW;
        Yii::$app->response->headers->set('Content-Type', 'application/csv');
        Yii::$app->response->headers->set('Content-Disposition', sprintf(
                'attachment;filename=%s.%s',
                'Долги',
                'csv')
        );

        return $data;
    }

    /**
     * Displays a single Debts model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView(int $id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Debts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws Exception
     */
    public function actionCreate()
    {
        $model = new Debts();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $transaction    = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            if ($model->save() === true) {
                $transaction->commit();
                return $this->redirect(['debts/index']);
            }

            $transaction->rollBack();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Debts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate(int $id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * Deletes an existing Debts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionDelete(int $id)
    {
        $model = $this->findModel($id);

        if ($model->getPayments()->count() > 0
            || $model->getParts()->count() > 0
        ) {
            Yii::$app->session->addFlash('error', 'Невозможно удалить долг');
        } else {
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Debts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Debts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel(int $id)
    {
        if (($model = Debts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
