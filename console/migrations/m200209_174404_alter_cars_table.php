<?php

use yii\db\Migration;

/**
 * Class m200209_174404_alter_cars_table
 */
class m200209_174404_alter_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('car_models', 'make', 'make_id');
        $this->renameColumn('car_models', 'model', 'model_id');

        $this->alterColumn('car_models', 'make_id', $this->integer());
        $this->alterColumn('car_models', 'model_id', $this->integer());

        $this->addForeignKey(
            'car_models_model_key',
            'car_models',
            'model_id',
            'car_model_variants',
            'id'
        );

        $this->addForeignKey(
            'car_models_make_key',
            'car_models',
            'make_id',
            'car_make_variants',
            'id'
        );

        $this->addForeignKey(
            'car_models_variants_make_key',
            'car_model_variants',
            'make_id',
            'car_make_variants',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->renameColumn('car_models', 'make_id', 'make');
        $this->renameColumn('car_models', 'model_id', 'model');

        $this->dropForeignKey('car_models_model_key', 'car_models');
        $this->dropForeignKey('car_models_make_key', 'car_models');
    }
}
