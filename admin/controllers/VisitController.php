<?php

namespace admin\controllers;

use admin\models\Debts;
use admin\models\User;
use admin\models\Visits;
use Carbon\CarbonImmutable;
use common\models\Accounts;
use common\models\Locations;
use common\models\Payments;
use common\models\VisitsProducts;
use Throwable;
use Yii;
use yii\db\Transaction;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

/**
 * VisitController implements the CRUD actions for Visits model.
 */
class VisitController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ]);
    }

    public function actions()
    {
        return [
            'create' => 'admin\actions\visit\CreateAction',
            'update' => 'admin\actions\visit\UpdateAction',
        ];
    }

    /**
     * Lists all Visits models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $accounts = $this->getVisibleAccounts();

        return $this->render('index', [
//            'accounts' => $accounts,
        ]);
    }

    /**
     * Displays a single Visits model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGenerateOrder($id)
    {
        $visitProduct = $this->findVisitsProducts($id);

        if (!in_array($visitProduct->status, [VisitsProducts::STATUS_PAID, VisitsProducts::STATUS_DONE])) {
            throw new BadRequestHttpException('Визит не завершен');
        }

        $this->layout = '@app/views/layouts/order';
        $clientOrderId = $visitProduct->visit->genClientOrder();
        $file = Yii::getAlias('@app') . '/runtime/orders/' . $clientOrderId . '.pdf';
        $html = $this->render('order', [
            'model' => $visitProduct->visit,
        ]);

        Yii::$app->html2pdf
            ->convert($html)
            ->saveAs($file)
        ;

        return Yii::$app->response->sendFile($file);
    }

    /**
     * Deletes an existing Visits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->post('id');
        $visitProduct = $this->findVisitsProducts($id);

        if ($visitProduct !== null) {
            /** @var VisitsProducts $visitProduct */
            $visit = $visitProduct->visit;

            if (in_array($visitProduct->status, [VisitsProducts::STATUS_PAID, VisitsProducts::STATUS_DONE], true) === true) {
                throw new BadRequestHttpException('Визит уже Завершен');
            }

            if (in_array($visit->status, [Visits::STATUS_PAID, Visits::STATUS_DONE], true) === true) {
                throw new BadRequestHttpException('Визит уже Завершен');
            }

            if ($visit->status === Visits::STATUS_CANCELLED) {
                throw new BadRequestHttpException('Визит уже Отменен');
            }

            $visitProduct->status = VisitsProducts::STATUS_CANCELLED;
            $visit->status = Visits::STATUS_CANCELLED;
            $visit->deleted_at = CarbonImmutable::now()->format(CarbonImmutable::DEFAULT_TO_STRING_FORMAT);
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            try {
                if ($visit->save() === false) {
                    Yii::error($visit->getErrors());

                    throw new ServerErrorHttpException('Delete failed');
                }

                if ($visitProduct->save() === false) {
                    Yii::error($visitProduct->getErrors());

                    throw new ServerErrorHttpException('Delete failed');
                }

                $transaction->commit();
            } catch (Throwable $e) {
                $transaction->rollBack();

                throw $e;
            }

        }
    }

    /**
     * Deletes an existing Visits model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionComplete()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $id = Yii::$app->request->post('id');
        /** @var VisitsProducts $visitProduct */
        $visitProduct = $this->findVisitsProducts($id);

        if ($visitProduct !== null) {
            $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            if ($visitProduct->visit->completed_at !== null) {
                Yii::$app->response->setStatusCode(400);

                return [
                    'error' => 'Визит уже завершен',
                ];
            }

            if (empty($visitProduct->category_id) === true) {
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();
                Yii::$app->response->setStatusCode(400);

                return [
                    'error' => 'Цель не указана',
                ];
            }

            if (empty($visitProduct->product_id) === true) {
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();
                Yii::$app->response->setStatusCode(400);

                return [
                    'error' => 'Продукт не указан',
                ];
            }

            if (empty($visitProduct->car->engine_model) === true) {
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();

                return [
                    'error' => 'Необходимо заполнить модель двигателя',
                ];
            }

            $user = Yii::$app->user->getIdentity();
            $role = $user->role;

            //todo Вынести в модель и сценарии
            if ($role === User::ROLE_MANAGER) {
                if (empty($visitProduct->car->gearbox_id) === true) {
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'Коробка не указана',
                    ];
                }

                if (empty($visitProduct->car->vin_number) === true) {
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'VIN не указан',
                    ];
                }

                if (empty($visitProduct->car->gos_number) === true) {
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'Гос.номер не указан',
                    ];
                }

                if (empty($visitProduct->visit->current_mileage) === true) {
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'Пробег не указан',
                    ];
                }
            }

            /** @var VisitsProducts $visitProduct */
            $visitProduct->status = VisitsProducts::STATUS_DONE;
            $visit = $visitProduct->visit;
            $visit->status = Visits::STATUS_DONE;
            $visit->fillStatusDates();

            if (($visit->save(false) === false)) {
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();

                return [
                    'error' => 'Ошибка при сохранении визита',
                ];
            }

            if ($visitProduct->save(false) === false) {
                Yii::error(VarDumper::dumpAsString($visitProduct->getErrors()));
                Yii::$app->response->statusCode = 400;
                $transaction->rollBack();

                return [
                    'error' => 'Ошибка при сохранении визита',
                ];
            }

            $partsSum = $visitProduct->getAllPartsPrice();

            $debt = new Debts();
            $debt->visit_id = $visit->id;
            $debt->type_id = $visitProduct->category->type_id;
            $debt->category_id = $visitProduct->category_id;
            $debt->location_id = $visit->location_id;

            $visitAt = CarbonImmutable::parse($visitProduct->visit_at);
            $debt->name = sprintf('За успешный визит от %s', $visitAt->format('d.m.Y H:i'));

            if ($visit->is_amount_confirmed === 1) {
                $debt->amount = $visitProduct->product->getHQShare($visit->location, $visit->real_amount) + $partsSum;
            } else {
                $debt->amount = $visitProduct->product->getHQShare($visit->location) + $partsSum;
            }

            $debt->paid_amount = $this->getPaidByBalance($debt->amount, $visit->location);

            if ($debt->amount === $debt->paid_amount) {
                $visit->status = Visits::STATUS_PAID;
                $debt->visitProduct->status = Visits::STATUS_PAID;
                $debt->visitProduct->save();
                $debt->paid_at = CarbonImmutable::now();
            }

            if ($visit->is_landing_paid === 1) {
                $visit->status = Visits::STATUS_PAID;
                $paymentToLocation = new Payments();
                $paymentToLocation->visit_id = $visit->id;
                $paymentToLocation->type_id = $debt->type_id;
                $paymentToLocation->category_id = $debt->category_id;
                $paymentToLocation->location_id = $debt->location_id;
                $paymentToLocation->status = Payments::STATUS_PAID;
                $paymentToLocation->amount = $visit->real_amount - $debt->amount;

                if ($paymentToLocation->amount > 0) {
                    if ($paymentToLocation->save() === false) {
                        Yii::debug($debt->toArray());
                        Yii::debug($visit->toArray());
                        Yii::debug($paymentToLocation->toArray());
                        Yii::error(VarDumper::dumpAsString($paymentToLocation->getErrors()));
                        Yii::$app->response->statusCode = 400;
                        $transaction->rollBack();

                        return [
                            'error' => 'Ошибка при формировании платежа',
                        ];
                    }
                }

                $paymentToAccount = new Payments();
                $paymentToAccount->visit_id = $visit->id;
                $paymentToAccount->type_id = $debt->type_id;
                $paymentToAccount->category_id = $debt->category_id;
                $paymentToAccount->status = Payments::STATUS_PAID;
                $paymentToAccount->account_id = Accounts::findOne(['name' => 'Карта'])->id; //todo Уточнить аккаунт
                $paymentToAccount->amount = $debt->amount;

                if ($paymentToAccount->save() === false) {
                    Yii::debug($debt->toArray());
                    Yii::debug($visit->toArray());
                    Yii::debug($paymentToAccount->toArray());
                    Yii::error(VarDumper::dumpAsString($paymentToAccount->getErrors()));
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'Ошибка при формировании платежа',
                    ];
                }

                $debt->paid_by = Yii::$app->user->getId();
                $debt->paid_at = CarbonImmutable::now();

            } else {
                if ($debt->save(false) === false) {
                    Yii::error(VarDumper::dumpAsString($debt->getErrors()));
                    Yii::$app->response->statusCode = 400;
                    $transaction->rollBack();

                    return [
                        'error' => 'Ошибка при формировании долга',
                    ];
                }
            }

            $visit->save();

            $transaction->commit();
        }

        return [
            'id' => $visitProduct->id,
            'externalId' => $visit->external_id,
            'isAmountConfirmed' => $visit->is_amount_confirmed,
            'isLandingPaid' => $visit->is_landing_paid,
            'real_amount' => $visit->real_amount,
            'payments' => [],
            'isNewRow' => false,
            'paid_at' => $visit->paid_at,
            'completed_at' => $visit->completed_at,
            'visit_id' => $visitProduct->visit_id,
            'visit_at' => $visitProduct->visit_at,
            'visit_month' => $visitAt->month,
            'visit_day' => $visitAt->day,
            'visit_year' => $visitAt->year,
            'visit_hour' => $visitAt->hour,
            'comment' => $visitProduct->comment,
            'status' => $this->getStatus($visit),
            'category' => [
                'name' => $visitProduct->category->name ?? null,
                'id' => $visitProduct->category->id ?? null,
            ],
            'location' => [
                'id' => $visitProduct->location->id,
                'name' => $visitProduct->location->name,
                'country' => $visitProduct->location->country->toArray(['short', 'gos_number_mask']),
            ],
            'car' => [
                'id' => $visitProduct->car->id,
                'full_name' => $visitProduct->car->getSpecific(),
                'make_name' => $visitProduct->car->make->name,
                'make_id' => $visitProduct->car->make->id,
                'model_name' => $visitProduct->car->model->name,
                'model_id' => $visitProduct->car->model->id,
                'model_variant_id' => $visitProduct->car->model->id,
                'engine_id' => $visitProduct->car->engine_id,
                'gearbox_id' => $visitProduct->car->gearbox_id,
                'engine_name' => $visitProduct->car->engine->fullName,
                'issue_year' => $visitProduct->car->issue_year,
                'vin_number' => $visitProduct->car->vin_number,
                'gos_number' => $visitProduct->car->gos_number,
                'current_mileage' => $visit->current_mileage,
            ],
            'product' => [
                'id' => $visitProduct->product->id ?? null,
                'name' => $visitProduct->product->name ?? null,
                'price' => $visitProduct->product->price ?? null,
                'total_price' => $visit->real_amount,
            ],
            'client' => [
                'phone' => $visitProduct->client->defaultPhone->phone,
                'id' => $visitProduct->client->id,
                'full_name' => $visitProduct->client->full_name,
            ],
            'parts' => $visitProduct->getParts()->select(['id', 'name', 'serial', 'count', 'amount'])->asArray()->all(),
        ];
    }

    /**
     * Finds the Visits model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Visits the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Visits::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findVisitsProducts($id)
    {
        $visitProducts = VisitsProducts::find()->where([
            'id' => $id,
        ])->one();
        if ($visitProducts !== null) {
            return $visitProducts;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function getVisibleAccounts()
    {
        return Accounts::find()
            ->where([
                'table_visible' => 1,
            ])->orderBy('id DESC')->all();
    }

    private function getPaidByBalance( ? float $amount, Locations $location) : float
    {
        if ($location->share === 0) { // Особенная логика для "основного" сервиса
            return 0;
        }

        if ($location->balance < 0) {
            return 0;
        }

        if (($location->balance - $amount) > 0) {
            return $amount;
        } else {
            return $location->balance;
        }
    }

    private function getStatus($visit): string
    {
        if ($visit->paid_at !== null && $visit->completed_at === null) {
            return 'paid_not_done';
        }

        return $visit->status;
    }
}
