<?php

use yii\db\Migration;

/**
 * Class m210717_192605_remove_products_external_id_index.
 */
class m210717_192605_remove_products_external_id_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropIndex('products_external_id_stage_idx', 'products');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createIndex('products_external_id_stage_idx', 'products', ['external_id', 'stage'], true);
    }
}
