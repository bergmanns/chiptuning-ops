<?php

namespace admin\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Debts;
use yii\db\ActiveQuery;

/**
 * DebtsSearch represents the model behind the search form of `common\models\Debts`.
 */
class DebtsSearch extends Debts
{
    public $total_amount;
    public $type_id;
    public $category_id;
    public $location_id;
    public $visit_id;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'paid_by', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['name', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Debts::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'visit_id' => $this->visit_id,
            'type_id' => $this->type_id,
            'location_id' => $this->location_id,
            'category_id' => $this->category_id,
            'paid_by' => $this->paid_by,
            'amount' => $this->amount,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query = $this->initializeDateFilter($query);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $this->initializeTotalAmount($query);

        return $dataProvider;
    }

    private function initializeDateFilter(ActiveQuery $query): ActiveQuery
    {
        if (!empty($this->created_at)) {
            list($start_date, $end_date) = explode(' - ', $this->created_at);

            $query->andWhere([
                'between',
                'created_at',
                $start_date,
                $end_date,
            ]);
        }

        return $query;
    }

    private function initializeTotalAmount(ActiveQuery $query)
    {
        $this->total_amount = (int) $query->sum('amount') - (int) $query->sum('paid_amount');
    }
}
