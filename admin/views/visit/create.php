<?php

use admin\assets\VisitCreateAsset;

/* @var $this yii\web\View */
/* @var $model common\models\Visits */

VisitCreateAsset::register($this);

$this->title = 'Новая запись';
$this->params['breadcrumbs'][] = ['label' => 'Визиты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="visits-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>


