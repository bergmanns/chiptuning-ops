<?php

use yii\db\Migration;

/**
 * Class m201125_175822_add_paid_status
 */
class m201125_175822_add_paid_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('visits_products', 'status', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201125_175822_add_paid_status cannot be reverted.\n";

        return false;
    }
}
