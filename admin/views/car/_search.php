<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\CarSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cars-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'model_id') ?>

    <?= $form->field($model, 'client_id') ?>

    <?= $form->field($model, 'chip_id') ?>

    <?= $form->field($model, 'issue_year') ?>

    <?php // echo $form->field($model, 'vin_number') ?>

    <?php // echo $form->field($model, 'gos_number') ?>

    <?php // echo $form->field($model, 'eng_chip_status') ?>

    <?php // echo $form->field($model, 'gear_chip_status') ?>

    <?php // echo $form->field($model, 'files_link') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
