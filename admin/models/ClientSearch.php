<?php

namespace admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * ClientSearch represents the model behind the search form of `common\models\Clients`.
 */
class ClientSearch extends Clients
{
    public $region_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'region_id', 'default_phone_id'], 'integer'],
            [['full_name', 'email', 'drive2', 'created_at', 'deleted_at', 'phone', 'region_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Clients::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query = $this->modifyQueryByRole($query);

        // grid filtering conditions
        $query->andFilterWhere([
            'clients.id' => $this->id,
            'clients.created_at' => $this->created_at,
            'clients.deleted_at' => $this->deleted_at,
        ]);

        if ($this->phone !== null) {
            $query->joinWith(['defaultPhone'])->andWhere(['like', 'client_phones.phone', $this->phone]);
        }

        if ($this->region_name !== null) {
            $query->joinWith(['region'])->andWhere(['like', 'regions.name', $this->region_name]);

            \Yii::debug($query->createCommand()->getRawSql());
        }
        $query->andFilterWhere(['like', 'clients.full_name', $this->full_name])
            ->andFilterWhere(['like', 'clients.email', $this->email])
            ->andFilterWhere(['like', 'clients.drive2', $this->drive2]);

        return $dataProvider;
    }

    private function modifyQueryByRole(ActiveQuery $query): ActiveQuery
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();

        if ($user->role === User::ROLE_ADMIN) {
            return $query;
        }

        $query->joinWith(['locations'])->andWhere([
             'locations.id' => $user->location_id,
         ]);

        return $query;
    }
}
