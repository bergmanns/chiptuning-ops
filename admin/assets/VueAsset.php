<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Vue framework asset bundle.
 */
class VueAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/vue/dist';
    public $js = [
        'vue.js',
    ];
    public $depends = [
    ];
}
