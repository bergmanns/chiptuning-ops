<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Main admin application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/admin.js',
        'js/confirm-swal.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'admin\assets\PhoneMaskAsset',

        // импорт файлов SweetAlertAsset
        'admin\assets\SweetAsset',
        // 'aryelds\sweetalert\SweetAlertAsset',
    ];
}
