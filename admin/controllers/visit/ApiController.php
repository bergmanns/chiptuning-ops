<?php

namespace admin\controllers\visit;

use common\models\VisitsProducts;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * VisitController implements the CRUD actions for Visits model.
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'search' => 'admin\actions\visit\api\SearchAction',
            'payments' => 'admin\actions\visit\api\PaymentsAction',
            'payment-save' => 'admin\actions\visit\api\PaymentsSaveAction',
        ];
    }

    /**
     * Return info about all Visits Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        return;

        Yii::$app->response->format = Response::FORMAT_JSON;
        $modelsQuery = VisitsProducts::find()
            ->select([
                'visits_products.*',
                'car_models.*',
                'car_model_variants.name as model_name',
                'car_make_variants.name as make_name',
                'client_phones.phone as phone',
            ])
            ->joinWith([
                'visit',
                'client',
                'car',
                'location',
                'product',
                'reason',
            ])
            ->leftJoin('car_models', '`car_models`.`id` = `cars`.`model_id`')
            ->leftJoin('car_model_variants', '`car_model_variants`.`id` = `car_models`.`model_id`')
            ->leftJoin('car_make_variants', '`car_make_variants`.`id` = `car_models`.`make_id`')
            ->leftJoin('client_phones', '`client_phones`.`id` = `clients`.`default_phone_id`')
            ->orderBy('visit_at ASC')
            ->asArray()
        ;

        $count = $modelsQuery->count();
        return [
            'total' => $count,
            'per_page' => $count,
            'current_page' => 1,
            'last_page' => 1,
//            'next_page_url' => '',
            'from' => 1,
            'to' => $count,
            'data' => $modelsQuery->all(),
        ];
    }
}
