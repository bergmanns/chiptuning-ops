<?php

use Carbon\CarbonImmutable;
use common\models\CashFlow;
use common\models\Payments;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CashFlow */

$this->title = sprintf('%s, %s', $model->name, CarbonImmutable::parse($model->created_at)->format('d.m.Y'));
$this->params['breadcrumbs'][] = ['label' => 'Cash Flows', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);

?>
<div class="cash-flow-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Delete', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Are you sure you want to delete this item?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'type_id',
                'value' => function ($model) {
                    /* @var $model CashFlow */
                    if ($model->type !== null) {
                        return $model->type->name;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'category_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CashFlow */
                    if ($model->category !== null) {
                        $html = Html::a($model->category->name, ['categories/view', 'id' => $model->category_id]);
                        return $html;
                    }

                    return null;
                },
            ],

            [
                'attribute' => 'location_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CashFlow */
                    if ($model->location !== null) {
                        $html = Html::a($model->location->name, ['location/view', 'id' => $model->location_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'amount',
            'share',
            'created_at:datetime',
            'updated_at:datetime',
            'paid_at:datetime',
        ],
    ]) ?>

    <?
    $dataProvider = new ActiveDataProvider([
        'query' => $model->getPayments(),
    ]);
    echo '<h3>Платежи</h3>';
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'visit_id',
            [
                'attribute' => 'account_id',
                'value' => function ($model) {
                    /* @var $model Payments */
                    return $model->account->name ?? null;
                },
            ],

            [
                'attribute' => 'status',
                'format' => 'HTML',
                'value' => function ($model) {
                /* @var $model Payments */

                    switch ($model->status) {
                        case Payments::STATUS_NOT_PAID:
                            return "<span class=\"gray\">Не оплачен</span>";
                        case Payments::STATUS_LOCAL_PAY:
                            return "<span class=\"green\">Оплачен наличными</span>";
                        case Payments::STATUS_PAID:
                            return "<span class=\"green\">Оплачен</span>";
                        case Payments::STATUS_CANCEL:
                            return "<span class=\"red\">Отменен</span>";
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'amount',
                'format' => 'html',
                'headerOptions' => [
                    'style' => 'width: 5%',
                ],
                'value' => function ($model) {
                    /* @var $model Payments */

                    if ($model->amount < 0) {
                        return "<span class=\"red\">$model->amount</span>";
                    } else {
                        return "<span class=\"green\">$model->amount</span>";
                    }
                },
            ],
            'created_at:datetime',
        ],
    ]);
    ?>
</div>
