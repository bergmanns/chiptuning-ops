<?php

use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Locations;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\Debts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="debts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::findAll(['deleted_at' => null]), 'id', 'name')) ?>
    <?= $form->field($model, 'amount')->textInput() ?>
    <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryTypes::findAll(['active' => 1]), 'id', 'name')) ?>
    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::findAll(['active' => 1]), 'id', 'name')) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::a('Отмена', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Добавить долг', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
