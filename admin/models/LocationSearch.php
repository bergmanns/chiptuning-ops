<?php

namespace admin\models;

use common\models\Locations;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LocationSearch represents the model behind the search form of `common\models\Locations`.
 */
class LocationSearch extends Locations
{
    public $country_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region_id', 'share'], 'integer'],
            [['name'], 'string'],
            [['region_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Locations::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        \Yii::debug($this->getErrors());
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
             $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'locations.id' => $this->id,
            'locations.share' => $this->share,
            'locations.region_id' => $this->region_id,
        ]);

        if ($this->country_id !== null) {
            $query->joinWith(['country'])->andFilterWhere([
                'countries.id' => $this->country_id,
            ]);

            \Yii::debug($query->createCommand()->getRawSql());
        }

        return $dataProvider;
    }
}
