<?php

declare(strict_types = 1);

namespace admin\actions\visit;

use admin\models\Visits;
use common\models\VisitsProducts;
use Yii;

class CreateAction extends BaseVisitAction
{
    /**
     * Create the Visits model.
     * If updated is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function run()
    {
        $this->model    = new Visits();

        if (Yii::$app->request->getMethod() === 'POST') {
            return $this->handleVisit();
        }

        Yii::$app->response->setStatusCode(400);

        return $this->controller->asJson($this->model->getErrors());
    }

    protected function getVisitProducts($visitId): ?VisitsProducts
    {
        return new VisitsProducts();
    }
}
