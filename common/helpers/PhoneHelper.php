<?php

declare(strict_types = 1);

namespace common\helpers;

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Throwable;

class PhoneHelper
{
    public const DEFAULT_COUNTRY = 'RU';

    public static function validatePhoneNumber($phone, string $country = self::DEFAULT_COUNTRY): bool
    {
        $phoneUtil = PhoneNumberUtil::getInstance();

        try {
            $phone = $phone instanceof PhoneNumber ? $phone : $phoneUtil->parse($phone, $country);

            if ($phoneUtil->isPossibleNumber($phone) === true && $phoneUtil->isValidNumber($phone) === true) {
                return true;
            }
        } catch (Throwable $e) {
            return false;
        }

        return false;
    }

    /**
     * @param string|PhoneNumber $phone
     * @param int $format
     * @return string
     * @throws NumberParseException
     */
    public static function getFormattedNumber($phone, $format): string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phone = $phone instanceof PhoneNumber ? $phone : $phoneUtil->parse($phone, self::DEFAULT_COUNTRY);

        return $phoneUtil->format($phone, $format);
    }

    /**
     * @param $phone
     * @return string
     * @throws NumberParseException
     */
    public static function getE164FormatterNumber($phone): string
    {
        return self::getFormattedNumber($phone, PhoneNumberFormat::E164);
    }

    /**
     * @param $phone
     * @return string
     * @throws NumberParseException
     */
    public static function getNational($phone): string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phone = $phone instanceof PhoneNumber ? $phone : $phoneUtil->parse($phone, self::DEFAULT_COUNTRY);

        return $phone->getNationalNumber();
    }

    public static function getPhoneRegion($phone): ?string
    {
        $phoneUtil = PhoneNumberUtil::getInstance();

        try {
            $phone = $phone instanceof PhoneNumber ? $phone : $phoneUtil->parse($phone, null);

            return $phoneUtil->getRegionCodeForNumber($phone);
        } catch (Throwable $e) {
            return null;
        }
    }
}
