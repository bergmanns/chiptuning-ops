<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%car_models}}`.
 */
class m200130_164019_create_car_models_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%car_models}}', [
            'id' => $this->primaryKey(),
            'make' => $this->string(),
            'model' => $this->string(),
            'body' => $this->string(),
            'stock_power' => $this->integer(),
            'injection' => $this->string(),
            'engine_name' => $this->string(),
            'gearbox_model' => $this->string(),
            'gearbox_vers' => $this->string(),
            'engine_type' => "ENUM('diesel', 'gasoline')"
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'cars_model_key',
            'cars',
            'model_id',
            'car_models',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%car_models}}');
    }
}
