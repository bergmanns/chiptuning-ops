<?php

namespace admin\assets;

use yii\web\AssetBundle;

/**
 * Vue framework asset bundle.
 */
class VueResourceAsset extends AssetBundle
{
    public $sourcePath = '@vendor/npm-asset/vue-resource/dist';
    public $js = [
        'vue-resource.js',
    ];
}
