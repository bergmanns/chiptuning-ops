<?php

use yii\db\Migration;

/**
 * Class m200131_174532_add_keys_for_visits_table
 */
class m200131_174532_add_keys_for_visits_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'visits_car_key',
            'visits',
            'car_id',
            'cars',
            'id'
        );

        $this->addForeignKey(
            'visits_staff_key',
            'visits',
            'contact_id',
            'staff',
            'id'
        );

        $this->addForeignKey(
            'visits_clients_key',
            'visits',
            'client_id',
            'clients',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('visits_car_key', 'visits');
        $this->dropForeignKey('visits_staff_key', 'visits');
        $this->dropForeignKey('visits_clients_key', 'visits');
    }
}
