<?php

declare(strict_types = 1);

namespace common\components\log;

use common\components\transport\TelegramTransport;
use Throwable;
use Yii;
use yii\caching\Cache;
use yii\helpers\VarDumper;
use yii\log\Target;

class TelegramTarget extends Target
{
    /**
     * Default error report timeout 1 minute.
     */
    public const CACHE_DURATION = 60;

    /**
     * @var string|null telegram error chat ID
     */
    public $channel;

    /**
     * @var TelegramTransport|null
     */
    private $client;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->initializeClient();

        Yii::debug('Init ' . __CLASS__, __CLASS__);

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function export()
    {
        if ($this->client === null) {
            return;
        }

        // https://github.com/yiisoft/yii2/issues/16251
        $messages = static::filterMessages(
            $this->messages,
            $this->levels,
            $this->categories,
            $this->except
        );

        foreach ($messages as $message) {
            try {
                $this->processMessage($message);
            } catch (Throwable $error) {
                // Something went wrong while logging.
            }
        }
    }

    private function initializeClient(): void
    {
        if ($this->channel === null) {
            Yii::debug('Channel not set ' . __CLASS__, __CLASS__);

            return;
        }

        $this->client = Yii::$app->telegramTransport;
    }

    /**
     * Processes a single log message.
     *
     * @throws \yii\base\InvalidConfigException
     */
    private function processMessage(array $messageData): void
    {
        Yii::debug('Send Message ' . __CLASS__, __CLASS__);

        $userId = Yii::$app->user->getId();
        $request = Yii::$app->request;
        /** @var Throwable $exception */
        $error = $messageData[0];

        if ($error instanceof Throwable) {
            $reportMessage = vsprintf(
<<<TEXT
Ошибка: %s
Код ошибки: %s
Тип ошибки: %s
Файл: %s
Строка: %s
Referrer: %s
Сайт: %s
IP: %s
Пользователь ID: %d
TEXT
    , [
        $error->getMessage(),
        $error->getCode(),
        $messageData[2],
        $error->getFile(),
        $error->getLine(),
        $request->getReferrer(),
        $request->getAbsoluteUrl(),
        $request->getUserIP(),
        $userId === 0 ? 'Guest' : $userId,
    ]);
        } else {
            if (empty($messageData[0]) === true) {
                return;
            }
            $reportMessage = vsprintf(
<<<TEXT
Данные: %s
Referrer: %s
Сайт: %s
IP: %s
Пользователь ID: %d
TEXT
, [
    VarDumper::export($messageData[0]),
    $request->getReferrer(),
    $request->getAbsoluteUrl(),
    $request->getUserIP(),
    $userId === 0 ? 'Guest' : $userId,
]);
        }
        /** @var $cache Cache */
        $cache = Yii::$app->cache;
        $cacheKey = sprintf('error-%s', md5($reportMessage));
        $messageInCache = $cache->get($cacheKey);

        if ($messageInCache === false) {
            $this->client->sendMessage($this->channel, 'custom', ['text' => $reportMessage]);

            $cache->set($cacheKey, 1, self::CACHE_DURATION);
        }
    }
}
