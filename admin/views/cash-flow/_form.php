<?php

use admin\assets\CashFlowAsset;
use admin\widgets\DropDownList;
use common\models\Accounts;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Locations;
use common\models\Payments;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CashFlow */
/* @var $form yii\widgets\ActiveForm */

CashFlowAsset::register($this);

?>

<div class="cash-flow-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model) ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'type_id')->dropDownList(ArrayHelper::map(CategoryTypes::findAll(['active' => 1]), 'id', 'name'), [
                'class' => 'type-selector form-control',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(Categories::find()->where(['active' => 1])->andFilterWhere(['type_id' => $model->type_id])->all(), 'id', 'name'), [
                'class' => 'category-selector form-control',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::findAll(['deleted_at' => null]), 'id', 'name')) ?>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md-2">
            <label>Счет</label>
            <?= DropDownList::widget([
                'name' => 'account_id_selector',
                'data' => ArrayHelper::map(Accounts::find()->all(), 'id', 'name'),
                'options' => [
                    'placeholder' => 'Выбрать счет ...',
                    'class' => 'payment-account-input'
                ]
            ]) ?>
        </div>
        <div class="col-md-2">
            <label>Сумма</label>
            <?= Html::input('text', 'payment-amount-input', null, [
                'class' => 'form-control payment-amount-input',
            ]) ?>
        </div>
        <div class="col-md-2">  <?= Html::button('Добавить', ['class' => 'btn btn-success add-payment-form', 'style' => 'margin-top: 25px;']) ?></div>
    </div>
    <div class="form-group payments-result container-fluid" data-payments-count="<?=$model->getPayments()->count() ?>">
        <?
        /** @var Payments $payment */
        foreach ($model->activePayments as $index => $payment) {
            $accountName = $payment->account->name;
                echo  "<div class=\"payment-row\">
            <input type=\"hidden\" name=\"payments[$index][id]\" value=\"$payment->id\">
            <input type=\"hidden\" name=\"payments[$index][account_id]\" value=\"$payment->account_id\">
            <input type=\"hidden\" name=\"payments[$index][value]\" value=\"$payment->amount\">
            <span class=\"account-id\">$payment->id $accountName: </span><span class=\"account-value\">$payment->amount ₽</span>
            <button account-id=\"$payment->account_id\" type=\"button\" class=\"btn btn-sm btn-default remove-item\"><i class=\"glyphicon glyphicon-trash\"></i></button>
        </div>";
            }
        ?>
    </div>
    <div class="form-group constant-fluid">
        <?= Html::a('Назад к списку', ['index'], ['class' => 'btn btn-default']) ?>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
