
$( document ).ready(function() {
    function initPhoneMask(){
        var isComplete;
        var url = "/js/phone-codes.json";
        var maskOpts = {
            inputmask: {
                definitions: {
                    "#": {
                        validator: "[0-9]",
                        cardinality: 1
                    }
                },
                showMaskOnHover: false,
                autoUnmask: true,
                clearMaskOnLostFocus: false,
            },
            match: /[0-9]/,
            replace: "#",
            listKey: "mask"
        };

        $.ajax({
            url: url,
            dataType: "json",
            success: function(response) {
                var maskList = response;
                var listCountries = $.masksSort(maskList, ["#"], /[0-9]|#/, "mask");

                $('.phone_mask').inputmasks(
                    $.extend(true, {}, maskOpts, {
                        list: listCountries
                    })
                )
            },
            error: function(err){
                console.log('error', err);
            }
        });
    }

    initPhoneMask();
    $('#locations-region_id').on('change', function(){
       if (this.value === '0') {
           $('.country-form').css('display', 'block');
           $('.region-form').css('display', 'block');
       } else {
           $('.country-form').css('display', 'none');
           $('.region-form').css('display', 'none');
       }
    });

    $('#regions-country_id').on('change', function(){
        if (this.value === '0') {
            $('.new-country-form').css('display', 'block');
        } else {
            $('.new-country-form').css('display', 'none');
        }
    });
});
