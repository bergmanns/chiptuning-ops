<?php

use common\models\Cars;
use yii\grid\GridView;use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Clients */
/* @var $carsProvider \yii\data\ActiveDataProvider */

$this->title = $model->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$carsCount = $model->getCars()->count();
YiiAsset::register($this);
?>
<div class="clients-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'region_id',
                'value' => function ($model) {
                    /* @var $model \admin\models\Clients */
                    return $model->region->name ?? null;
                },
            ],
            'phone',
            'full_name',
            'email:email',
            'drive2',
            'created_at',
            'deleted_at',
        ],
    ]) ?>

    <?php if ($carsCount > 1) {
         echo '<h3>Машины клиента</h3>';
         echo GridView::widget([
            'dataProvider' => $carsProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'model_make',
                    'value' => function ($model) {
                        /* @var $model \common\models\Cars */
                        return $model->chassis->make ?? null;
                    },
                ],
                [
                    'attribute' => 'model_model',
                    'value' => function ($model) {
                        /* @var $model \common\models\Cars */
                        return $model->chassis->model ?? null;
                    },
                ],
                'chip_id',
                'issue_year',
                'vin_number',
                'gos_number',
                'eng_chip_status',
                'gear_chip_status',
                //'files_link',
                'created_at',
                //'updated_at',
                //'deleted_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'controller' => 'car'
                ],
            ],
        ]);
    } elseif ($carsCount == 1) {
        $car = $model->getCars()->one();
        echo '<p>';
        echo '<h3>Машина клиента</h3>';
        echo Html::a('Редактировать', ['car/update', 'id' => $car->id], ['class' => 'btn btn-primary']);
        echo '</p>';
        echo DetailView::widget([
            'model' => $car,
            'attributes' => [
            'id',
            [
                'attribute' => 'model_make',
                'label' => 'Марка',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Cars */
                    if ($model->chassis !== null) {
                        $html = Html::a($model->chassis->make, ['car-make-variant/view', 'id' => $model->chassis->make_id]);
                        return $html;
                    }

                    return null;
                },
            ],

            [
                'attribute' => 'model_model',
                'label' => 'Модель',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Cars */
                    if ($model->chassis !== null) {
                        $modelName = $model->chassis->model->name ?? null;
                        $html = Html::a($modelName, ['car-model-variant/view', 'id' => $model->chassis->model_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'chassis_id',
                'label' => 'Кузов',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Cars */
                    if ($model->chassis !== null) {
                        $modelName = $model->chassis->name ?? null;
                        $html = Html::a($modelName, ['chassis/view', 'id' => $model->engine->chassis_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'engine_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Cars */
                    if ($model->engine !== null) {
                        $modelName = $model->engine->fullName ?? null;
                        $html = Html::a($modelName, ['engine/view', 'id' => $model->engine_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'client_id',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model Cars */
                    if ($model->client !== null) {
                        $html = Html::a($model->client->full_name, ['client/view', 'id' => $model->client_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'chip_id',
            'issue_year',
            'vin_number',
            'gos_number',
            'eng_chip_status',
            'gear_chip_status',
            'files_link',
            'created_at',
            'updated_at',
            'deleted_at',
        ],
        ]);
    } ?>

</div>
