<?php

namespace admin\controllers\accounts;

use common\models\Accounts;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Payments;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * VisitController implements the CRUD actions for Visits model.
 */
class ApiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'index' => ['GET'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
        ];
    }

    /**
     * Return info about all Visits Products models.
     * @return mixed
     */
    public function actionIndex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $items = Accounts::find()->asArray()->where([])->all();

        return $items;
    }

    public function actionVisible()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $items = $this->getVisibleAccounts();

        return $items;
    }

    public function actionBalance()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $id = Yii::$app->request->post('id');
        $account = $this->findModel($id);
        $balance = Yii::$app->request->post('balance');
        $newBalance = Yii::$app->request->post('newBalance');

        $type = CategoryTypes::getTypeByName(CategoryTypes::PAYMENT_TYPE_NAME);
        $category = Categories::getCategoryByName(Categories::BALANCE_FIX_CATEGORY_NAME, $type);

        $balanceFixPayment = new Payments();
        $balanceFixPayment->type_id = $type->id;
        $balanceFixPayment->category_id = $category->id;
        $balanceFixPayment->account_id = $account->id;
        $balanceFixPayment->name = 'Коррекция баланса';
        $balanceFixPayment->status = Payments::STATUS_PAID;
        $balanceFixPayment->amount = $newBalance - $account->balance;

        Yii::debug($balanceFixPayment->toArray());

        if ($balanceFixPayment->save() === false) {
            Yii::debug($balanceFixPayment->getErrors());

            throw new BadRequestHttpException('Ошибка Базы Данных');
        }

        return $account->toArray();
    }

    private function getVisibleAccounts()
    {
        return Accounts::find()
            ->where([
                'table_visible' => 1,
            ])->orderBy('id DESC')->all();
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Accounts::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
