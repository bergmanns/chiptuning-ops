<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "locations_balance_history".
 *
 * @property int $id
 * @property int|null $location_id
 * @property int|null $payment_id
 * @property int|null $change_by
 * @property float|null $old_balance
 * @property float|null $new_balance
 * @property string|null $created_at
 *
 * @property Staff $changeBy
 * @property Locations $location
 * @property Payments $payment
 */
class LocationsBalanceHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'locations_balance_history';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'blameable' => [
                'class' => 'yii\behaviors\BlameableBehavior',
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => 'change_by',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['location_id', 'payment_id', 'change_by'], 'integer'],
            [['location_id', 'change_by'], 'required'],
            [['old_balance', 'new_balance'], 'number'],
            [['change_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['change_by' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::class, 'targetAttribute' => ['location_id' => 'id']],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payments::class, 'targetAttribute' => ['payment_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location_id' => 'Location ID',
            'payment_id' => 'Payment ID',
            'change_by' => 'Change By',
            'old_balance' => 'Old Balance',
            'new_balance' => 'New Balance',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[ChangeBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChangeBy()
    {
        return $this->hasOne(User::class, ['id' => 'change_by']);
    }

    /**
     * Gets query for [[Location]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id']);
    }

    /**
     * Gets query for [[Payment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payments::class, ['id' => 'payment_id']);
    }
}
