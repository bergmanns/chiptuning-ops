<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "chips".
 *
 * @property int $id
 * @property int $staff_id
 * @property int|null $visit_id
 * @property int|null $product_id
 * @property int|null $ver_id
 * @property int|null $payment_id
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $cancelled_at
 * @property string|null $status
 *
 * @property Cars[] $cars
 * @property Payments $payment
 * @property Products $product
 * @property User $staff
 * @property ChipVersions $ver
 * @property Visits $visit
 */
class Chips extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chips';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['staff_id'], 'required'],
            [['staff_id', 'visit_id', 'product_id', 'ver_id', 'payment_id'], 'integer'],
            [['status'], 'string'],
            [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payments::class, 'targetAttribute' => ['payment_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::class, 'targetAttribute' => ['product_id' => 'id']],
            [['staff_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['staff_id' => 'id']],
            [['ver_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChipVersions::class, 'targetAttribute' => ['ver_id' => 'id']],
            [['visit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Visits::class, 'targetAttribute' => ['visit_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'staff_id' => Yii::t('app', 'Staff ID'),
            'visit_id' => Yii::t('app', 'Visit ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'ver_id' => Yii::t('app', 'Ver ID'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'cancelled_at' => Yii::t('app', 'Cancelled At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Cars::class, ['chip_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payments::class, ['id' => 'payment_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStaff()
    {
        return $this->hasOne(User::class, ['id' => 'staff_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVer()
    {
        return $this->hasOne(ChipVersions::class, ['id' => 'ver_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(Visits::class, ['id' => 'visit_id']);
    }
}
