<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cash_flow".
 *
 * @property int $id
 * @property int|null $visit_id
 * @property int|null $type_id
 * @property int|null $category_id
 * @property int|null $location_id
 * @property float|null $amount
 * @property float|null $share
 * @property string $created_at [datetime]
 * @property string $updated_at [datetime]
 * @property string $paid_at [datetime]
 * @property string $name [string]
 *
 * @property Categories $category
 * @property CategoryTypes $type
 * @property Locations $location
 * @property Visits $visit
 * @property Payments $payments
 * @property Payments $activePayments

 */
class CashFlow extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_flow';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['visit_id', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['amount', 'share'], 'number'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CategoryTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['location_id'], 'exist', 'skipOnError' => true, 'targetClass' => Locations::className(), 'targetAttribute' => ['location_id' => 'id']],
            [['visit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Visits::className(), 'targetAttribute' => ['visit_id' => 'id']],

            [['type_id', 'category_id', 'location_id', 'name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'visit_id' => Yii::t('app', 'Визит'),
            'type_id' => Yii::t('app', 'Тип'),
            'category_id' => Yii::t('app', 'Категория'),
            'location_id' => Yii::t('app', 'Офис'),
            'amount' => Yii::t('app', 'Стоимость'),
            'share' => Yii::t('app', 'Доля'),
            'name' => Yii::t('app', 'Наименование'),
            'created_at' => Yii::t('app', 'Создано'),
            'updated_at' => Yii::t('app', 'Изменено'),
            'paid_at' => Yii::t('app', 'Оплачено'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CategoryTypes::className(), ['id' => 'type_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::className(), ['id' => 'location_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(Visits::className(), ['id' => 'visit_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::class, ['cash_flow_id' => 'id']);
    }

    public function getActivePayments()
    {
        return $this->getPayments()->andWhere(['<>', 'status', Payments::STATUS_CANCEL]);
    }
}
