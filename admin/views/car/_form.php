<?php

use admin\models\Clients;
use admin\widgets\DropDownList;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Cars */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cars-form">

    <?php $form = ActiveForm::begin();?>

    <?=$form->field($model, 'make_id')->dropDownList(
    [
        $model->make->id ?? null => $model->make->name ?? null,
    ]
    , [
        'class' => 'car-make-selector form-control',
    ])?>

    <?=$form->field($model, 'model_id')->dropDownList([
    $model->model->id ?? null => $model->model->name ?? null,
], [
    'class' => 'car-model-variants-selector form-control',
])?>

    <?=$form->field($model, 'chassis_id')->dropDownList([
    $model->chassis->id ?? null => $model->chassis->name ?? null,
], [
    'class' => 'car-chassis-selector form-control',
])?>

    <?=$form->field($model, 'engine_id')->dropDownList([
    $model->engine_id ?? null => $model->engine->fullName ?? [],
], [
    'class' => 'car-engine-selector form-control',
])?>

    <?=$form->field($model, 'engine_model')->dropDownList([
    $model->engine_model ?? null => $model->engine_model ?? [],
], [
    'class' => 'car-engine-model-selector form-control',
])?>

    <?=$form->field($model, 'gearbox_id')->dropDownList([
    $model->gearbox_id ?? null => $model->gearbox->name ?? null,
], [
    'class' => 'car-gearbox-selector form-control',
])?>

    <?=$form->field($model, 'client_id')->widget(DropDownList::class, [
    'data' => ArrayHelper::map(Clients::getWithPhone(), 'id', 'name_with_phone'),
])?>

    <?=$form->field($model, 'vin_number')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'gos_number')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'issue_year')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'eng_chip_status')->dropDownList(['stock' => 'Stock', 'stage_1' => 'Stage 1', 'stage_2' => 'Stage 2', 'stage_3' => 'Stage 3'], ['prompt' => ''])?>

    <?=$form->field($model, 'gear_chip_status')->dropDownList(['stock' => 'Stock', 'chip' => 'Chip'], ['prompt' => ''])?>

    <div class="form-group">
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
