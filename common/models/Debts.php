<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Transaction;

/**
 * This is the model class for table "debts".
 *
 * @property int $id
 * @property int $visit_id
 * @property int $type_id
 * @property int $category_id
 * @property int $location_id
 * @property int|null $paid_by
 * @property string|null $name
 * @property float|null $amount
 * @property float|null $paid_amount
 * @property string|null $paid_at
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Visits $visit
 * @property VisitsProducts $visitProduct
 * @property CategoryTypes $type
 * @property Categories $category
 * @property Locations $location
 * @property User $paidBy

 */
class Debts extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'debts';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init(): void
    {
        parent::init();

        $this->on(self::EVENT_AFTER_INSERT, [$this, 'updateLocationBalance']);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['paid_by', 'visit_id', 'type_id', 'category_id', 'location_id'], 'integer'],
            [['amount'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['paid_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['paid_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'visit_id' => Yii::t('app', 'Визит'),
            'type_id' => Yii::t('app', 'Тип'),
            'category_id' => Yii::t('app', 'Категория'),
            'location_id' => Yii::t('app', 'Филиал'),
            'paid_by' => Yii::t('app', 'Погасил'),
            'name' => Yii::t('app', 'Наименование'),
            'amount' => Yii::t('app', 'Сумма'),
            'paid_amount' => Yii::t('app', 'Оплачено'),
            'paid_at' => Yii::t('app', 'Оплачено в'),
            'created_at' => Yii::t('app', 'Создан'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPaidBy()
    {
        return $this->hasOne(User::class, ['id' => 'paid_by']);
    }

    public function getActivePayments()
    {
        return $this->getPayments()->andWhere(['<>', 'status', Payments::STATUS_CANCEL]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::class, ['id' => 'category_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CategoryTypes::class, ['id' => 'type_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::class, ['id' => 'location_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisit()
    {
        return $this->hasOne(Visits::class, ['id' => 'visit_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getVisitProduct()
    {
        return $this->hasOne(VisitsProducts::class, ['visit_id' => 'visit_id']);
    }

    public function getPayments() {
        return $this->hasMany(Payments::class, ['id' => 'payment_id'])
            ->viaTable('debts_payments_relation', ['debt_id' => 'id']);
    }

    public function updateLocationBalance()
    {
        if (empty($this->location_id) === true) {
            return;
        }

        if ($this->location->share === 0) {
            return;
        }

        $transaction = $this->getDb()->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $history = new LocationsBalanceHistory();
            $history->location_id = $this->location_id;
            $history->old_balance = $this->location->balance;
            $history->new_balance = $this->location->balance - $this->amount;

            $this->location->balance = $history->new_balance;

            if ($history->save() && $this->location->save()) {
                $transaction->commit();

                return;
            }

            Yii::debug($history->getErrors());
            Yii::debug($this->location->getErrors());

            throw new Exception('Balance update failed');
        } catch (Throwable $e) {
            $transaction->rollBack();
            Yii::error($e);

            throw new Exception('Balance update failed');
        }
    }
}
