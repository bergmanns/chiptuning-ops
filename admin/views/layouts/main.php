<?php

/* @var $this \yii\web\View */
/* @var $content string */

use admin\assets\AppAsset;
use common\models\User;
use common\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'renderInnerContainer' => false,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    $menuItems = [
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $user = Yii::$app->user;
        $menuItems = [
            [
                'label' => 'Визиты',
                'icon' => 'tent',
                'url' => '/visit/index',
                'visible' => $user->can(User::ROLE_MANAGER),
                'active' => $this->context->id === 'visit',
            ],
            [
                'label' => 'Деньги',
                'icon' => 'tent',
                'items' => [
                    ['label' => 'Платежи', 'url' => '/payments/index/'],
                    ['label' => 'Баланс на счетах', 'url' => '/cash-flow/balance/'],
                    ['label' => 'Долги', 'url' => '/debts/index/'],
                    ['label' => 'Баланс филиалов', 'url' => '/location/balance/'],
                ],
                'visible' => $user->can(User::ROLE_ADMIN),
                'active' => (in_array($this->context->id, ['payments', 'debts']) || in_array(Yii::$app->requestedRoute, ['location/balance/', 'cash-flow/balance/'])),
            ],

            [
                'url' => '/car/index/',
                'label' => 'Машины',
                'icon' => 'road',
                'items' => [
                    ['label' => 'Марки', 'url' => '/car-make-variant/index/'],
                    ['label' => 'Модели', 'url' => '/car-model-variant/index/'],
                    ['label' => 'Кузова', 'url' => '/chassis/index/'],
                    ['label' => 'Двигатели', 'url' => '/engine/index/'],
                    ['label' => 'Коробки', 'url' => '/gearboxes/index/'],
                    ['label' => 'Машины клиентов', 'url' => '/car/index/'],
//                    ['label' => 'Каталог', 'url' => '/car-model/index/'],
                    ['label' => 'Запчасти', 'url' => '/parts/index/'],
                ],
                'visible' => $user->can(User::ROLE_MANAGER),
                'active' => in_array($this->context->id,
                    [
                        'car-make-variant',
                        'car-model-variant',
                        'chassis',
                        'engine',
                        'gearboxes',
                        'car',
                        'parts',
                    ]),
            ],
            [
                'url' => '/client/index/',
                'label' => 'Клиенты',
                'icon' => 'user',
                'visible' => $user->can(User::ROLE_MANAGER),
                'active' => $this->context->id === 'client',
            ],
            [
                'url' => '/settings/index',
                'label' => 'Настройки',
                'icon' => 'wrench',
                'items' => [
                    ['label' => 'Филиалы', 'url' => '/location/index/'],
                    ['label' => 'Сотрудники', 'url' => '/user/index/'],
                    ['label' => 'Аккаунты', 'url' => '/accounts/index/'],
//                    ['label' => 'Цели Визита', 'url' => '/visit-reason/index/'],
                    ['label' => 'Продукты', 'url' => '/product/index/'],
                    ['label' => 'Категории платежей', 'url' => '/categories/index/'],
                    ['label' => 'Типы Категорий', 'url' => '/category-types/index/'],
                ],
                'visible' => $user->can(User::ROLE_ADMIN),
                'active' => in_array($this->context->id,
                    [
                        'location',
                        'user',
                        'accounts',
                        'categories',
                        'category-types',
                        'product',
                    ]) && !in_array(Yii::$app->requestedRoute, ['location/balance/', 'cash-flow/balance/']),
            ],
        ];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выйти (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn pull-right btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-left'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    $sideNavItems = [];


    ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
