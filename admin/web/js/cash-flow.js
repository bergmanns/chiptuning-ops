$( document ).ready(function() {
    $(".type-selector").select2({
        theme: 'krajee',
        allowClear: true,
        width: '100%',
        ajax: {
            url: "/category-types/get",
            dataType: 'json',
            delay: 500,
            data: function (params) {
                return {
                    name: params.term, // search term
                };
            },
            processResults: function (data) {
                return {
                    results: data.items,
                };
            },
            cache: true
        },
        placeholder: 'Выберите тип',
        minimumInputLength: 0,
    }).on('select2:select', function (e) {
        var typeSelected = e.params.data.id;
        console.log(typeSelected);

        $('.category-selector').val(null).select2({
            theme: 'krajee',
            allowClear: true,
            width: '100%',
            ajax: {
                url: "/categories/get",
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    console.log(params);
                    return {
                        name: params.term,
                        type_id: typeSelected,
                    };
                },
                processResults: function (data) {
                    return {
                        results: data.items,
                    };
                },
                cache: true
            },
            placeholder: 'Выберите категорию',
            minimumInputLength: 0,
        })
    });
});
