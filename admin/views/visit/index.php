<?php

use admin\assets\VisitAsset;
use common\models\Categories;

/* @var $this yii\web\View */

$this->title = 'Визиты';
$this->params['breadcrumbs'][] = $this->title;
VisitAsset::register($this);
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment-with-locales.min.js"></script>

<div class="visits-index">
    <!-- Modal -->
    <div id="app">
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="visit-edit">Визит {{selectedVisitDateAt}} {{selectedVisitTimeAt}}</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div style="overflow:hidden;">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <input id="visits-visit_at" type="text" class="form-control hidden">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 client-info-block">
                                <h3>Клиент</h3>
                                <div class="form-group">
                                    <label for="visits-client_phone" class="control-label">Номер телефона:</label>
                                    <input v-on:change="getClient" id="visits-client_phone" type="text" class="form-control client-input-phone" v-model="selectedRow.client.phone">
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_name" class="control-label">ФИО:</label>
                                    <input type="text" class="form-control" v-model="selectedRow.client.full_name" id="visits-client_name">
                                </div>
                                <div class="form-group">
                                    <label for="visits-location" class="control-label">Филиал:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.location.id" id="visits-location">
                                        <template v-for="location in locations">
                                            <option v-on:click="selectCountryMask(location.country)" v-bind:value="location.id">{{location.name}}</option>
                                        </template>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 client-info-block client-car-block">
                                <h3>Машина</h3>
                                <div class="form-group">
                                    <label for="visits-car_id" class="control-label">Машины клиента:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.id" id="visits-car_id" v-on:change="selectCar(selectedRow.car.id)">
                                        <option value="-1">Добавить новую</option>
                                        <option v-for="car in cars" v-bind:value="car.id">
                                            {{ car.full_name }}
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_make" class="control-label">Марка:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.make_id" id="visits-client_car_make"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_model" class="control-label">Модель:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.model_id" id="visits-client_car_model"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_chassis" class="control-label">Кузов:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.chassis_id" id="visits-client_car_chassis"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_engine" class="control-label">Двигатель:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.engine_id" id="visits-client_car_engine"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_engine_model" class="control-label">Модель двигателя:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.engine_model" id="visits-client_car_engine_model"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-client_car_gearbox" class="control-label">Коробка:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.car.gearbox_id" id="visits-client_car_gearbox"></select>
                                </div>
                                <div class="form-group">
                                    <label for="visits-vin_number" class="control-label">VIN:</label>
                                    <input type="text" class="form-control" id="visits-vin_number">
                                </div>
                                <div class="form-group">
                                    <label for="visits-gos_number" class="control-label">Гос. номер:</label>
                                    <div class="input-group">
                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{currentCountry}} <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <template v-for="country in gosCountries">
                                                    <li v-on:click="selectCountryMask(country)" ><a data-mask="{{country.gos_number_mask}}" href="#">{{country.short}}</a></li>
                                                </template>
                                            </ul>
                                        </div>
                                        <input type="text" class="form-control gos-input-number" v-model="selectedRow.car.gos_number" id="visits-gos_number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Пробег:</label>
                                    <input type="text" class="form-control" v-model="selectedRow.car.current_mileage" id="recipient-name">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 client-info-block" style="overflow:hidden;">
                                <h3>Визит</h3>
                                <div class="form-group">
                                    <label for="reason-name" class="control-label">Цель:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.category.id" id="reason-name">
                                        <?php
$items = Categories::find()->where(['for_visit' => 1])->all();

foreach ($items as $item) {
    echo "<option value='$item->id'>$item->name</option>";
}
?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Продукт:</label>
                                    <select type="text" class="form-control" v-model="selectedRow.product.id" id="visits-products">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Комментарий:</label>
                                    <textarea rows="10" class="form-control" v-model="selectedRow.comment" id="recipient-name"></textarea>
                                </div>
                                <div class="form-group form-check">
                                    <label class="control-label">Цена визита:</label>
                                    <div class="col-12">
                                        <input type="checkbox" v-model="selectedRow.isLandingPaid" class="form-check-input" id="is_landing_paid">
                                        <label class="form-check-label" for="is_landing_paid">Оплата на сайте</label>
                                    </div>
                                    <div class="col-12">
                                        <input type="checkbox" v-model="selectedRow.isAmountConfirmed" class="form-check-input" id="is_amount_confirmed">
                                        <label class="form-check-label" for="is_amount_confirmed">Согласована</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label for="external-order-id" class="control-label">Номер заказа:</label>
                                            <input type="text" class="form-control" v-model="selectedRow.externalId" id="external-order-id" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <label for="real_amount" class="control-label">Итоговая цена для клиента:</label>
                                            <input type="text" class="form-control" v-model="selectedRow.real_amount" id="real_amount">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <h3>Запчасти</h3>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 client-info-block" style="overflow:hidden;">
                                <div class="form-group">
                                    <label for="visits-client_car_parts_name" class="control-label">Название:</label>
                                    <input type="text" class="form-control" v-model="newPart.name" id="visits-client_car_parts_name">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-6 client-info-block" style="overflow:hidden;">
                                <div class="form-group">
                                    <label for="visits-client_car_parts_name" class="control-label">Количество:</label>
                                    <input type="text" class="form-control" v-model="newPart.count" id="visits-client_car_parts_name">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 client-info-block" style="overflow:hidden;">
                                <div class="form-group">
                                    <label for="visits-client_car_parts_name" class="control-label">Стоимость за шт:</label>
                                    <input type="text" class="form-control" v-model="newPart.amount" id="visits-client_car_parts_name">
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 client-info-block" style="overflow:hidden;">
                                <div class="form-group" style="padding-top: 24px;">
                                    <button type="button" class="btn btn-success" @click="addPart(newPart)">Добавить</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <ul id="car-parts-block">
                                <template v-for="(index, part) in selectedRow.parts" :key="index">
                                <li>
                                    {{ part.name }} {{ part.amount }} x{{ part.count }} <button @click="deletePart(index)" type="button" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-trash"></i></button>
                                </li>
                                </template>
                            </ul>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Отмена</button>
                        <button type="button" class="btn btn-success" @click="saveVisit(this.selectedRow)">Сохранить</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-inline">
            <div class="col-md-6 form-inline">
                <button :class="{active: filter.interval === 'month'}" class="btn btn-default" @click="setTableInterval('month')">Месяц</button>
                <button :class="{active: filter.interval === 'week'}" class="btn btn-default" @click="setTableInterval('week')">Неделя</button>
                <button :class="{active: filter.interval === 'day'}" class="btn btn-default" @click="setTableInterval('day')">День</button>
            </div>
            <div class="col-md-6 form-inline">
                <div class="form-inline form-group pull-right">
                    <input placeholder="Поиск по номеру" class="form-control phone_mask phone-filter">
                    <button class="btn btn-default" @click="applyFilter">Применить</button>
                    <button class="btn btn-default" @click="resetFilter">Сбросить</button>
                </div>
            </div>
        </div>
        <div style="margin-top: 10px" class="row form-inline">
            <div class="col-md-6 form-inline">
                <button class="btn btn-default" @click="shiftFilter('prev')"><i class="glyphicon glyphicon-chevron-left"></i> Пред.</button>
                {{getFilterValue()}}
                <button class="btn btn-default" @click="shiftFilter('next')">След <i class="glyphicon glyphicon-chevron-right"></i> </button>
            </div>
        </div>
        <div style="margin-top: 10px" class="row form-inline">
            <div class="col-md-6 form-inline">
                <button class="btn btn-default" @click="addItem"><i class="glyphicon glyphicon-pencil"></i> Новый визит</button>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <vuetable v-ref:vuetable
                      api-url="/visit/api/search"
                      pagination-path=""
                      :fields="fields"
                      :sort-order="sortOrder"
                      :multi-sort="multiSort"
                      table-class="table table-hover"
                      ascending-icon="glyphicon glyphicon-chevron-up"
                      descending-icon="glyphicon glyphicon-chevron-down"
                      pagination-info-template="Отображено c {from} по {to} из {total} визитов"
                      pagination-info-no-data-template=""
                      :pagination-component="paginationComponent"
                      :item-actions="itemActions"
                      :append-params="moreParams"
                      :per-page="perPage"
                      wrapper-class="vuetable-wrapper"
                      table-wrapper=".vuetable-wrapper"
                      loading-class="loading"
                      detail-row-component="payment-table"
                      detail-row-id="id"
                      detail-row-transition=""
                      row-class-callback="rowClassCB"
            ></vuetable>
        </div>
</div>
</div>
