<?php

namespace admin\widgets;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

class DropDownList extends Select2
{
    /**
     * {@inheritdoc}
     */
    public function __construct($config = [])
    {
        $config = ArrayHelper::merge([
            'options' => [
                'placeholder' => '',
            ],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ], $config);

        parent::__construct($config);
    }
}
