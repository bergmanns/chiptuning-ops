<?php

use admin\widgets\DropDownList;
use common\models\Categories;
use common\models\CategoryTypes;
use common\models\Debts;
use common\models\Locations;
use common\models\User;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;

/* @var $searchModel admin\models\DebtsSearch */
/* @var $role string */

return [
    [
        'attribute' => 'created_at',
        'format' => 'datetime',
        'filter' => DateRangePicker::widget([
            'model' => $searchModel,
            'attribute' => 'created_at',
            'convertFormat' => true,
            'pluginOptions' => [
                'locale' => [
                    'format' => 'Y-m-d',
                ],
            ],
        ]),
    ],
    [
        'attribute' => 'amount',
        'format' => 'currency'
    ],
    [
        'attribute' => 'paid_amount',
        'format' => 'currency'
    ],
    [
        'attribute' => 'location_id',
        'label' => 'Филиал',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'location_id',
            'data' => ArrayHelper::map(Locations::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Debts */
            return $model->location->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'type_id',
        'label' => 'Тип',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'type_id',
            'data' => ArrayHelper::map(CategoryTypes::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Debts */
            return $model->type->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'category_id',
        'label' => 'Категория',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'category_id',
            'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'name'),
        ]),
        'value' => function ($model) {
            /* @var $model Debts */
            return $model->category->name ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    [
        'attribute' => 'visit_id',
        'label' => 'Визит',
        'value' => function ($model) {
            /* @var $model Debts */
            return $model->visit_id ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],

    [
        'attribute' => 'paid_by',
        'label' => 'Погасил',
        'filter' => DropDownList::widget([
            'model' => $searchModel,
            'attribute' => 'paid_by',
            'data' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
        ]),
        'value' => function ($model) {
            /* @var $model Debts */
            return $model->paidBy->username ?? null;
        },
        'headerOptions' => [
            'style' => 'width: 10%',
        ],
    ],
    'name',

    [
        'class' => 'admin\components\grid\ActionWithAccessColumn',
        'role' => $role,
    ],
];