<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CarGearboxes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-gearboxes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'make_id')->dropDownList(
        [
            $model->make_id ?? null => $model->make->name ?? null
        ]
        , [
        'class' => 'car-make-selector form-control',
    ])?>

    <?= $form->field($model, 'model_id')->dropDownList([
        $model->model_id ?? null => $model->model->name ?? null
    ], [
        'class' => 'car-model-variants-selector form-control',
    ])?>

    <?= $form->field($model, 'chassis_id')->dropDownList([
        $model->chassis_id ?? null => $model->chassis->name ?? null
    ], [
        'class' => 'car-chassis-selector form-control',
    ])?>

    <?= $form->field($model, 'engine_id')->dropDownList([
        $model->engine_id ?? null => $model->engine->fullName ?? null,
    ], [
        'class' => 'car-engine-selector form-control',
    ])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
