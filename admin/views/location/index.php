<?php

use admin\models\User;
use admin\widgets\DropDownList;
use common\models\Countries;
use common\models\Locations;
use common\models\Regions;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Филиалы';
$this->params['breadcrumbs'][] = $this->title;
$role = Yii::$app->user->getIdentity()->role;

?>
<div class="locations-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if ($role === User::ROLE_ADMIN) {
             echo Html::a('Добавить офис', ['create'], ['class' => 'btn btn-success']);
        } ?>
        <?= Html::a('Сбросить фильтр', ['index'], ['class' => 'btn btn-success']) ?>

    </p>


    <?= GridView::widget([
        'options' => [
            'class' => 'table-responsive',
        ],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            [
                'attribute' => 'country_id',
                'label' => 'Страна',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'country_id',
                    'data' => ArrayHelper::map(Countries::find()->all(), 'id', 'name'),
                ]),
                'value' => function ($model) {
                    /* @var $model Locations */
                    return $model->country->name ?? null;
                },
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
            ],
            [
                'attribute' => 'region_id',
                'filter' => DropDownList::widget([
                    'model' => $searchModel,
                    'attribute' => 'region_id',
                    'data' => ArrayHelper::map(Regions::find()->all(), 'id', 'name'),
                ]),
                'headerOptions' => [
                    'style' => 'width: 10%',
                ],
                'value' => function ($model) {
                    /* @var $model Locations */
                    return $model->region->name ?? null;
                },
            ],
            'name',

            [
                'attribute' => 'share',
                'headerOptions' => [
                    'style' => 'width: 20%',
                ],
                'value' => function ($model) {
                    /* @var $model Locations */
                    if ($model->share !== null) {
                        return sprintf('%d%%', $model->share);
                    }

                    return null;
                },
            ],
            'balance:currency',
            'created_at',
            //'updated_at',
            //'deleted_at',

            [
                'class' => 'admin\components\grid\ActionWithAccessColumn',
                'role' => $role,
            ],
        ],
    ]); ?>


</div>
