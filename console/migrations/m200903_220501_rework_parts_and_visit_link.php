<?php

use yii\db\Migration;

/**
 * Class m200903_220501_rework_parts_and_visit_link
 */
class m200903_220501_rework_parts_and_visit_link extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('parts_visit_id_key', 'parts');
        $this->renameColumn('parts', 'visit_id', 'visit_product_id');

        $this->addForeignKey(
            'parts_visit_product_id',
            'parts',
            'visit_product_id',
            'visits_products',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('parts_visit_product_id', 'parts');
        $this->renameColumn('parts', 'visit_product_id', 'visit_id');

        $this->addForeignKey(
            'parts_visit_id',
            'parts',
            'visit_id',
            'visits',
            'id'
        );
    }
}
