<?php

use yii\db\Migration;

/**
 * Class m200902_142324_add_parts_table
 */
class m200902_142324_add_parts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('parts', [
            'id' => $this->primaryKey(),
            'visit_id' => $this->integer(),
            'debt_id' => $this->integer(),
            'location_id' => $this->integer(),
            'name' => $this->string(),
            'serial' => $this->string(),
            'count' => $this->integer(),
            'amount' => $this->float(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->addForeignKey(
            'parts_visit_id_key',
            'parts',
            'visit_id',
            'visits',
            'id'
        );

        $this->addForeignKey(
            'parts_debt_id_key',
            'parts',
            'debt_id',
            'debts',
            'id'
        );

        $this->addForeignKey(
            'parts_location_id_key',
            'parts',
            'location_id',
            'locations',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('parts');
    }
}
