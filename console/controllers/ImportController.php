<?php


namespace console\controllers;


use common\components\product\LandingImporter;
use Yii;
use yii\console\Controller;

class ImportController extends Controller
{
    public function actionLanding()
    {
        /** @var LandingImporter $importer */
        $importer = Yii::$app->landingImporter;

        $importer->import();
    }
}