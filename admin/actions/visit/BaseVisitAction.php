<?php

declare (strict_types = 1);

namespace admin\actions\visit;

use admin\models\Clients;
use admin\models\Visits;
use Carbon\CarbonImmutable;
use common\models\Cars;
use common\models\Locations;
use common\models\Parts;
use common\models\Products;
use common\models\VisitsProducts;
use Throwable;
use Yii;
use yii\base\Action;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

abstract class BaseVisitAction extends Action
{

    /**
     * @var Visits
     */
    protected $model;

    protected $scenario;

    /**
     * @var array
     */
    private $post = [];
    /**
     * @var Locations|mixed|null
     */
    private $location;
    /**
     * @var Clients|mixed|null
     */
    private $client;
    /**
     * @var Cars|mixed|null
     */
    private $car;

    /**
     * @var mixed
     */
    private $errors = [];

    /**
     * @param Visits $model
     * @param $location
     * @return Clients|null
     * @throws BadRequestHttpException
     */
    private function initializeClient(Visits &$model, $location)
    {
        $clientId = ArrayHelper::getValue($this->post, 'Visits.client_id');

        if ($clientId !== null && (int) $clientId !== 0) {
            /** @var Clients $client */
            $client = Clients::findOne(['id' => $clientId]);
            $client->addLocation($location);

            return $client;
        }

        $client = new Clients();
        $client->full_name = ArrayHelper::getValue($this->post, 'Visits.client_name');
        $client->region_id = $location->region_id;
        $phone = ArrayHelper::getValue($this->post, 'Visits.client_phone');

        if ($client->save() === false) {
            $model->addError('client_id', 'Проверьте данные клиента');

            throw new BadRequestHttpException('Client save error');
        }

        if (empty($client->setDefaultPhone($phone)) === false) {
            $model->addError('client_phone');

            return $client;
        }

        $client->save(false);
        $client->link('locations', $location);

        return $client;
    }

    /**
     * @param Visits $model
     * @return Locations|null
     * @throws BadRequestHttpException
     */
    private function initializeLocation(Visits &$model):  ? Locations
    {
        $locationId = ArrayHelper::getValue($this->post, 'Visits.location_id');
        $location = Locations::findOne(['id' => $locationId]);

        if ($location === null) {
            $model->addError('location_id', 'Филиал не указан');

            throw new BadRequestHttpException('Филиал не указан');
        }

        return $location;
    }

    protected function initializeCar(Visits &$model,  ? Clients $client) :  ? Cars
    {
        $carId = ArrayHelper::getValue($this->post, 'Visits.car_id');

        if (empty($carId) === true || $carId === 'undefined') {
            return null;
        }

        $vin = ArrayHelper::getValue($this->post, 'Visits.vin_number');
        $vin = empty($vin) === true ? null : $vin;
        $gos = ArrayHelper::getValue($this->post, 'Visits.gos_number');
        $gos = empty($gos) === true ? null : $gos;
        $engineModel = ArrayHelper::getValue($this->post, 'Visits.engine_model');

        if ($carId === '-1') {
            $car = new Cars;
            $car->client_id = $client->id;
            $car->engine_id = ArrayHelper::getValue($this->post, 'Visits.engine_id');
            $car->gearbox_id = ArrayHelper::getValue($this->post, 'Visits.gearbox_id');
            $car->engine_model = $engineModel;

            $car->vin_number = $vin;
            $car->gos_number = $gos;

            if ($car->save() === false) {
                ArrayHelper::merge($this->errors, $car->getErrors());

                $model->addError('car_id', 'Проверьте данные машины');
            }

            return $car;
        }

        $car = Cars::findOne(['id' => $carId, 'client_id' => $client->id]);

        if ($car === null) {
            $model->addError('car_id', 'Проверьте данные машины');
            ArrayHelper::merge($this->errors, $car->getErrors());

            throw new BadRequestHttpException('Visit save error');
        }

        return $car;
    }

    private function initializeVisit(Visits &$model,  ? Clients $client,  ? Cars $car,  ? Locations $location) : Visits
    {
        if ($this->model->isComplete() === true && $this->scenario === Visits::SCENARIO_UPDATE_BY_ADMIN) {
            $model->current_mileage = ArrayHelper::getValue($this->post, 'Visits.current_mileage');
            $this->saveVisit($model);

            return $model;
        }

        $model->current_mileage = ArrayHelper::getValue($this->post, 'Visits.current_mileage');

        $model->car_id = $car->id ?? null;
        $model->client_id = $client->id;
        $model->location_id = $location->id ?? 1;
        $model->source = ArrayHelper::getValue($this->post, 'Visits.source');
        $model->external_id = ArrayHelper::getValue($this->post, 'Visits.external_id');
        $model->real_amount = ArrayHelper::getValue($this->post, 'Visits.real_amount');
        $model->is_amount_confirmed = in_array(
            ArrayHelper::getValue($this->post, 'Visits.is_amount_confirmed'),
            ['true', '1'],
            true
        );
        $model->is_landing_paid = in_array(
            ArrayHelper::getValue($this->post, 'Visits.is_landing_paid'),
            ['true', '1'],
            true
        );

        if ($model->is_landing_paid === true) {
            $model->status = Visits::STATUS_PAID;
            $model->fillStatusDates();
        }

        $this->saveVisit($model);

        return $model;
    }

    /**
     * @param Visits $model
     * @return array
     * @throws BadRequestHttpException
     */
    private function initializeVisitProducts(array &$VisitsProducts, Visits $model, Locations $location) : bool
    {
        $product = ArrayHelper::getValue($this->post, 'VisitsProducts');

        if ($product === null) {
            throw new BadRequestHttpException('VisitsProducts missing');
        }

        $isVisitProductsSaved = true;
        $visitAt = ArrayHelper::getValue($product, 'visit_at');
        $visitProduct = $this->getVisitProducts($model->id);
        $visitProduct = $this->updateVisitComment($model, $visitProduct, $product);

        if ($this->model->isComplete() === true && $this->scenario === Visits::SCENARIO_UPDATE_BY_ADMIN) {
            $visitProduct = $this->updateVisitComment($model, $visitProduct, $product);
            $isVisitProductsSaved = $isVisitProductsSaved && $visitProduct->save();
            $VisitsProducts[] = $visitProduct;

            return $isVisitProductsSaved;
        }

        $visitProduct->visit_id = $model->id;
        $visitProduct->product_id = ArrayHelper::getValue($product, 'product_id');
        $visitProduct->category_id = ArrayHelper::getValue($product, 'category_id');
        $visitProduct->visit_at = $this->getVisitAt($visitAt, $location->getTimeZone());

        $isVisitProductsSaved = $isVisitProductsSaved && $visitProduct->save();
        $VisitsProducts[] = $visitProduct;

        $this->initializeParts($visitProduct, $location);
        $this->addProductToTotalAmount($model, $visitProduct);

        return $isVisitProductsSaved;
    }

    private function getVisitAt($dateTime, $timeZone) : CarbonImmutable
    {
        $visitAt = CarbonImmutable::parse($dateTime, $timeZone);

        return $visitAt;
    }

    private function initializeParts(VisitsProducts $visitProduct, Locations $location)
    {
        //todo зарефачить, когда будем переделывать бизнес-логику зч
        $visitProduct->removeAllParts();

        $newParts = Yii::$app->request->post('Parts', []);

        if (is_array($newParts) === false) {
            return;
        }

        foreach ($newParts as $part) {
            $model = new Parts();
            $model->load($part, '');
            $model->location_id = $location->id;
            $model->visit_product_id = $visitProduct->id;

            Yii::debug($model->toArray());

            if ($model->save() === false) {
                Yii::debug($model->getErrors());
            }
        }
    }

    private function addProductToTotalAmount(Visits $model, VisitsProducts $visitProduct)
    {
        if (empty($model->total_amount) === true) {
            $model->total_amount = 0;
        }

        $model->total_amount = $visitProduct->product->price ?? 0;
    }

    private function checkPrice(VisitsProducts $visitProduct)
    {
        if (empty($this->model->total_amount) === true) {
            return;
        }

        if (empty($this->model->real_amount) === true) {
            $this->model->real_amount = $this->model->total_amount;
        }

        /** @var Products $product */
        $product = $visitProduct->product;

        if (empty($product) === true) {
            return;
        }

        $hqAmount = $product->getHQShare($this->location);
        $discount = $this->model->total_amount - $this->model->real_amount;

        Yii::debug($this->model->real_amount);
        Yii::debug($product->price);
        Yii::debug($hqAmount);
        Yii::debug($discount);
        Yii::debug($this->model->is_amount_confirmed);
        Yii::debug($this->model->real_amount >= $hqAmount);

        if ($this->model->is_amount_confirmed === false) {
            if ($this->model->real_amount < $hqAmount) {
                $this->model->addError('real_amount', 'Цена ниже допустимой без согласования');

                throw new BadRequestHttpException('Visit save error');
            }
        }
    }

    private function saveVisit(Visits $model)
    {
        if ($model->save() === false) {
            ArrayHelper::merge($this->errors, $model->getErrors());
            Yii::debug($model->getErrors());

            throw new BadRequestHttpException('Visit save error');
        }
    }

    protected function handleVisit()
    {
        $visitProducts = [];
        $this->post = Yii::$app->request->post();
        $transaction = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

        try {
            $this->location = $this->initializeLocation($this->model);
            $this->client = $this->initializeClient($this->model, $this->location);
            $this->car = $this->initializeCar($this->model, $this->client);
            $this->model = $this->initializeVisit($this->model, $this->client, $this->car, $this->location);

            if ($this->initializeVisitProducts($visitProducts, $this->model, $this->location) === true) {
                $this->checkPrice($visitProducts[0]);
                $this->saveVisit($this->model);
                $transaction->commit();

                return $this->controller->asJson(['id' => $this->model->id]);
            }

            $transaction->rollBack();
        } catch (Throwable $e) {
            Yii::error($e);

            $transaction->rollBack();
        }

        Yii::$app->response->setStatusCode(400);

        return $this->controller->asJson($this->model->getErrors());
    }

    private function updateVisitComment($model, $visitProduct, $product)
    {
        $visitProduct->comment = ArrayHelper::getValue($product, 'comment');

        if (empty($model->external_id) === false && $model->is_landing_paid === true) {
            $otherComment = preg_replace('/Номер заказа: [0-9]*/', '', $visitProduct->comment);
            $visitProduct->comment = 'Номер заказа: ' . $model->external_id . $otherComment;
            $visitProduct->status = VisitsProducts::STATUS_PAID;
        }

        return $visitProduct;
    }
}
