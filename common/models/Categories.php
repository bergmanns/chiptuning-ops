<?php

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property int $type_id
 * @property string|null $name
 * @property int|null $active
 * @property bool $for_visit
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Payments[] $payments
 * @property CategoryTypes $type
 */
class Categories extends ActiveRecord
{
    const PARTS_CATEGORY_NAME = 'Запчасти';

    const BALANCE_FIX_CATEGORY_NAME = 'Коррекция баланса';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['active', 'for_visit'], 'integer'],
            [['active', 'name', 'type_id'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', 'Тип'),
            'for_visit' => Yii::t('app', 'Для визита'),
            'name' => Yii::t('app', 'Название'),
            'active' => Yii::t('app', 'Включена'),
            'created_at' => Yii::t('app', 'Добавлена'),
            'updated_at' => Yii::t('app', 'Изменена'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['category_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(CategoryTypes::className(), ['id' => 'type_id']);
    }

    public static function getPartsCategory(): Categories
    {
        $type = CategoryTypes::getDefaultType();

        return self::getCategoryByName(self::PARTS_CATEGORY_NAME, $type);
    }

    public static function getCategoryByName($name, CategoryTypes $type): Categories
    {
        $category =  Categories::findOne(['name' => $name, 'type_id' => $type->id]);

        if ($category !== null) {
            return $category;
        }

        $category = new Categories();
        $category->name = $name;
        $category->type_id = $type->id;
        $category->for_visit = 1;
        $category->active = 1;

        $category->save();

        return $category;
    }
}
