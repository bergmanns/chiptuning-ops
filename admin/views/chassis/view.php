<?php

use common\components\user\AccessChecker;
use common\models\CarChassis;
use yii\helpers\Html;
use yii\web\YiiAsset;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\CarChassis */

$name = $model->name;
$this->title = $name;
$this->params['breadcrumbs'][] = ['label' => 'Машины', 'url' => ['car/index']];
$this->params['breadcrumbs'][] = ['label' => 'Кузов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);
?>
<div class="car-models-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php if (Yii::$app->user->can(AccessChecker::EDIT_CAR_ITEM) === true) {
            echo Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ;
            echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ;
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'make_id',
                'label' => 'Марка',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarChassis */
                    if ($model->make !== null) {
                        $html = Html::a($model->make->name, ['car-make-variant/view', 'id' => $model->make_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            [
                'attribute' => 'model_id',
                'label' => 'Серия',
                'format' => 'HTML',
                'value' => function ($model) {
                    /* @var $model CarChassis */
                    if ($model->model !== null) {
                        $html = Html::a($model->model->name, ['car-model-variant/view', 'id' => $model->model_id]);
                        return $html;
                    }

                    return null;
                },
            ],
            'name',
        ],
    ]) ?>

</div>
