<?php

namespace common\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "car_engines".
 *
 * @property int $id
 * @property int|null $chassis_id
 * @property int|null $hp
 * @property int|null $nm
 * @property string|null $name
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at

 * @property string|null fullName
 *
 * @property CarChassis $chassis
 * @property Cars[] $cars
 */
class CarEngines extends ActiveRecord
{
    public $make_id;
    public $model_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_engines';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'common\behaviors\TimestampBehavior',
            ],
            'softdeleteable' => [
                'class' => 'common\behaviors\SoftDeleteableBehavior',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['chassis_id', 'hp', 'nm'], 'integer'],
            [['hp', 'nm'], 'integer', 'min' => 0],
            [['name'], 'string', 'max' => 255],
            [['name', 'hp', 'nm', 'chassis_id'], 'required'],
            [['chassis_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarChassis::className(), 'targetAttribute' => ['chassis_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'chassis_id' => 'Кузов',
            'hp' => 'л.с.',
            'nm' => 'Hm',
            'name' => 'Название',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }

    public function getFullName()
    {
        return sprintf('%s %shp %snm', $this->name, $this->hp, $this->nm);
    }
    /**
     * Gets query for [[Chassis]].
     *
     * @return ActiveQuery
     */
    public function getChassis()
    {
        return $this->hasOne(CarChassis::className(), ['id' => 'chassis_id']);
    }

    /**
     * Gets query for [[Cars]].
     *
     * @return ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Cars::className(), ['engine_id' => 'id']);
    }
}
